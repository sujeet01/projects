package com.urmis.PushNotification;

import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.urmis.Repository.AppPrefs;
import com.urmis.Utils.Constants;

public class FirebaseIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        Log.e("token",refreshedToken);
        // TODO: Implement this method to send any registration to your app's servers.
        saveToken(refreshedToken);
    }

    /**
     * Persist token to third-party servers.
     * <p/>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void saveToken(String token) {
        // Add custom implementation, as needed.
     AppPrefs appPrefs= AppPrefs.getInstance(this);
        appPrefs.putData(Constants.shp_user_Token, token);
        //update the registration token if user is logged in

    }
}