package com.urmis.Authenticate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.urmis.Map.MainActivity;
import com.urmis.Network.Callback.NwCall;
import com.urmis.Network.WebServiceCalls;
import com.urmis.R;
import com.urmis.Repository.AppPrefs;
import com.urmis.Repository.Models.User;
import com.urmis.Utils.Constants;
import com.urmis.Utils.PopMessage;
import com.urmis.Utils.Validation;

public class LoginActivity extends AppCompatActivity {
  private static int NAVIGATE_TO;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login_new);

    NAVIGATE_TO = getIntent().getIntExtra("navigate", Constants.NAVIGATE_TO_PROJECT);
    setUpLogin();

  }

  private void setUpLogin() {
    //init views
    final EditText edEmail = (EditText) findViewById(R.id.input_email);
    final EditText edPassword = (EditText) findViewById(R.id.input_passsword);
    Button btnLogin = (Button) findViewById(R.id.btnLogin);
    //event
    btnLogin.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        if (Validation.isEmailAddress(edEmail, true) && Validation.hasText(edPassword)) {
          loginUser(edEmail.getText().toString(), edPassword.getText().toString());
        }
      }
    });
  }

  private void loginUser(String email, String passWord) {
    WebServiceCalls.login(email, passWord, new NwCall(this, "Signing in..") {
      @Override public void onSuccess(String msg) {
      }

      @Override public void onFailure(String msg) {
        PopMessage.makeshorttoast(LoginActivity.this, msg);
      }

      @Override public void onSuccess(Bundle msg) {
        PopMessage.makeshorttoast(LoginActivity.this, "Successfully signed in..");
        User user=msg.getParcelable("user");
        AppPrefs appPrefs=AppPrefs.getInstance(LoginActivity.this);
        appPrefs.putUser(user);
        setResult(RESULT_OK);
        finish();
      }

      @Override public void onFailure(Bundle msg) {

      }
    });
  }

  @Override public void onBackPressed() {
    super.onBackPressed();
    //finish();
    setResult(0);
  }



}
