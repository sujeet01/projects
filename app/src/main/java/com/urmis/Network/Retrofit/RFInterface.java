package com.urmis.Network.Retrofit;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

import com.urmis.Network.ProgressRequestBody;
import com.urmis.Projects.Progress.UpdateProgress;
import com.urmis.Repository.Models.EditProgress.ResponseDataForApprove;
import com.urmis.Repository.Models.EditProgress.ResponseEditProgress;
import com.urmis.Projects.Progress.ProjectMonthlyProgressManager;
import com.urmis.Repository.Models.EditProgress.ResponseUserTypeForProgress;
import com.urmis.Repository.Models.EditProgress.UpdateProjectMonthlyProgress;
import com.urmis.Repository.Models.Model.GetScenario;
import com.urmis.Repository.Models.Model.GetScenarioResults;
import com.urmis.Repository.Models.Model.GetStationsModel;
import com.urmis.Repository.Models.Model.RunScenarioModel;
import com.urmis.Repository.Models.Progress.NewProgressWorkStatement;
import com.urmis.Repository.Models.Project.Project;
import com.urmis.Repository.Models.SendModels.LoginModel;
import com.urmis.Repository.Models.User;
import com.urmis.Repository.Models.supervisorList.userSupervisorList;
import com.urmis.Repository.ResponseModels.RMNewsList;
import com.urmis.Repository.ResponseModels.RmAlertList;

/**
 * Created by Brajendr on 11/24/2016.
 */

public interface RFInterface {

  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })
  @POST("http://182.76.3.237/URMISWebAPI/api/Account/UserLogin") Call<List<User>> login(
          @Body LoginModel loginModel);

  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })
  @GET("http://182.76.3.237/URMISWebAPI/Announcements")
  Call<List<RMNewsList>> getNews();


  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })
  @GET("http://182.76.3.237/URMISWebAPI/GetAlerts")
  Call<List<RmAlertList>> getAlerts();

  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })
  @GET("http://182.76.3.237/URMISWebAPI/GetStations/")
  Call<List<GetStationsModel>> getStations(
          @Query("riverName") String riverName
  );

  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })
  @GET("http://182.76.3.237/URMISWebAPI/GetScenarioName/")
  Call<List<GetScenario>> getScenarios(
          @Query("riverName") String riverName
  );

  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })
  @GET("http://182.76.3.237/URMISWebAPI/GetScenarioResults/")
  Call<List<GetScenarioResults>> getScenarioResult(
          @Query("riverName") String riverName,
          @Query("scenarioName") String scenarioName

  );

  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })
  @GET("Projects")
  Call<List<Project>> getProjects();

  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })
  @POST("http://182.76.3.237/URMISWebAPI/GetProjectProgress")
  Call<List<NewProgressWorkStatement>> getProgress(
          @Query("id") String id);


  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })
  @POST("http://182.76.3.237/URMISWebAPI/GetProjectMonthlyProgressDataForEdit")
  Call<List<ResponseEditProgress>> getEditProgressList(
          @Query("id") String id);


  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })
  @POST("http://182.76.3.237/URMISWebAPI/GetProjectMonthlyProgressDataForApprove")
  Call<List<ResponseDataForApprove>> getApproveProgressList(
          @Query("id") String id,
          @Query("userid") String userid,
          @Query("currentuserdesignation") String currentuserdesignation
  );

  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })
  @POST("http://182.76.3.237/URMISWebAPI/GetSuperVisorList")
  Call<List<userSupervisorList>> getSupervisorList(
          @Query("currentUserId") String userid);

  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",

  })
  @POST("http://182.76.3.237/URMISWebAPI/ProjectImageUpload")
  Call<ResponseBody> uploadProjectImages(@Body MultipartBody filePart);


  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: multipart/form-data"
  })
  @Multipart
  @POST("http://182.76.3.237/URMISWebAPI/ProjectImageUpload")
  Call<ResponseBody> uploadImages(@Part("id") RequestBody user_id,
                                  @Part("media_file\"; filename=\"media_file\" ") ProgressRequestBody media_file);


  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: multipart/form-data"
  })
  @Multipart
  @POST("http://182.76.3.237/URMISWebAPI/ProjectImageUpload")
  Call<ResponseBody> uploadImages(@Query("id") String p, @Part("media_file\"; filename=\"media_file.jpg\" ") RequestBody media_file);

  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: multipart/form-data"
  })
  @Multipart
  @POST("http://182.76.3.237/URMISWebAPI/ProjectImageUpload")
  Call<ResponseBody> uploadAttachment(@Part MultipartBody.Part filePart,
                                      @Part MultipartBody.Part filePart1);

  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })

  @POST("http://182.76.3.237/URMISWebAPI/SaveProjectProgress")
  Call<ResponseBody> saveProjectProgress(
          @Query("id") String id,
          @Body List<ProjectMonthlyProgressManager> projectMonthlyProgressManagers,
    @Query("progressApproverId") String progressApproverId,
    @Query("progressApproverDesignation") String progressApproverDesignation);

//                @Query("id") String id,
//                @Query("projectMonthlyProgressManager") List<ProjectMonthlyProgressManager> progressWorkList);

//  @POST("http://182.76.3.237/URMISWebAPI/SaveProjectProgress")
//  Call<ResponseBody> saveProjectProgress(@Body List<UpdateProgress> updateProgressList);


  @GET("http://182.76.3.237/URMISWebAPI/RunScenario")
  Call<List<RunScenarioModel>> runScenario(@Query("scenarioName") String scenarioName,
                                     @Query("riverName") String riverName,
                                     @Query("location") String locationName,
                                     @Query("dischargeFileValue") String dischargeValue,
                                     @Query("email") String emailId);




  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })
  @POST("http://182.76.3.237/URMISWebAPI/UpdateProjectMonthlyProgress")
  Call<ResponseBody> updateProjectMonthlyProgress(
          @Query("projectid") String projectid,
          @Query("monthlyprogressid") String monthlyprogressid,
          @Query("valuefinancial") String valuefinancial,
          @Query("valuephysical") String valuephysical,
          @Query("datefinancial") String datefinancial,
          @Query("datephysical") String datephysical,
          @Query("progressApproverId") String progressApproverId,
          @Query("progressApproverDesignation") String progressApproverDesignation
  );

  @POST("http://182.76.3.237/URMISWebAPI/ApproveProjectMonthlyProgress")
  Call<ResponseBody> approveProjectMonthlyProgress(
          @Query("projectid") String projectid,
          @Query("monthlyprogressid") String monthlyprogressid,
          @Query("currentuserdesignation") String userDesignation,
          @Query("progressApproverId") String progressApproverId,
          @Query("progressApproverText") String progressApproverDesignation

  );

  @POST("http://182.76.3.237/URMISWebAPI/NotApproveProjectMonthlyProgress")
  Call<ResponseBody> notApproveProjectMonthlyProgress(
          @Query("projectid") String projectid,
          @Query("monthlyprogressid") String monthlyprogressid);

  @Headers({
          "Authorization: Basic YWRtaW46ZHNzYWRtaW4=",
          "Content-Type: application/json"
  })
  @POST("http://182.76.3.237/URMISWebAPI/GetUserTypeForProgress")
  Call<List<ResponseUserTypeForProgress>> getUserTypeForProgress(
          @Query("projectid") String id);


          //@Body UpdateProjectMonthlyProgress updateProjectMonthlyProgress);

}
