package com.urmis.Network.Callback;

import android.os.Bundle;

/**
 * Created by Brajendr on 12/8/2016.
 */

public interface NetworkCallback {
  public void onSuccess(String msg);
  public void onFailure(String msg);
  public void onSuccess(Bundle msg);
  public void onFailure(Bundle msg);
}
