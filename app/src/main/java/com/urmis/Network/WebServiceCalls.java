package com.urmis.Network;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.urmis.Network.Callback.NwCall;
import com.urmis.Network.Retrofit.RFClient;
import com.urmis.Network.Retrofit.RFInterface;
import com.urmis.Projects.Progress.UpdateProgress;
import com.urmis.Repository.Models.EditProgress.ApproveProjectMonthlyProgress;
import com.urmis.Repository.Models.EditProgress.ResponseDataForApprove;
import com.urmis.Repository.Models.EditProgress.ResponseEditProgress;
import com.urmis.Projects.Progress.ProjectMonthlyProgressManager;
import com.urmis.Repository.Models.AlertContainer;
import com.urmis.Repository.Models.EditProgress.ResponseUserTypeForProgress;
import com.urmis.Repository.Models.EditProgress.UpdateProjectMonthlyProgress;
import com.urmis.Repository.Models.Model.GetScenario;
import com.urmis.Repository.Models.Model.GetScenarioResults;
import com.urmis.Repository.Models.Model.GetStationsModel;
import com.urmis.Repository.Models.Model.RunScenarioModel;
import com.urmis.Repository.Models.Progress.NewProgressWorkStatement;
import com.urmis.Repository.Models.Project.Project;
import com.urmis.Repository.Models.SendModels.LoginModel;
import com.urmis.Repository.Models.User;
import com.urmis.Repository.Models.supervisorList.userSupervisorList;
import com.urmis.Repository.ResponseModels.RMNewsList;
import com.urmis.Repository.ResponseModels.RmAlertList;

import static android.content.ContentValues.TAG;

/**
 * Created by Brajendr on 4/10/2017.
 */

public class WebServiceCalls {
    private static RFInterface rfInterface = RFClient.getClient().create(RFInterface.class);

    public static void getNews(final NwCall nwCall) {
        nwCall.onStart();
        rfInterface.getNews().enqueue(new Callback<List<RMNewsList>>() {
            @Override
            public void onResponse(Call<List<RMNewsList>> call, Response<List<RMNewsList>> response) {
                nwCall.onFinish();
                try {
                    Log.e("re", new Gson().toJson(response));
                    RMNewsList rmNewsList = response.body().get(0);
                    if (rmNewsList.getStatus().equalsIgnoreCase("ok")) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("newsList", rmNewsList.getNewsArrayList());
                        nwCall.onSuccess(bundle);
                    }
                } catch (NullPointerException e) {
                    nwCall.onFailure("Error, please try afer some time.");
                }
            }

            @Override
            public void onFailure(Call<List<RMNewsList>> call, Throwable t) {
                nwCall.onFinish();
                nwCall.onFailure("Error, while fetching news.");
            }
        });
    }

    public static void getProject(String user_id, final NwCall nwCall) {
        nwCall.onStart();
        rfInterface.getProjects().enqueue(new Callback<List<Project>>() {
            @Override
            public void onResponse(Call<List<Project>> call,
                                   Response<List<Project>> response) {
                nwCall.onFinish();
                try {
                    Log.e("response", new Gson().toJson(response));
                    ArrayList<Project> projectArrayList = (ArrayList<Project>) response.body();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("projectList", projectArrayList);
                    nwCall.onSuccess(bundle);


                } catch (Exception e) {
                    nwCall.onFailure("Error, please try after some time.");
                }
            }

            @Override
            public void onFailure(Call<List<Project>> call, Throwable t) {
                nwCall.onFinish();
                Log.e("das", "error as " + t.getMessage());
                nwCall.onFailure("Error, while fetching projects...");
            }
        });

    }

    public static void login(String email, String passWord, final NwCall nwCall) {
            nwCall.onStart();
            final LoginModel loginModel = new LoginModel();
            loginModel.setEmail(email);
            loginModel.setPassword(passWord);
            rfInterface.login(loginModel).enqueue(new Callback<List<User>>() {
                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                    nwCall.onFinish();
                    List<User> users = response.body();
                    User user = users.get(0);
                    if (user.getStatus().equalsIgnoreCase("ok")) {
                        user.setLoggedIn(true);
                        user.setEmailId(loginModel.getEmail());
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("user", user);
                        nwCall.onSuccess(bundle);
                    } else if (user.getStatus().equalsIgnoreCase("error")) {
                        nwCall.onFailure(user.getMessage());
                    } else {
                        nwCall.onFailure("Error, please try afer some time");
                    }
                }

                @Override
                public void onFailure(Call<List<User>> call, Throwable t) {
                    nwCall.onFinish();
                    nwCall.onFailure("Error, while signing in..");
                }
            });
    }


    public static void getStations(String riverName, final NwCall nwCall) {
        nwCall.onStart();
        rfInterface.getStations(riverName).enqueue(new Callback<List<GetStationsModel>>() {
            @Override
            public void onResponse(Call<List<GetStationsModel>> call,
                                   Response<List<GetStationsModel>> response) {
                nwCall.onFinish();
                try {
                    Log.e("response", new Gson().toJson(response));
                    ArrayList<GetStationsModel> stationArrayList = (ArrayList<GetStationsModel>) response.body();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("stationList", stationArrayList);
                    nwCall.onSuccess(bundle);


                } catch (Exception e) {
                    nwCall.onFailure("Error, please try after some time.");
                }
            }

            @Override
            public void onFailure(Call<List<GetStationsModel>> call, Throwable t) {
                nwCall.onFinish();
                Log.e("das", "error as " + t.getMessage());
                nwCall.onFailure("Error, while fetching locations...");
            }
        });
    }


    public static void getScenarios(String riverName, final NwCall nwCall) {
        nwCall.onStart();
        rfInterface.getScenarios(riverName).enqueue(new Callback<List<GetScenario>>() {
            @Override
            public void onResponse(Call<List<GetScenario>> call,
                                   Response<List<GetScenario>> response) {
                nwCall.onFinish();
                try {
                    Log.e("response", new Gson().toJson(response));
                    ArrayList<GetScenario> scenariosArrayList = (ArrayList<GetScenario>) response.body();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("scenarioList", scenariosArrayList);
                    nwCall.onSuccess(bundle);


                } catch (Exception e) {
                    nwCall.onFailure("Error, please try after some time.");
                }
            }

            @Override
            public void onFailure(Call<List<GetScenario>> call, Throwable t) {
                nwCall.onFinish();
                Log.e("das", "error as " + t.getMessage());
                nwCall.onFailure("Error, while fetching Secnarios...");
            }
        });
    }

    public static void getScenarioResults(String riverName,String scenarioName ,final NwCall nwCall) {
        nwCall.onStart();
        rfInterface.getScenarioResult(riverName,scenarioName).enqueue(new Callback<List<GetScenarioResults>>() {
            @Override
            public void onResponse(Call<List<GetScenarioResults>> call,
                                   Response<List<GetScenarioResults>> response) {
                nwCall.onFinish();
                try {
                    Log.e("response", new Gson().toJson(response));
                    ArrayList<GetScenarioResults> scenarioResultArrayList = (ArrayList<GetScenarioResults>) response.body();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("scenarioResultList", scenarioResultArrayList);
                    nwCall.onSuccess(bundle);


                } catch (Exception e) {
                    nwCall.onFailure("Error, please try after some time.");
                }
            }

            @Override
            public void onFailure(Call<List<GetScenarioResults>> call, Throwable t) {
                nwCall.onFinish();
                Log.e("das", "error as " + t.getMessage());
                nwCall.onFailure("Error, while fetching Secnarios...");
            }
        });
    }




    public static void getAlerts(final NwCall nwCall) {
        rfInterface.getAlerts().enqueue(new Callback<List<RmAlertList>>() {
            @Override
            public void onResponse(Call<List<RmAlertList>> call, Response<List<RmAlertList>> response) {
                nwCall.onFinish();
                try {
                    RmAlertList rmAlertList = response.body().get(0);
                    if (rmAlertList.getStatus().equalsIgnoreCase("ok")) {
                        ArrayList<AlertContainer> alertContainers = rmAlertList.getAlertContainerArrayList();
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("alertList", alertContainers);
                        nwCall.onSuccess(bundle);
                    }

                } catch (Exception e) {
                    nwCall.onFailure("Error, please try afer some time");
                }
            }

            @Override
            public void onFailure(Call<List<RmAlertList>> call, Throwable t) {

                Log.e("eror", "e" + t.getMessage());
                nwCall.onFinish();
                nwCall.onFailure("Error, while fetching alerts..");
            }
        });

    }

    public static void getProgress(String id, final NwCall nwCall) {
        nwCall.onStart();
        rfInterface.getProgress(id).enqueue(new Callback<List<NewProgressWorkStatement>>() {
            @Override
            public void onResponse(Call<List<NewProgressWorkStatement>> call, Response<List<NewProgressWorkStatement>> response) {
                nwCall.onFinish();
                try {

                  List<NewProgressWorkStatement> progressData = response.body();
                  NewProgressWorkStatement ll = progressData.get(0);

                  List<NewProgressWorkStatement> lll =(List<NewProgressWorkStatement>)response.body();


                    Bundle bundle = new Bundle();
                    //bundle.putParcelable("progressList1", ll);
                   bundle.putParcelable("progressdata",ll);
                    nwCall.onSuccess(bundle);
                } catch (Exception E) {

                }
            }

            @Override
            public void onFailure(Call<List<NewProgressWorkStatement>> call, Throwable t) {
                nwCall.onFinish();
                nwCall.onFailure("Error, while fetching alerts..");
            }
        });
    }

    public static void getEditProgress(String id, final NwCall nwCall) {
        nwCall.onStart();
        rfInterface.getEditProgressList(id).enqueue(new Callback<List<ResponseEditProgress>>() {
            @Override
            public void onResponse(Call<List<ResponseEditProgress>> call, Response<List<ResponseEditProgress>> response) {
                nwCall.onFinish();
                try {


                    ArrayList<ResponseEditProgress> editProgressData = (ArrayList<ResponseEditProgress>) response.body();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("editprogressdata",editProgressData);
                    nwCall.onSuccess(bundle);

                } catch (Exception E) {

                }
            }

            @Override
            public void onFailure(Call<List<ResponseEditProgress>> call, Throwable t) {
                nwCall.onFinish();
                nwCall.onFailure("Error, while fetching alerts..");
            }
        });
    }

    public static void getUserTypeForProgress(String id, final NwCall nwCall) {
        nwCall.onStart();
        rfInterface.getUserTypeForProgress(id).enqueue(new Callback<List<ResponseUserTypeForProgress>>() {
            @Override
            public void onResponse(Call<List<ResponseUserTypeForProgress>> call, Response<List<ResponseUserTypeForProgress>> response) {
                nwCall.onFinish();
                try {


                    ArrayList<ResponseUserTypeForProgress> responseUserTypeForProgresses = (ArrayList<ResponseUserTypeForProgress>) response.body();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("responseUserTypeForProgresses",responseUserTypeForProgresses);
                    nwCall.onSuccess(bundle);

                } catch (Exception E) {

                }
            }

            @Override
            public void onFailure(Call<List<ResponseUserTypeForProgress>> call, Throwable t) {
                nwCall.onFinish();
                nwCall.onFailure("Error, while fetching User Type..");
            }
        });
    }


    public static void getApproveProgress(String id,String userid, String currentuserdesignation, final NwCall nwCall) {
        nwCall.onStart();
        rfInterface.getApproveProgressList(id,userid,currentuserdesignation).enqueue(new Callback<List<ResponseDataForApprove>>() {
            @Override
            public void onResponse(Call<List<ResponseDataForApprove>> call, Response<List<ResponseDataForApprove>> response) {
                nwCall.onFinish();
                try {


                    ArrayList<ResponseDataForApprove> approveProgressData = (ArrayList<ResponseDataForApprove>) response.body();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("approveprogressdata",approveProgressData);
                    nwCall.onSuccess(bundle);

                } catch (Exception E) {

                }
            }

            @Override
            public void onFailure(Call<List<ResponseDataForApprove>> call, Throwable t) {
                nwCall.onFinish();
                nwCall.onFailure("Error, while fetching progress approved data..");
            }
        });
    }

    public static void getSupervisorList(String userid, final NwCall nwCall) {
        nwCall.onStart();
        rfInterface.getSupervisorList(userid).enqueue(new Callback<List<userSupervisorList>>() {
            @Override
            public void onResponse(Call<List<userSupervisorList>> call, Response<List<userSupervisorList>> response) {
                nwCall.onFinish();
                try {


                    ArrayList<userSupervisorList> supervisorList = (ArrayList<userSupervisorList>) response.body();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("supervisorList",supervisorList);
                    nwCall.onSuccess(bundle);

                } catch (Exception E) {

                }
            }

            @Override
            public void onFailure(Call<List<userSupervisorList>> call, Throwable t) {
                nwCall.onFinish();
                nwCall.onFailure("Error, while fetching progress approved data..");
            }
        });
    }



    public static void updateProjectMonthlyProgress(String projectid, String monthlyprogressid, String valuefinancial, String valuephysical, String datefinancial, String datephysical,String progressApproverId,String progressApproverDesignation, final NwCall nwCall) {
        nwCall.onStart();
        final UpdateProjectMonthlyProgress updateProjectMonthlyProgress = new UpdateProjectMonthlyProgress();
        updateProjectMonthlyProgress.setProjectid(projectid);
        updateProjectMonthlyProgress.setMonthlyprogressid(monthlyprogressid);
        updateProjectMonthlyProgress.setValuefinancial(valuefinancial);
        updateProjectMonthlyProgress.setValuephysical(valuephysical);
        updateProjectMonthlyProgress.setDatefinancial(datefinancial);
        updateProjectMonthlyProgress.setDatephysical(datephysical);
        updateProjectMonthlyProgress.setDatephysical(progressApproverId);
        updateProjectMonthlyProgress.setDatephysical(progressApproverDesignation);
        rfInterface.updateProjectMonthlyProgress(projectid,monthlyprogressid,valuefinancial,valuephysical,datefinancial,datephysical,progressApproverId,progressApproverDesignation).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                nwCall.onFinish();
                nwCall.onSuccess("Your Progress is updated successfully.");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                nwCall.onFinish();

                nwCall.onFailure("Error, while updating in..");
            }
        });
    }

    public static void approveProjectMonthlyProgress(String projectid, String monthlyprogressid,String userDesignation,String progressApproverId,String progressApproverDesignatiobn, final NwCall nwCall) {
        nwCall.onStart();
        final ApproveProjectMonthlyProgress approveProjectMonthlyProgress = new ApproveProjectMonthlyProgress();
        approveProjectMonthlyProgress.setProjectid(projectid);
        approveProjectMonthlyProgress.setMonthlyprogressid(monthlyprogressid);
        final ProgressDialog progressDoalog;
        rfInterface.approveProjectMonthlyProgress(projectid,monthlyprogressid,userDesignation,progressApproverId,progressApproverDesignatiobn).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                nwCall.onFinish();
                nwCall.onSuccess("Your Progress is approved successfully.");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                nwCall.onFinish();

                nwCall.onFailure("Error, while Approving ");
            }
        });
    }

    public static void notApproveProjectMonthlyProgress(String projectid, String monthlyprogressid, final NwCall nwCall) {
        nwCall.onStart();
        final ApproveProjectMonthlyProgress approveProjectMonthlyProgress = new ApproveProjectMonthlyProgress();
        approveProjectMonthlyProgress.setProjectid(projectid);
        approveProjectMonthlyProgress.setMonthlyprogressid(monthlyprogressid);
        final ProgressDialog progressDoalog;
        rfInterface.notApproveProjectMonthlyProgress(projectid,monthlyprogressid).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                nwCall.onFinish();
                nwCall.onSuccess("Your Progress is not approved successfully.");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                nwCall.onFinish();

                nwCall.onFailure("Error, while Not Approving ");
            }
        });
    }


            public static void saveProgress(String id, List<ProjectMonthlyProgressManager> progressWorkList,String progressApproverId,String progressApproverDesignation,
                                              final NwCall nwCall) {
                nwCall.onStart();
                final UpdateProgress updateProgress = new UpdateProgress();
                updateProgress.setProjectId(id);
                updateProgress.setprojectMonthlyProgressManager(progressWorkList);

                rfInterface.saveProjectProgress(id, progressWorkList,progressApproverId,progressApproverDesignation).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody>call, Response<ResponseBody> response) {
                        nwCall.onFinish();
                        nwCall.onSuccess("successfully saved the progress");
                    }

                    @Override
                    public void onFailure(Call<ResponseBody>call, Throwable t) {
                        nwCall.onFinish();
                        nwCall.onFailure("Error, while updation. Please try again.");
                    }
        });
    }


    public static void runScenario(String scenarioName, String riverName, String stationName, double dischargeValue, String emailId, final NwCall nwCall) {
        nwCall.onStart();
        String discharge = Double.toString(dischargeValue);
        rfInterface.runScenario(scenarioName, riverName, stationName, discharge, emailId).enqueue(new Callback<List<RunScenarioModel>>() {
            @Override
            public void onResponse(Call<List<RunScenarioModel>> call, Response<List<RunScenarioModel>>  response) {
               List<RunScenarioModel> resultresponse =response.body();
               String result= resultresponse.get(0).getResult();
                nwCall.onSuccess(result);
            }

            @Override
            public void onFailure(Call<List<RunScenarioModel>>  call, Throwable t) {
                nwCall.onFinish();
                nwCall.onFailure("Error, while running simulation. Please try again.");
            }
        });
    }


}
