package com.urmis.Utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.Window;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;

import com.github.mikephil.charting.formatter.StackedValueFormatter;
import com.github.mikephil.charting.renderer.DataRenderer;
import com.github.mikephil.charting.renderer.XAxisRenderer;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.urmis.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * Created by Brajendr on 4/21/2017.
 */

public class Utility {

  public static Dialog createDialog(Context context, int layout) {
    final Dialog dialog = new Dialog(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(layout);
    dialog.show();
    return dialog;
  }

  public static Dialog createDialog(Context context, int layout, boolean isFullscreen) {
    Dialog dialog;
    if (isFullscreen) {
      dialog = new Dialog(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
    } else {
      dialog = new Dialog(context);
    }

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(layout);
    dialog.show();
    return dialog;
  }

  public static BitmapDescriptor getMarkerIcon(String color) {
    float[] hsv = new float[3];
    Color.colorToHSV(Color.parseColor(color), hsv);
    return BitmapDescriptorFactory.defaultMarker(hsv[0]);
  }

  public static void addChartData(PieChart mChart, Float[] yData,String[] xData)
  {
      List<PieEntry> yVals1 = new ArrayList<PieEntry>() ;

//      ArrayList<String> xVals = new ArrayList<String>();
//      for (int i = 0; i < xData.length; i++)
      //    xVals.add(xData[i]);

    for (int i = 0; i < yData.length; i++)
      yVals1.add(new PieEntry(yData[i],xData[i] ));




    // create pie data set
    PieDataSet dataSet = new PieDataSet(yVals1,"progress");
    dataSet.setSliceSpace(3);
    dataSet.setSelectionShift(5);
    dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
    dataSet.setValueLinePart1OffsetPercentage(60.f);
    dataSet.setValueLinePart1Length(0.8f);
    dataSet.setValueLinePart2Length(0.4f);

    // add many colors
    ArrayList<Integer> colors = new ArrayList<Integer>();

    for (int c : ColorTemplate.VORDIPLOM_COLORS)
      colors.add(c);

    for (int c : ColorTemplate.JOYFUL_COLORS)
      colors.add(c);

    for (int c : ColorTemplate.COLORFUL_COLORS)
      colors.add(c);

      int [] color={ Color.rgb(30,144,255), Color.rgb(220,20,60), Color.rgb(255,99,71),

      };

    //dataSet.setColors(colors);
      dataSet.setColors(color);
      //dataSet.setValueFormatter(new ValueFormatter());


    // instantiate pie data object now
    PieData data = new PieData(dataSet);
  // data.setValueFormatter(new ValueFormatter());
   data.setValueFormatter(new LargeValueFormatter());

   data.setValueTextSize(11f);
    data.setValueTextColor(Color.GRAY);
    data.setValueTextColor(Color.BLACK);

    mChart.setData(data);

    // undo all highlights
    mChart.highlightValues(null);

    // update pie chart
    mChart.invalidate();

    Legend l = mChart.getLegend();

    l.setPosition(Legend.LegendPosition.ABOVE_CHART_CENTER);
    l.setXEntrySpace(7);
    l.setYEntrySpace(5);
  }

  public static String getCurrentDate() {
    Date todayDate = Calendar.getInstance().getTime();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
    String todayString = formatter.format(todayDate);
    return todayString;
  }

  private static class ValueFormatter implements IValueFormatter {

      private DecimalFormat mFormat;

      public  ValueFormatter(){
          mFormat = new DecimalFormat("###,###,##0.0");
      }
    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        //return mFormat.format(value);
        return "" + ((int) value);
    }
  }

}

