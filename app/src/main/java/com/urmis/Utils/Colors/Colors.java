package com.urmis.Utils.Colors;

import java.util.Random;

/**
 * Created by Brajendr on 4/13/2017.
 */

public class Colors {
  private static String[] colors = {
      "#500f4d8d", "#50a2e24e", "#50f7573c", "#508900FF", "#50233859", "#50999b30", "#50ca48d6",
      "#5046a39e", "#5056af7a", "#505b8eaf", "#50543693", "#50ed0942", "#419E9E","#505e5134",
      "#500f4d8d", "#50a2e24e", "#50f7573c", "#508900FF", "#50233859", "#50999b30", "#50ca48d6",
      "#5046a39e", "#5056af7a", "#505b8eaf", "#50543693", "#50ed0942", "#50af4123","#505e5134"
  };

  public static String getRandomColor() {
    int rnd = new Random().nextInt(colors.length);
    return colors[rnd];
  }

  public static String getColor(int rnd) {
    return colors[rnd];
  }
}
