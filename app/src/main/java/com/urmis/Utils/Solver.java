package com.urmis.Utils;

import java.util.HashSet;

/**
 * Created by Brajendr on 4/27/2017.
 */

public class Solver {

  public static boolean solve(int[][] board,int n,int size){
    for(int i=0; i<size; i++){
      for(int j=0; j<size; j++){
        if(board[i][j]!='.')
          continue;

        for(int k=1; k<=size; k++){
          board[i][j] = (char) (k+'0');
          if(isValid(board, i, j,n,size) && solve(board,n,size))
            return true;
          board[i][j] = '.';
        }

        return false;
      }
    }

    return true; // does not matter
  }

  public static boolean isValid(int[][] board, int i, int j,int q,int size) {
    HashSet<Integer> set = new HashSet<>();

    for (int k = 0; k < size; k++) {
      if (set.contains(board[i][k])) return false;

      if (board[i][k] != '.') {
        set.add(board[i][k]);
      }
    }

    set.clear();

    for (int k = 0; k < size; k++) {
      if (set.contains(board[k][j])) return false;

      if (board[k][j] != '.') {
        set.add(board[k][j]);
      }
    }

    set.clear();

    for (int m = 0; m < q; m++) {
      for (int n = 0; n < q ; n++) {
        int x = i / q * q + m;
        int y = j / q * q + n;
        if (set.contains(board[x][y])) return false;

        if (board[x][y] != '.') {
          set.add(board[x][y]);
        }
      }
    }

    return true;
  }

}
