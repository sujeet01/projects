package com.urmis.Utils;

import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;
import java.util.Map;

/**
 * Created by Brajendr on 4/10/2017.
 */

public interface Constants {
 // String BASE_URL = "http://180.151.231.108/URMISWebAPI/swagger/";
 
  //String BASE_URL = "http://182.76.2.237/urmiswebapi/";
 String BASE_URL = "http://182.76.3.237/URMISWebAPI/";
  String SHP_USER_INFORMATION = "userInfo";

  //navigate
  int NAVIGATE_TO_PROJECT = 23;
  int NAVIGATE_TO_REPORT = 24;
 int NAVIGATE_TO_MODEL = 25;
 int NAVIGATE_TO_NEWS = 26;

  //shapetype
  int POLYGON = 0;
  int POINT = 1;
  int POLYLINE = 2;
  String shp_user_Token="fcmToken";

  //alerttype
  String ALERT_NORMAL="green";
  String ALERT_CRITICAL="red";
  String ALERT_WARNING="yellow";



}
