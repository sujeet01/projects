package com.urmis.Alerts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.HashMap;
import java.util.List;
import com.urmis.R;
import com.urmis.Repository.Models.Alert;
import com.urmis.Utils.Constants;

public class AlertsListAdapter extends BaseExpandableListAdapter {

  private Context _context;
  private List<String> _listDataHeader; // header titles
  // child data in format of header title, child title
  private HashMap<String, List<Alert>> _listDataChild;

  public AlertsListAdapter(Context context, List<String> listDataHeader,
      HashMap<String, List<Alert>> listChildData) {
    this._context = context;
    this._listDataHeader = listDataHeader;
    this._listDataChild = listChildData;
  }

  @Override public Object getChild(int groupPosition, int childPosititon) {
    return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
  }

  @Override public long getChildId(int groupPosition, int childPosition) {
    return childPosition;
  }

  @Override
  public View getChildView(int groupPosition, final int childPosition, boolean isLastChild,
      View convertView, ViewGroup parent) {
    LayoutInflater infalInflater =
            (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if(childPosition==0) return convertView=infalInflater.inflate(R.layout.layout_alert_child_header,null);
    final Alert alert = (Alert) getChild(groupPosition, childPosition-1);

    if (convertView == null) {

      convertView = infalInflater.inflate(R.layout.layout_alert_child, null);
    }

    TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
    TextView txtListDate = (TextView) convertView.findViewById(R.id.lblListDate);
    ImageView linearLayout = (ImageView) convertView.findViewById(R.id.ll_alert_type);

    int pipeIndex=alert.getStationname().indexOf("|");
    txtListChild.setText(alert.getStationname().substring(0,pipeIndex));
    txtListDate.setText(alert.getValue()+" "+alert.getStationname().substring(pipeIndex+1,alert.getStationname().length()));
    linearLayout.setBackgroundResource(getColor(alert.getColor()));
    return convertView;
  }

  private int getColor(String category) {
    switch (category) {
      case Constants.ALERT_CRITICAL:
        return R.color.red;

      case Constants.ALERT_WARNING:
        return R.color.yellow;
      case Constants.ALERT_NORMAL:
        return R.color.colorPrimary;

      default:
        return android.R.color.holo_red_light;
    }
  }

  @Override public int getChildrenCount(int groupPosition) {

    return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size()+1;
  }

  @Override public Object getGroup(int groupPosition) {
    return this._listDataHeader.get(groupPosition);
  }

  @Override public int getGroupCount() {

    Log.e("size", this._listDataHeader.size() + " ");
    return this._listDataHeader.size();
  }

  @Override public long getGroupId(int groupPosition) {
    return groupPosition;
  }

  @Override public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
      ViewGroup parent) {
    LayoutInflater infalInflater =
            (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    String headerTitle = (String) getGroup(groupPosition);
    if (convertView == null) {

      convertView = infalInflater.inflate(R.layout.layout_alert_header, null);
    }

    TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
    lblListHeader.setTypeface(null, Typeface.BOLD);
    lblListHeader.setText(headerTitle);

    return convertView;
  }

  @Override public boolean hasStableIds() {
    return false;
  }

  @Override public boolean isChildSelectable(int groupPosition, int childPosition) {
    return true;
  }
}