package com.urmis.Alerts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.urmis.Network.Callback.NwCall;
import com.urmis.Network.WebServiceCalls;

import com.urmis.R;
import com.urmis.Repository.Models.Alert;
import com.urmis.Repository.Models.AlertContainer;
import com.urmis.Utils.PopMessage;

/**
 * Created by Brajendr on 4/10/2017.
 */

public class AlertFragment extends Fragment {
    private View view;
    private ExpandableListView rcAlertList;
    private ArrayList<AlertContainer> alertList = new ArrayList<>();
    private boolean wasAlreadyRunOnce;
    private RelativeLayout rlLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout insLinear;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_alert, container, false);
        init();
        setUpList();
        setUpSwipeReferesh();
        if (!wasAlreadyRunOnce) {
            getAlerts();
            wasAlreadyRunOnce = true;
        }
        return view;
    }

    private void getAlerts() {
        swipeRefreshLayout.setRefreshing(true);
        WebServiceCalls.getAlerts(new NwCall(rlLayout) {
            @Override
            public void onSuccess(String msg) {

                swipeRefreshLayout.setRefreshing(false);
                PopMessage.makeshorttoast(getActivity(), msg);
            }

            @Override
            public void onFailure(String msg) {
                swipeRefreshLayout.setRefreshing(false);
                PopMessage.makeshorttoast(getActivity(), msg);
                if(alertList.size()==0)
                {
                    insLinear.setVisibility(View.VISIBLE);
                }
                else
                {
                    insLinear.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSuccess(Bundle msg) {
                swipeRefreshLayout.setRefreshing(false);
                ArrayList<AlertContainer> news = msg.getParcelableArrayList("alertList");
                alertList = new ArrayList<AlertContainer>();
                alertList.addAll(news);
                if(news.size()==0)
                {
                    insLinear.setVisibility(View.VISIBLE);
                }
                else
                {
                    insLinear.setVisibility(View.GONE);
                }
                setUpList();
            }

            @Override
            public void onFailure(Bundle msg) {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void setUpSwipeReferesh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                alertList = new ArrayList<>();
                setUpList();
                getAlerts();
            }
        });
    }

    private void setUpList() {
        List<String> headers = new ArrayList<>();
        HashMap<String, List<Alert>> hashMap = new HashMap<>();
        for (AlertContainer alertContainer : alertList) {
            headers.add(alertContainer.getName());
            hashMap.put(alertContainer.getName(), alertContainer.getAlerts());
        }
        rcAlertList.setAdapter(new AlertsListAdapter(getActivity(), headers, hashMap));
    }

    private void init() {
        rcAlertList = (ExpandableListView) view.findViewById(R.id.rcAlert);
        rlLayout = (RelativeLayout) view.findViewById(R.id.rlLoading);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        insLinear=(LinearLayout)view.findViewById(R.id.layout_instructions);
    }
}
