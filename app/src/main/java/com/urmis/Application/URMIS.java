package com.urmis.Application;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.urmis.Network.ConnectivityReceiver;
import com.urmis.Utils.FontsOverride;

/**
 * Created by Brajendr on 4/11/2017.
 */

public class URMIS extends Application {
  private static URMIS mInstance;

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }

  @Override public void onCreate() {
    super.onCreate();
    mInstance = this;
    FontsOverride.setDefaultFont(this, "MONOSPACE", "lat.ttf");
  }
  public static synchronized URMIS getInstance() {
    return mInstance;
  }

  public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
    ConnectivityReceiver.connectivityReceiverListener = listener;
  }
}
