package com.urmis.Repository;

/**
 * Created by Brajendr on 7/19/2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import com.urmis.Repository.Models.Shape.ShapeLayer;
import com.urmis.Repository.Models.User;
import com.urmis.Utils.Constants;

public class AppPrefs {

  private Context context;

  private static AppPrefs ourInstance;
  private SharedPreferences preferences;

  public static AppPrefs getInstance(Context context) {
    if (ourInstance == null) {
      ourInstance = new AppPrefs(context);
    }
    return ourInstance;
  }

  private AppPrefs(Context context) {
    this.context = context;
  }

  public SharedPreferences getPreferences() {
    if (preferences == null) {
      preferences = context.getSharedPreferences("prefs", Context.MODE_MULTI_PROCESS);
    }
    return preferences;
  }

  public <T> void putData(String key, T obj) {

    SharedPreferences.Editor editor = getPreferences().edit();
    if (obj instanceof String) {
      editor.putString(key, (String) obj);
    } else if (obj instanceof Boolean) {
      editor.putBoolean(key, ((Boolean) obj).booleanValue());
    } else if (obj instanceof Integer) {
      editor.putInt(key, ((Integer) obj).intValue());
    } else if (obj instanceof Long) {
      editor.putLong(key, ((Long) obj).longValue());
    }
    editor.commit();
  }

  public <T> T getData(String key, T obj) {
    if (obj instanceof String) {
      return (T) getPreferences().getString(key, (String) obj);
    } else if (obj instanceof Boolean) {
      return (T) (Boolean) getPreferences().getBoolean(key, ((Boolean) obj).booleanValue());
    } else if (obj instanceof Integer) {
      return (T) (Integer) getPreferences().getInt(key, ((Integer) obj).intValue());
    } else if (obj instanceof Long) {
      return (T) (Long) getPreferences().getLong(key, ((Long) obj).longValue());
    }
    return null;
  }

  public void removeKeyData(String key) {
    getPreferences().edit().remove(key).commit();
  }

  public void clearAll() {
    preferences.edit().clear().commit();
  }

  public Typeface getTypeFaceByName(String fontName) {
    return Typeface.createFromAsset(context.getAssets(), fontName);
  }

  public User getUser() {
    Gson gson = new Gson();
    String json = getPreferences().getString(Constants.SHP_USER_INFORMATION, "");
    User obj = null;
    if (TextUtils.isEmpty(json)) {
      obj = new User();
    } else {
      obj = gson.fromJson(json, User.class);
    }
    return obj;
  }

  public void putUser(User user) {

    Gson gson = new Gson();
    SharedPreferences.Editor editor = getPreferences().edit();
    String jsonValue = gson.toJson(user);
    editor.putString(Constants.SHP_USER_INFORMATION, jsonValue);
    editor.commit();
  }

  public void flushData() {
    ourInstance.putUser(new User());
  }

  public ArrayList<ShapeLayer> getShapeLayerList() {



    Type type = new TypeToken<ArrayList<ShapeLayer>>() {
    }.getType();
    String lists = preferences.getString("shapeLayer", "");
    Gson gson = new Gson();
    ArrayList<ShapeLayer> schoolinfomodels;
    if (!lists.equals("")) {
      schoolinfomodels = gson.fromJson(lists, type);
    } else {
      schoolinfomodels = new ArrayList<ShapeLayer>();
    }
    return schoolinfomodels;
  }

  public void putShapeLayerList(ArrayList<ShapeLayer> arrayList) {
    Gson gson = new Gson();
    SharedPreferences.Editor editor = getPreferences().edit();
    String jsonCars = gson.toJson(arrayList);
    editor.putString("shapeLayer", jsonCars);
    editor.commit();
  }
}

