package com.urmis.Repository.ResponseModels;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.urmis.Repository.Models.Project.Project;

public class RMProjectList {

@SerializedName("status")
@Expose
private String status;
@SerializedName("project_list")
@Expose
private ArrayList<Project> projectList = null;

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public ArrayList<Project> getProjectList() {
return projectList;
}

public void setProjectList(ArrayList<Project> projectList) {
this.projectList = projectList;
}

}