package com.urmis.Repository.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import com.urmis.Repository.Models.News;

/**
 * Created by Brajendr on 4/11/2017.
 */

public class RMNewsList {

  @SerializedName("status")
  @Expose
  private String status;

  public ArrayList<News> getNewsArrayList() {
    return newsArrayList;
  }

  public void setNewsArrayList(ArrayList<News> newsArrayList) {
    this.newsArrayList = newsArrayList;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @SerializedName("news_list")

  @Expose
  private ArrayList<News> newsArrayList;
}
