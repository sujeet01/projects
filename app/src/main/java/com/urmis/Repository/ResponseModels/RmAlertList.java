package com.urmis.Repository.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.urmis.Repository.Models.AlertContainer;

import java.util.ArrayList;

/**
 * Created by KaranVeer on 16-05-2017.
 */

public class RmAlertList {
    @SerializedName("status")
    @Expose
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<AlertContainer> getAlertContainerArrayList() {
        return alertContainerArrayList;
    }

    public void setAlertContainerArrayList(ArrayList<AlertContainer> alertContainerArrayList) {
        this.alertContainerArrayList = alertContainerArrayList;
    }

    @SerializedName("catchment_list")
    @Expose
    private ArrayList<AlertContainer> alertContainerArrayList;
}
