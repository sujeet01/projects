package com.urmis.Repository.Models.Shape;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Created by Brajendr on 4/14/2017.
 */

public class MShape {
  private int shapeType;

  private Polygon polygon;

  private PolylineOptions polylineOptions;

  private PolygonOptions polygonOptions;

  private MarkerOptions markerOptions;

  public PolylineOptions getPolylineOptions() {
    return polylineOptions;
  }

  public void setPolylineOptions(PolylineOptions polylineOptions) {
    this.polylineOptions = polylineOptions;
  }

  public PolygonOptions getPolygonOptions() {
    return polygonOptions;
  }

  public void setPolygonOptions(PolygonOptions polygonOptions) {
    this.polygonOptions = polygonOptions;
  }

  public MarkerOptions getMarkerOptions() {
    return markerOptions;
  }

  public void setMarkerOptions(MarkerOptions markerOptions) {
    this.markerOptions = markerOptions;
  }

  public Polyline getPolyline() {
    return polyline;
  }

  public void setPolyline(Polyline polyline) {
    this.polyline = polyline;
  }

  public Marker getMarker() {
    return marker;
  }

  public void setMarker(Marker marker) {
    this.marker = marker;
  }

  public MShape() {
  }

  private Marker marker;

  private Polyline polyline;

  public Polygon getPolygon() {
    return polygon;
  }

  public void setPolygon(Polygon polygon) {
    this.polygon = polygon;
  }

  public int getShapeType() {
    return shapeType;
  }

  public void setShapeType(int shapeType) {
    this.shapeType = shapeType;
  }
}
