package com.urmis.Repository.Models.EditProgress;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ResponseDataForApprove implements Parcelable {

    @SerializedName("monthly_progress_id")
    private String monthly_progress_id;

    @SerializedName("financialvalue")
    private String financialvalue;

    @SerializedName("physicalvalue")
    private String physicalvalue;

    @SerializedName("financialdate")
    private String financialdate;

    @SerializedName("physicaldate")
    private String physicaldate;

    public String getMonthlyProgressId() {
        return monthly_progress_id;
    }

    public void setMonthlyProgressId(String monthly_progress_id) {
        this.monthly_progress_id = monthly_progress_id;
    }

    public String getFinancialvalue() {
        return financialvalue;
    }

    public void setFinancialvalue(String financialvalue) {
        this.financialvalue = financialvalue;
    }

    public String getPhysicalvalue() {
        return physicalvalue;
    }

    public void setPhysicalvalue(String physicalvalue) {
        this.physicalvalue = physicalvalue;
    }

    public String getFinancialdate() {
        return financialdate;
    }

    public void setFinancialdate(String financialdate) {
        this.financialdate = financialdate;
    }

    public String getPhysicaldate() {
        return physicaldate;
    }

    public void setPhysicaldate(String physicaldate) {
        this.physicaldate = physicaldate;
    }
    @Override
    public String toString(){
        return
                "ResponseEditProgress{" +
                        "monthly_progress_id = '" + monthly_progress_id + '\'' +
                        ",financialvalue = '" + financialvalue + '\'' +
                        ",physicaldate = '" + physicaldate + '\'' +
                        ",financialdate = '" + financialdate + '\'' +
                        ",physicalvalue = '" + physicalvalue + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
    protected ResponseDataForApprove(Parcel in) {


        this.monthly_progress_id = in.readString();
        this.financialvalue = in.readString();
        this.physicalvalue = in.readString();
        this.financialdate = in.readString();
        this.physicaldate = in.readString();
    }


    public static final Parcelable.Creator<ResponseDataForApprove> CREATOR = new Parcelable.Creator<ResponseDataForApprove>() {
        @Override
        public ResponseDataForApprove createFromParcel(Parcel source) {
            return new ResponseDataForApprove(source);
        }

        @Override
        public ResponseDataForApprove[] newArray(int size) {
            return new ResponseDataForApprove[ size];
        }

    };
}
