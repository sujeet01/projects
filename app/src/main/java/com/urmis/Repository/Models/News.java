package com.urmis.Repository.Models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Brajendr on 4/11/2017.
 */

public class News implements Parcelable {

  @SerializedName("title")
  @Expose
  private String news;

  @SerializedName("author")
  @Expose
  private String author;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  @SerializedName("newsData")
  @Expose

  private String description;

  public String getNews() {
    return news;
  }

  public void setNews(String news) {
    this.news = news;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  @SerializedName("date")
  @Expose
  private String date;

  public News() {
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.news);
    dest.writeString(this.author);
    dest.writeString(this.description);
    dest.writeString(this.date);
  }

  protected News(Parcel in) {
    this.news = in.readString();
    this.author = in.readString();
    this.description = in.readString();
    this.date = in.readString();
  }

  public static final Creator<News> CREATOR = new Creator<News>() {
    @Override public News createFromParcel(Parcel source) {
      return new News(source);
    }

    @Override public News[] newArray(int size) {
      return new News[size];
    }
  };
}
