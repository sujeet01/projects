package com.urmis.Repository.Models.EditProgress;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApproveProjectMonthlyProgress {

    @SerializedName("projectid")
    @Expose
    private String projectid;

    @SerializedName("monthlyprogressid")
    @Expose
    private String monthlyprogressid;

    public String getProjectid() {
        return projectid;
    }

    public void setProjectid(String projectid) {
        this.projectid = projectid;
    }

    public String getMonthlyprogressid() {
        return monthlyprogressid;
    }

    public void setMonthlyprogressid(String monthlyprogressid) {
        this.monthlyprogressid = monthlyprogressid;
    }
}
