package com.urmis.Repository.Models.Model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class GetScenarioResults implements Parcelable {

	@SerializedName("50 Year (m)")
	private String jsonMember50YearM;

	@SerializedName("100 Year (m^3/sec)")
	private String jsonMember100YearM3Sec;

	@SerializedName("Chainage")
	private String chainage;

	@SerializedName("25 Year (m)")
	private String jsonMember25YearM;

	@SerializedName("Water Level (m)")
	private String waterLevelM;

	@SerializedName("25 Year (m^3/sec)")
	private String jsonMember25YearM3Sec;

	@SerializedName("Discharge (m^3/sec)")
	private String dischargeM3Sec;

	@SerializedName("Duration (minutes)")
	private String durationMinutes;

	@SerializedName("50 Year (m^3/sec)")
	private String jsonMember50YearM3Sec;

	@SerializedName("100 Year (m)")
	private String jsonMember100YearM;

	@SerializedName("HFL-June 2013(m)")
	private String hflJune2013M;

	public void setJsonMember50YearM(String jsonMember50YearM){
		this.jsonMember50YearM = jsonMember50YearM;
	}

	public String getJsonMember50YearM(){
		return jsonMember50YearM;
	}

	public void setJsonMember100YearM3Sec(String jsonMember100YearM3Sec){
		this.jsonMember100YearM3Sec = jsonMember100YearM3Sec;
	}

	public String getJsonMember100YearM3Sec(){
		return jsonMember100YearM3Sec;
	}

	public void setChainage(String chainage){
		this.chainage = chainage;
	}

	public String getChainage(){
		return chainage;
	}

	public void setJsonMember25YearM(String jsonMember25YearM){
		this.jsonMember25YearM = jsonMember25YearM;
	}

	public String getJsonMember25YearM(){
		return jsonMember25YearM;
	}

	public void setWaterLevelM(String waterLevelM){
		this.waterLevelM = waterLevelM;
	}

	public String getWaterLevelM(){
		return waterLevelM;
	}

	public void setJsonMember25YearM3Sec(String jsonMember25YearM3Sec){
		this.jsonMember25YearM3Sec = jsonMember25YearM3Sec;
	}

	public String getJsonMember25YearM3Sec(){
		return jsonMember25YearM3Sec;
	}

	public void setDischargeM3Sec(String dischargeM3Sec){
		this.dischargeM3Sec = dischargeM3Sec;
	}

	public String getDischargeM3Sec(){
		return dischargeM3Sec;
	}

	public void setDurationMinutes(String durationMinutes){
		this.durationMinutes = durationMinutes;
	}

	public String getDurationMinutes(){
		return durationMinutes;
	}

	public void setJsonMember50YearM3Sec(String jsonMember50YearM3Sec){
		this.jsonMember50YearM3Sec = jsonMember50YearM3Sec;
	}

	public String getJsonMember50YearM3Sec(){
		return jsonMember50YearM3Sec;
	}

	public void setJsonMember100YearM(String jsonMember100YearM){
		this.jsonMember100YearM = jsonMember100YearM;
	}

	public String getJsonMember100YearM(){
		return jsonMember100YearM;
	}

	public void setHflJune2013M(String hflJune2013M){
		this.hflJune2013M = hflJune2013M;
	}

	public String getHflJune2013M(){
		return hflJune2013M;
	}

	@Override
 	public String toString(){
		return 
			"GetScenarioResults{" +
			"50 Year (m) = '" + jsonMember50YearM + '\'' + 
			",100 Year (m^3/sec) = '" + jsonMember100YearM3Sec + '\'' + 
			",chainage = '" + chainage + '\'' + 
			",25 Year (m) = '" + jsonMember25YearM + '\'' + 
			",water Level (m) = '" + waterLevelM + '\'' + 
			",25 Year (m^3/sec) = '" + jsonMember25YearM3Sec + '\'' + 
			",discharge (m^3/sec) = '" + dischargeM3Sec + '\'' + 
			",duration (minutes) = '" + durationMinutes + '\'' + 
			",50 Year (m^3/sec) = '" + jsonMember50YearM3Sec + '\'' + 
			",100 Year (m) = '" + jsonMember100YearM + '\'' +
					",HFL-June 2013(m) = '" + hflJune2013M + '\'' +
					"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.jsonMember50YearM);
		dest.writeString(this.jsonMember100YearM3Sec);
		dest.writeString(this.chainage);
		dest.writeString(this.jsonMember25YearM);
		dest.writeString(this.waterLevelM);
		dest.writeString(this.jsonMember25YearM3Sec);
		dest.writeString(this.dischargeM3Sec);
		dest.writeString(this.durationMinutes);
		dest.writeString(this.jsonMember50YearM3Sec);
		dest.writeString(this.jsonMember100YearM);
		dest.writeString(this.hflJune2013M);


	}
	protected GetScenarioResults(Parcel in) {
		this.jsonMember50YearM = in.readString();
		this.jsonMember100YearM3Sec = in.readString();
		this.chainage = in.readString();
		this.jsonMember25YearM = in.readString();
		this.waterLevelM = in.readString();
		this.jsonMember25YearM3Sec = in.readString();
		this.dischargeM3Sec = in.readString();
		this.durationMinutes = in.readString();
		this.jsonMember50YearM3Sec = in.readString();
		this.jsonMember100YearM = in.readString();
		this.hflJune2013M = in.readString();


	}
	public static final Creator<GetScenarioResults> CREATOR = new Creator<GetScenarioResults>() {
		@Override
		public GetScenarioResults createFromParcel(Parcel source) {
			return new GetScenarioResults(source);
		}

		@Override
		public GetScenarioResults[] newArray(int size) {
			return new GetScenarioResults[size];
		}
	};
}