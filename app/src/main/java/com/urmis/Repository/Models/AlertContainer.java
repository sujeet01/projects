package com.urmis.Repository.Models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * Created by Brajendr on 5/8/2017.
 */

public class AlertContainer implements Parcelable {

  @SerializedName("catchmentname")
  @Expose
  private String name;

  public ArrayList<Alert> getAlerts() {
    return alerts;
  }

  public void setAlerts(ArrayList<Alert> alerts) {
    this.alerts = alerts;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @SerializedName("alert_list")

  @Expose
  private ArrayList<Alert> alerts;

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.name);
    dest.writeTypedList(this.alerts);
  }

  public AlertContainer() {
  }

  protected AlertContainer(Parcel in) {
    this.name = in.readString();
    this.alerts = in.createTypedArrayList(Alert.CREATOR);
  }

  public static final Parcelable.Creator<AlertContainer> CREATOR =
      new Parcelable.Creator<AlertContainer>() {
        @Override public AlertContainer createFromParcel(Parcel source) {
          return new AlertContainer(source);
        }

        @Override public AlertContainer[] newArray(int size) {
          return new AlertContainer[size];
        }
      };
}
