package com.urmis.Repository.Models.Model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class GetScenario implements Parcelable {

	@SerializedName("id")
	@Expose
	private String stationId;
	@SerializedName("scenario")
	@Expose
	private String scenario;

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public String getStationId() {
		return stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    @SerializedName("password")
//    @Expose
//    private String password;

	public GetScenario()
	{

	}
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.stationId);
		dest.writeString(this.scenario);
	}

	protected GetScenario(Parcel in) {
		this.stationId = in.readString();
		this.scenario = in.readString();
	}

	public static final Creator<GetScenario> CREATOR = new Creator<GetScenario>() {
		@Override
		public GetScenario createFromParcel(Parcel source) {
			return new GetScenario(source);
		}

		@Override
		public GetScenario[] newArray(int size) {
			return new GetScenario[size];
		}
	};
}
