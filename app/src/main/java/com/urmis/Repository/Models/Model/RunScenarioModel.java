package com.urmis.Repository.Models.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RunScenarioModel implements Parcelable {

    @SerializedName("result")
    @Expose
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.result);

    }

    protected RunScenarioModel(Parcel in) {
        this.result = in.readString();

    }

    public static final Creator<RunScenarioModel> CREATOR = new Creator<RunScenarioModel>() {
        @Override
        public RunScenarioModel createFromParcel(Parcel source) {
            return new RunScenarioModel(source);
        }

        @Override
        public RunScenarioModel[] newArray(int size) {
            return new RunScenarioModel[size];
        }
    };
}
