package com.urmis.Repository.Models.EditProgress;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ResponseUserTypeForProgress implements Parcelable{

    @SerializedName("iscompleted")
    private String iscompleted;

    @SerializedName("project_creator_id")
    private String project_creator_id;

    @SerializedName("project_handler_id")
    private String project_handler_id;

    @SerializedName("project_approver_id")
    private String project_approver_id;

    public String getIscompleted() {
        return iscompleted;
    }

    public void setIscompleted(String iscompleted) {
        this.iscompleted = iscompleted;
    }

    public String getProject_creator_id() {
        return project_creator_id;
    }

    public void setProject_creator_id(String project_creator_id) {
        this.project_creator_id = project_creator_id;
    }

    public String getProject_handler_id() {
        return project_handler_id;
    }

    public void setProject_handler_id(String project_handler_id) {
        this.project_handler_id = project_handler_id;
    }

    public String getProject_approver_id() {
        return project_approver_id;
    }

    public void setProject_approver_id(String project_approver_id) {
        this.project_approver_id = project_approver_id;
    }

    @Override
    public String toString(){
        return
                "ResponseEditProgress{" +
                        "iscompleted = '" + iscompleted + '\'' +
                        ",project_creator_id = '" + project_creator_id + '\'' +
                        ",project_handler_id = '" + project_handler_id + '\'' +
                        ",project_approver_id = '" + project_approver_id + '\'' +

                        "}";
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
    protected ResponseUserTypeForProgress(Parcel in) {


        this.iscompleted = in.readString();
        this.project_creator_id = in.readString();
        this.project_handler_id = in.readString();
        this.project_approver_id = in.readString();

    }

    public static final Parcelable.Creator<ResponseUserTypeForProgress> CREATOR = new Parcelable.Creator<ResponseUserTypeForProgress>() {
        @Override
        public ResponseUserTypeForProgress createFromParcel(Parcel source) {
            return new ResponseUserTypeForProgress(source);
        }

        @Override
        public ResponseUserTypeForProgress[] newArray(int size) {
            return new ResponseUserTypeForProgress[ size];
        }

    };
}
