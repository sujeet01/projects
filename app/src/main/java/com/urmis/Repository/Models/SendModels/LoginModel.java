package com.urmis.Repository.Models.SendModels;

        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

/**
 * Created by Brajendr on 4/13/2017.
 */

public class LoginModel {

  @SerializedName("email")
  @Expose
  private String email;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @SerializedName("password")
  @Expose
  private String password;
}
