package com.urmis.Repository.Models.Progress;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by KaranVeer on 16-05-2017.
 */

public class NewProgressWorkStatement implements Parcelable {



    @SerializedName("fund_sanctioned") @Expose private String fund_sanctioned;
    @SerializedName("cumulative_financial_progress") @Expose private String cumulative_financial_progress;
    @SerializedName("total_physical_progress") @Expose private String total_physical_progress;
    @SerializedName("cumulative_physical_progress") @Expose private String cumulative_physical_progress;
    @SerializedName("approvedfinancial") @Expose private String approvedfinancial;
    @SerializedName("approvedphysical") @Expose private String approvedphysical;



    public String getFundSanctioned() {
        return fund_sanctioned;
    }

    public void setFundSanctioned(String fund_sanctioned) {
        this.fund_sanctioned = fund_sanctioned;
    }

    public String getCumulativeFinancialProgress() {
        return cumulative_financial_progress;
    }

    public void setCumulativeFinancialProgress(String cumulative_financial_progress) {
        this.cumulative_financial_progress = cumulative_financial_progress;
    }

    public String getTotalPhysicalProgress() {
        return total_physical_progress;
    }

    public void setTotalPhysicalProgress(String total_physical_progress) {
        this.total_physical_progress = total_physical_progress;
    }

    public String getCumulativePhysicalProgress() {
        return cumulative_physical_progress;
    }

    public void setCumulativePhysicalProgress(String cumulative_physical_progress) {
        this.cumulative_physical_progress = cumulative_physical_progress;
    }

    public String getApprovedfinancial() {
        return approvedfinancial;
    }

    public void setApprovedphysical(String approvedfinancial) {
        this.approvedfinancial = approvedfinancial;
    }

    public String getApprovedphysical() {
        return approvedphysical;
    }

    public void setAppprovedphysical(String approvedphysical) {
        this.approvedphysical = approvedphysical;
    }


    @Override
    public String toString() {
        return fund_sanctioned;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fund_sanctioned);
        dest.writeString(this.cumulative_financial_progress);
        dest.writeString(this.total_physical_progress);
        dest.writeString(this.cumulative_physical_progress);
        dest.writeString(this.approvedfinancial);
        dest.writeString(this.approvedphysical);
    }

    public NewProgressWorkStatement() {
    }

    protected NewProgressWorkStatement(Parcel in) {
        this.fund_sanctioned = in.readString();
        this.cumulative_financial_progress = in.readString();
        this.total_physical_progress = in.readString();
        this.cumulative_physical_progress = in.readString();
        this.approvedfinancial = in.readString();
        this.approvedphysical = in.readString();
//        this.progressWorkItemArrayList = new ArrayList<ProgressWorkItem>();
//        in.readList(this.progressWorkItemArrayList, ProgressWorkItem.class.getClassLoader());
    }

    public static final Parcelable.Creator<ProgressWorkStatement> CREATOR = new Parcelable.Creator<ProgressWorkStatement>() {
        @Override
        public ProgressWorkStatement createFromParcel(Parcel source) {
            return new ProgressWorkStatement(source);
        }

        @Override
        public ProgressWorkStatement[] newArray(int size) {
            return new ProgressWorkStatement[size];
        }
    };
}
