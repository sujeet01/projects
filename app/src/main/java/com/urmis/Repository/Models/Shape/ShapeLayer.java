package com.urmis.Repository.Models.Shape;

import android.graphics.Color;
import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;

import com.urmis.Utils.Colors.Colors;
import com.urmis.Utils.Constants;
import com.urmis.Utils.Utility;

/**
 * Created by Brajendr on 4/6/2017.
 */

public class ShapeLayer implements Serializable {

  private LatLng centreLatLong;
  private String color= Colors.getRandomColor();

  public ShapeLayer() {
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
    for (int i = 0; i < mShapeArrayList.size(); i++) {
      MShape mShape = mShapeArrayList.get(i);
      int shapeType = mShape.getShapeType();
      switch (shapeType) {
        case Constants.POINT:
          //mShape.getMarkerOptions().icon(Utility.getMarkerIcon(color));
          if(mShape.getMarker()!=null)
          mShape.getMarker().setIcon(Utility.getMarkerIcon(color));
          break;
        case Constants.POLYLINE:
          //mShape.getPolylineOptions().color(Color.parseColor(color));
          if(mShape.getMarker()!=null)
          mShape.getPolyline().setColor(Color.parseColor(color));
          break;
        case Constants.POLYGON:
          //mShape.getPolygonOptions().fillColor(Color.parseColor(color));
          if(mShape.getMarker()!=null)
          mShape.getPolygon().setFillColor(Color.parseColor(color));
          break;
      }
    }
  }

  public LatLng getCentreLatLong() {
    return centreLatLong;
  }

  public void setCentreLatLong(LatLng centreLatLong) {
    this.centreLatLong = centreLatLong;
  }

  public boolean isVisibility() {
    return visibility;
  }

  public void setVisibility(boolean visibility) {
    this.visibility = visibility;
    for (int i = 0; i < mShapeArrayList.size(); i++) {
      MShape mShape = mShapeArrayList.get(i);
      int shapeType = mShape.getShapeType();
      switch (shapeType) {
        case Constants.POINT:
          mShape.getMarker().setVisible(visibility);
          break;
        case Constants.POLYLINE:
          mShape.getPolyline().setVisible(visibility);
          break;
        case Constants.POLYGON:
          mShape.getPolygon().setVisible(visibility);
          break;
      }
    }
  }

  boolean visibility = false;

  ArrayList<MShape> mShapeArrayList = new ArrayList<>();

  public ArrayList<MShape> getmShapeArrayList() {
    return mShapeArrayList;
  }

  public void setmShapeArrayList(ArrayList<MShape> mShapeArrayList) {
    this.mShapeArrayList = mShapeArrayList;
  }
}
