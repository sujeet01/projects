package com.urmis.Repository.Models.Project;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalientFeature implements Parcelable {

  @SerializedName("attributename") @Expose private String attribute;

  @SerializedName("attributevalue") @Expose private String value;

  public String getAttribute() {
    return attribute;
  }

  public void setAttribute(String attribute) {
    this.attribute = attribute;
  }


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.attribute);
    dest.writeString(this.value);
  }

  public SalientFeature() {
  }

  protected SalientFeature(Parcel in) {
    this.attribute = in.readString();
    this.value = in.readString();
  }

  public static final Parcelable.Creator<SalientFeature> CREATOR =
      new Parcelable.Creator<SalientFeature>() {
        @Override public SalientFeature createFromParcel(Parcel source) {
          return new SalientFeature(source);
        }

        @Override public SalientFeature[] newArray(int size) {
          return new SalientFeature[size];
        }
      };
}
