package com.urmis.Repository.Models.Shape;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Brajendr on 4/17/2017.
 */

public class ShapeFileList implements Parcelable {

  private String name;
  private String color;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isChecked() {
    return isChecked;
  }

  public void setChecked(boolean checked) {
    isChecked = checked;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  private boolean isChecked=true;

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.name);
    dest.writeString(this.color);
    dest.writeByte(this.isChecked ? (byte) 1 : (byte) 0);
  }

  public ShapeFileList() {
  }

  protected ShapeFileList(Parcel in) {
    this.name = in.readString();
    this.color = in.readString();
    this.isChecked = in.readByte() != 0;
  }

  public static final Parcelable.Creator<ShapeFileList> CREATOR =
      new Parcelable.Creator<ShapeFileList>() {
        @Override public ShapeFileList createFromParcel(Parcel source) {
          return new ShapeFileList(source);
        }

        @Override public ShapeFileList[] newArray(int size) {
          return new ShapeFileList[size];
        }
      };
}
