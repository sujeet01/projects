package com.urmis.Repository.Models.Project;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListOfWorkStatement implements Parcelable {

@SerializedName("workstatementname") @Expose private String tittle;

@SerializedName("workstatementvalue") @Expose private String value;

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @SerializedName("work_items")
@Expose
private List<Workitems> workItems = null;

public String getTittle() {
return tittle;
}

public void setTittle(String tittle) {
this.tittle = tittle;
}

  public List<Workitems> getWorkItems() {
    return workItems;
  }

  public void setWorkItems(List<Workitems> workItems) {
    this.workItems = workItems;
  }

  public ListOfWorkStatement() {
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.tittle);
    dest.writeString(this.value);
   // dest.writeTypedList(this.workItems);
  }

  protected ListOfWorkStatement(Parcel in) {
    this.tittle = in.readString();
    this.value = in.readString();
    //this.workItems = in.createTypedArrayList(Workitems.CREATOR);
  }

  public static final Creator<ListOfWorkStatement> CREATOR = new Creator<ListOfWorkStatement>() {
    @Override public ListOfWorkStatement createFromParcel(Parcel source) {
      return new ListOfWorkStatement(source);
    }

    @Override public ListOfWorkStatement[] newArray(int size) {
      return new ListOfWorkStatement[size];
    }
  };
}