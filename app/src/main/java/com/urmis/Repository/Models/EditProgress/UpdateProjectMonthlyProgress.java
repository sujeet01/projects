package com.urmis.Repository.Models.EditProgress;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateProjectMonthlyProgress {

    @SerializedName("projectid")
    @Expose
    private String projectid;

    @SerializedName("monthlyprogressid")
    @Expose
    private String monthlyprogressid;

    @SerializedName("valuefinancial")
    @Expose
    private String valuefinancial;

    @SerializedName("valuephysical")
    @Expose
    private String valuephysical;

    @SerializedName("datefinancial")
    @Expose
    private String datefinancial;

    @SerializedName("datephysical")
    @Expose
    private String datephysical;

    public String getProjectid() {
        return projectid;
    }

    public void setProjectid(String projectid) {
        this.projectid = projectid;
    }

    public String getMonthlyprogressid() {
        return monthlyprogressid;
    }

    public void setMonthlyprogressid(String monthlyprogressid) {
        this.monthlyprogressid = monthlyprogressid;
    }

    public String getValuefinancial() {
        return valuefinancial;
    }

    public void setValuefinancial(String valuefinancial) {
        this.valuefinancial = valuefinancial;
    }

    public String getValuephysical() {
        return valuephysical;
    }

    public void setValuephysical(String valuephysical) {
        this.valuephysical = valuephysical;
    }

    public String getDatefinancial() {
        return datefinancial;
    }

    public void setDatefinancial(String datefinancial) {
        this.datefinancial = datefinancial;
    }

    public String getDatephysical() {
        return datephysical;
    }

    public void setDatephysical(String datephysical) {
        this.datephysical = datephysical;
    }
}
