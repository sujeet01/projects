package com.urmis.Repository.Models.Project;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Brajendr on 4/28/2017.
 */

public class Workitems implements Parcelable {
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @SerializedName("name")
  @Expose

  private String name;


  @SerializedName("quantity")
  @Expose

  private String quantity;

  public String getRate() {
    return rate;
  }

  public void setRate(String rate) {
    this.rate = rate;
  }

  public String getQuantity() {
    return quantity;
  }

  public void setQuantity(String quantity) {
    this.quantity = quantity;
  }

  @SerializedName("rate")

  @Expose

  private String rate;


  public Workitems() {
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.name);
    dest.writeString(this.quantity);
    dest.writeString(this.rate);
  }

  protected Workitems(Parcel in) {
    this.name = in.readString();
    this.quantity = in.readString();
    this.rate = in.readString();
  }

  public static final Creator<Workitems> CREATOR = new Creator<Workitems>() {
    @Override
    public Workitems createFromParcel(Parcel source) {
      return new Workitems(source);
    }

    @Override
    public Workitems[] newArray(int size) {
      return new Workitems[size];
    }
  };
}
