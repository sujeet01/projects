package com.urmis.Repository.Models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Brajendr on 4/10/2017.
 */

public class User implements Parcelable {

  @SerializedName("userId") @Expose private String user_id;

  @SerializedName("status") @Expose private String status="";

  @SerializedName("message") @Expose private String message;

  @SerializedName("designationId") @Expose private String designationId;

  @SerializedName("designationName") @Expose private String designationName;

  private String emailId;
  private boolean isLoggedIn = false;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getDesignationId() {
    return designationId;
  }

  public void setDesignationId(String designationId) {
    this.designationId = designationId;
  }

  public String getDesignationName() {
    return designationName;
  }

  public void setDesignationName(String designationName) {
    this.designationName = designationName;
  }



  public String getEmailId() {
    return emailId;
  }

  public void setEmailId(String emailId) {
    this.emailId = emailId;
  }

  public boolean isLoggedIn() {
    return isLoggedIn;
  }

  public void setLoggedIn(boolean loggedIn) {
    isLoggedIn = loggedIn;
  }



  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.user_id);
    dest.writeString(this.status);
    dest.writeString(this.message);
    dest.writeByte(this.isLoggedIn ? (byte) 1 : (byte) 0);
    dest.writeString(this.emailId);
    dest.writeString(this.designationId);
    dest.writeString(this.designationName);
  }

  public User() {
  }

  protected User(Parcel in) {
    this.user_id = in.readString();
    this.status = in.readString();
    this.message = in.readString();
    this.isLoggedIn = in.readByte() != 0;
    this.emailId = in.readString();
    this.designationId = in.readString();
    this.designationName = in.readString();
  }

  public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
    @Override public User createFromParcel(Parcel source) {
      return new User(source);
    }

    @Override public User[] newArray(int size) {
      return new User[size];
    }
  };
}
