package com.urmis.Repository.Models.supervisorList;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class userSupervisorList implements Parcelable {

	@SerializedName("designationname")
	private String designationname;

	@SerializedName("designationid")
	private String designationid;

	@SerializedName("userid")
	private String userid;

	@SerializedName("username")
	private String username;


	public void setDesignationname(String designationname){
		this.designationname = designationname;
	}

	public String getDesignationname(){
		return designationname;
	}

	public void setDesignationid(String designationid){
		this.designationid = designationid;
	}

	public String getDesignationid(){
		return designationid;
	}

	public void setUserid(String userid){
		this.userid = userid;
	}

	public String getUserid(){
		return userid;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	public userSupervisorList()
	{

	}
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int i) {
		dest.writeString(this.designationname);
		dest.writeString(this.designationid);
		dest.writeString(this.userid);
		dest.writeString(this.username);

	}

	protected userSupervisorList(Parcel in) {
		designationname = in.readString();
		designationid = in.readString();
		userid = in.readString();
		username = in.readString();
	}

	public static final Creator<userSupervisorList> CREATOR = new Creator<userSupervisorList>() {
		@Override
		public userSupervisorList createFromParcel(Parcel in) {
			return new userSupervisorList(in);
		}

		@Override
		public userSupervisorList[] newArray(int size) {
			return new userSupervisorList[size];
		}
	};

}