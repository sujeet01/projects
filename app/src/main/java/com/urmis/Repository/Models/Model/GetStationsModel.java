package com.urmis.Repository.Models.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.urmis.Repository.Models.Project.Image;
import com.urmis.Repository.Models.Project.ListOfWorkStatement;
import com.urmis.Repository.Models.Project.Project;
import com.urmis.Repository.Models.Project.SalientFeature;

import java.util.ArrayList;


/**
 * Created by Brajendr on 4/13/2017.
 */

public class GetStationsModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String stationId;
    @SerializedName("location")
    @Expose
    private String stationName;

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    @SerializedName("password")
//    @Expose
//    private String password;

    public GetStationsModel()
    {

    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.stationId);
        dest.writeString(this.stationName);
    }

    protected GetStationsModel(Parcel in) {
        this.stationId = in.readString();
        this.stationName = in.readString();
    }

    public static final Creator<GetStationsModel> CREATOR = new Creator<GetStationsModel>() {
        @Override
        public GetStationsModel createFromParcel(Parcel source) {
            return new GetStationsModel(source);
        }

        @Override
        public GetStationsModel[] newArray(int size) {
            return new GetStationsModel[size];
        }
    };
}
