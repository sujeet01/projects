package com.urmis.Repository.Models.Project;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Project implements Parcelable {

  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("district")
  @Expose
  private String district;
  @SerializedName("river")
  @Expose
  private String river;
  @SerializedName("percentage_completed")
  @Expose
  private String percentage_completed;
  @SerializedName("river_bank")
  @Expose
  private String riverBank;
  @SerializedName("tac_approved")
  @Expose
  private String tacApproved;

  @SerializedName("project_creator")
  @Expose
  private String project_creator;

  @SerializedName("list_of_project_accessors")
  @Expose
  private ArrayList<String> list_of_project_accessors;
  public ArrayList<String> getList_of_project_accessors() {
    return list_of_project_accessors;
  }
  public void setList_of_project_accessors(ArrayList<String> list_of_project_accessors) {
    this.list_of_project_accessors = list_of_project_accessors;
  }

  @SerializedName("progress_approver")
  @Expose
  private String progress_approver;
  public String getProgress_approver() { return progress_approver; }
  public void setProgress_approver(String progress_approver) { this.progress_approver = progress_approver; }

  @SerializedName("progress_approver_name")
  @Expose
  private String progress_approver_name;
  public String getProgress_approver_name() { return progress_approver_name; }
  public void setProgress_approver_name(String progress_approver_name) { this.progress_approver_name = progress_approver_name; }

  @SerializedName("progress_approver_designation")
  @Expose
  private String progress_approver_designation;
  public String getProgress_approver_designation() { return progress_approver_designation; }
  public void setProgress_approver_designation(String progress_approver_designation) { this.progress_approver_designation = progress_approver_designation; }

  @SerializedName("list_of_progress_approver_after_progressenter")
  @Expose
  private ArrayList<String> list_of_progress_approver_after_progressenter=null;
  public ArrayList<String> getlist_of_progress_approver_after_progressenter() {
    return list_of_progress_approver_after_progressenter;
  }
  public void setlist_of_progress_approver_after_progressenter(ArrayList<String> list_of_progress_approver_after_progressenter) {
    this.list_of_progress_approver_after_progressenter = list_of_progress_approver_after_progressenter;
  }

  @SerializedName("list_of_work_statement")
  @Expose
  private List<ListOfWorkStatement> listOfWorkStatement = null;
  public List<ListOfWorkStatement> getListOfWorkStatement() {
    return listOfWorkStatement;
  }

  public void setListOfWorkStatement(List<ListOfWorkStatement> listOfWorkStatement) {
    this.listOfWorkStatement = listOfWorkStatement;
  }


  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getCompetionDate() {
    return competionDate;
  }

  public void setCompetionDate(String competionDate) {
    this.competionDate = competionDate;
  }

  public ArrayList<String> getDateList() {
    return dateList;
  }

  public void setDateList(ArrayList<String> dateList) {
    this.dateList = dateList;
  }

  @SerializedName("fund_sanctioned")

  @Expose
  private String fundSanction;
  @SerializedName("start_date")
  @Expose
  private String startDate;
  @SerializedName("completion_date")
  @Expose
  private String competionDate;
  @SerializedName("date_list")
  @Expose
  private ArrayList<String> dateList;
  @SerializedName("fund_consumed")
  @Expose
  private String fundConsumed;
  @SerializedName("list_of_images")
  @Expose
  private List<Image> images = null;
  @SerializedName("salient_features")
  @Expose
  private List<SalientFeature> salientFeatures = null;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDistrict() {
    return district;
  }

  public void setDistrict(String district) {
    this.district = district;
  }

  public String getRiver() {
    return river;
  }

  public void setRiver(String river) {
    this.river = river;
  }

  public String getProject_creator() {
    return project_creator;
  }

  public void setProject_creator(String project_creator) {
    this.project_creator = project_creator;
  }

  public String getPercentage_completed() {
    return percentage_completed;
  }

  public void setPercentage_completed(String percentage_completed) {
    this.percentage_completed = percentage_completed;
  }

  public String getRiverBank() {
    return riverBank;
  }

  public void setRiverBank(String riverBank) {
    this.riverBank = riverBank;
  }

  public String getTacApproved() {
    return tacApproved;
  }

  public void setTacApproved(String tacApproved) {
    this.tacApproved = tacApproved;
  }

  public String getFundSanction() {
    return fundSanction;
  }

  public void setFundSanction(String fundSanction) {
    this.fundSanction = fundSanction;
  }

  public String getFundConsumed() {
    return fundConsumed;
  }

  public void setFundConsumed(String fundConsumed) {
    this.fundConsumed = fundConsumed;
  }

  public List<Image> getImages() {
    return images;
  }

  public void setImages(List<Image> images) {
    this.images = images;
  }

  public List<SalientFeature> getSalientFeatures() {
    return salientFeatures;
  }

  public void setSalientFeatures(List<SalientFeature> salientFeatures) {
    this.salientFeatures = salientFeatures;
  }


  public Project() {
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.id);
    dest.writeString(this.name);
    dest.writeString(this.district);
    dest.writeString(this.river);
    dest.writeString(this.percentage_completed);
    dest.writeString(this.project_creator);
    dest.writeString(this.progress_approver);
    dest.writeString(this.progress_approver_name);
    dest.writeString(this.progress_approver_designation);
    dest.writeString(this.riverBank);
    dest.writeString(this.tacApproved);
    dest.writeString(this.fundSanction);
    dest.writeString(this.startDate);
    dest.writeString(this.competionDate);
    dest.writeStringList(this.dateList);
    dest.writeString(this.fundConsumed);
    dest.writeList(this.images);
    dest.writeTypedList(this.salientFeatures);
    dest.writeTypedList(this.listOfWorkStatement);
    //dest.writeTypedList(this.list_of_progress_approver_after_progressenter);
  }

  protected Project(Parcel in) {
    this.id = in.readString();
    this.name = in.readString();
    this.district = in.readString();
    this.river = in.readString();
    this.percentage_completed = in.readString();
    this.project_creator = in.readString();
    this.progress_approver = in.readString();
    this.progress_approver_name = in.readString();
    this.progress_approver_designation = in.readString();
    this.riverBank = in.readString();
    this.tacApproved = in.readString();
    this.fundSanction = in.readString();
    this.startDate = in.readString();
    this.competionDate = in.readString();
    this.dateList = in.createStringArrayList();
    this.fundConsumed = in.readString();
    this.images = new ArrayList<Image>();
    in.readList(this.images, Image.class.getClassLoader());
    this.salientFeatures = in.createTypedArrayList(SalientFeature.CREATOR);
    this.listOfWorkStatement = in.createTypedArrayList(ListOfWorkStatement.CREATOR);

  }

  public static final Creator<Project> CREATOR = new Creator<Project>() {
    @Override
    public Project createFromParcel(Parcel source) {
      return new Project(source);
    }

    @Override
    public Project[] newArray(int size) {
      return new Project[size];
    }
  };
}