package com.urmis.Repository.Models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Brajendr on 4/13/2017.
 */

public class Alert implements Parcelable {

  @SerializedName("stationname") @Expose private String stationname;

  @SerializedName("value") @Expose private double value;

  public String getStationname() {
    return stationname;
  }

  public void setStationname(String stationname) {
    this.stationname = stationname;
  }

  public double getValue() {
    return value;
  }

  public void setValue(double value) {
    this.value = value;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  @SerializedName("color") @Expose private String color;

  public Alert() {
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.stationname);
    dest.writeDouble(this.value);
    dest.writeString(this.color);
  }

  protected Alert(Parcel in) {
    this.stationname = in.readString();
    this.value = in.readDouble();
    this.color = in.readString();
  }

  public static final Creator<Alert> CREATOR = new Creator<Alert>() {
    @Override
    public Alert createFromParcel(Parcel source) {
      return new Alert(source);
    }

    @Override
    public Alert[] newArray(int size) {
      return new Alert[size];
    }
  };
}
