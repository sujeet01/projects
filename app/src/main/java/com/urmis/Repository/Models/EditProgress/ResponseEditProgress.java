package com.urmis.Repository.Models.EditProgress;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class ResponseEditProgress implements Parcelable{

	@SerializedName("monthly_progress_id")
	private String monthlyProgressId;

	@SerializedName("financial_progress_quantity_value")
	private String financialProgressQuantityValue;

	@SerializedName("physical_progress_date")
	private String physicalProgressDate;

	@SerializedName("financial_progress_date")
	private String financialProgressDate;

	@SerializedName("progressapproved")
	private String progressapproved;

	@SerializedName("physical_progress_quantity_value")
	private String physicalProgressQuantityValue;

	public void setMonthlyProgressId(String monthlyProgressId){
		this.monthlyProgressId = monthlyProgressId;
	}

	public String getMonthlyProgressId(){
		return monthlyProgressId;
	}

	public void setFinancialProgressQuantityValue(String financialProgressQuantityValue){
		this.financialProgressQuantityValue = financialProgressQuantityValue;
	}

	public String getFinancialProgressQuantityValue(){
		return financialProgressQuantityValue;
	}

	public void setPhysicalProgressDate(String physicalProgressDate){
		this.physicalProgressDate = physicalProgressDate;
	}

	public String getPhysicalProgressDate(){
		return physicalProgressDate;
	}

	public void setFinancialProgressDate(String financialProgressDate){
		this.financialProgressDate = financialProgressDate;
	}

	public String getFinancialProgressDate(){
		return financialProgressDate;
	}

	public void setProgressapproved(String progressapproved){
		this.progressapproved = progressapproved;
	}

	public String getProgressapproved(){
		return progressapproved;
	}

	public void setPhysicalProgressQuantityValue(String physicalProgressQuantityValue){
		this.physicalProgressQuantityValue = physicalProgressQuantityValue;
	}

	public String getPhysicalProgressQuantityValue(){
		return physicalProgressQuantityValue;
	}

	@Override
 	public String toString(){
		return 
			"ResponseEditProgress{" +
			"monthly_progress_id = '" + monthlyProgressId + '\'' + 
			",financial_progress_quantity_value = '" + financialProgressQuantityValue + '\'' + 
			",physical_progress_date = '" + physicalProgressDate + '\'' + 
			",financial_progress_date = '" + financialProgressDate + '\'' + 
			",progressapproved = '" + progressapproved + '\'' + 
			",physical_progress_quantity_value = '" + physicalProgressQuantityValue + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

	}
	protected ResponseEditProgress(Parcel in) {


		this.monthlyProgressId = in.readString();
		this.financialProgressQuantityValue = in.readString();
		this.physicalProgressQuantityValue = in.readString();
		this.financialProgressDate = in.readString();
		this.physicalProgressDate = in.readString();
		this.progressapproved = in.readString();

	}

	public static final Parcelable.Creator<ResponseEditProgress> CREATOR = new Parcelable.Creator<ResponseEditProgress>() {
		@Override
		public ResponseEditProgress createFromParcel(Parcel source) {
			return new ResponseEditProgress(source);
		}

		@Override
		public ResponseEditProgress[] newArray(int size) {
			return new ResponseEditProgress[ size];
		}

	};
}