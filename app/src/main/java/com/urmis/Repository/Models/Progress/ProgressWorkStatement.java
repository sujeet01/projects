package com.urmis.Repository.Models.Progress;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by KaranVeer on 16-05-2017.
 */

public class ProgressWorkStatement implements Parcelable {
    @SerializedName("title") @Expose private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<ProgressWorkItem> getProgressWorkItemArrayList() {
        return progressWorkItemArrayList;
    }

    public void setProgressWorkItemArrayList(ArrayList<ProgressWorkItem> progressWorkItemArrayList) {
        this.progressWorkItemArrayList = progressWorkItemArrayList;
    }

    @Override
    public String toString() {
        return title;
    }

    @SerializedName("work_items")
    @Expose
    ArrayList<ProgressWorkItem> progressWorkItemArrayList;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeList(this.progressWorkItemArrayList);
    }

    public ProgressWorkStatement() {
    }

    protected ProgressWorkStatement(Parcel in) {
        this.title = in.readString();
        this.progressWorkItemArrayList = new ArrayList<ProgressWorkItem>();
        in.readList(this.progressWorkItemArrayList, ProgressWorkItem.class.getClassLoader());
    }

    public static final Parcelable.Creator<ProgressWorkStatement> CREATOR = new Parcelable.Creator<ProgressWorkStatement>() {
        @Override
        public ProgressWorkStatement createFromParcel(Parcel source) {
            return new ProgressWorkStatement(source);
        }

        @Override
        public ProgressWorkStatement[] newArray(int size) {
            return new ProgressWorkStatement[size];
        }
    };
}
