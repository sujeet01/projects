package com.urmis.Repository.Models.Progress;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by KaranVeer on 16-05-2017.
 */

public class ProgressWorkItem implements Parcelable {
    @SerializedName("workstatementname")
    @Expose
    private String name;

    @SerializedName("workstatementitemid")
    @Expose
    private String workStatementItemId;

    public String getWorkStatementId() {
        return workStatementId;
    }

    public void setWorkStatementId(String workStatementId) {
        this.workStatementId = workStatementId;
    }

    public String getWorkItemId() {
        return workItemId;
    }

    public void setWorkItemId(String workItemId) {
        this.workItemId = workItemId;
    }

    public String getWorkStatementItemId() {
        return workStatementItemId;
    }

    public void setWorkStatementItemId(String workStatementItemId) {
        this.workStatementItemId = workStatementItemId;
    }

    @SerializedName("workstatementid")
    @Expose
    private String workStatementId;

    @SerializedName("workitemid")
    @Expose
    private String workItemId;

    @Override
    public String toString() {
        return name;
    }

    @SerializedName("totalquantity")
    @Expose
    private String total;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    @SerializedName("balancequantity")
    @Expose
    private String balance;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.total);
        dest.writeString(this.balance);
    }

    public ProgressWorkItem() {
    }

    protected ProgressWorkItem(Parcel in) {
        this.name = in.readString();
        this.total = in.readString();
        this.balance = in.readString();
    }

    public static final Parcelable.Creator<ProgressWorkItem> CREATOR = new Parcelable.Creator<ProgressWorkItem>() {
        @Override
        public ProgressWorkItem createFromParcel(Parcel source) {
            return new ProgressWorkItem(source);
        }

        @Override
        public ProgressWorkItem[] newArray(int size) {
            return new ProgressWorkItem[size];
        }
    };
}
