package com.urmis.Results;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import com.urmis.Network.Callback.NwCall;
import com.urmis.Network.WebServiceCalls;
import com.urmis.R;
import com.urmis.Repository.Models.News;
import com.urmis.Utils.PopMessage;
import com.urmis.Utils.RecyclerViewTools.CommonRecyclerViewAdapter;
import com.urmis.Utils.RecyclerViewTools.ViewHolder;
import com.urmis.Utils.Utility;

/**
 * Created by Brajendr on 4/10/2017.
 */

public class OldNewsFragment extends Fragment {
    private View view;
    private RecyclerView rcNewsList;
    private ArrayList<News> newsList = new ArrayList<>();
    private boolean wasAlreadyRunOnce;
    private RelativeLayout rlLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout insLinear;

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_results, container, false);
        init();
        setUpList();
        setUpSwipeReferesh();
        if (!wasAlreadyRunOnce) {
            getNews();
            wasAlreadyRunOnce = true;
        }
        return view;
    }

    private void setUpSwipeReferesh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                newsList=new ArrayList<News>();
                setUpList();
                getNews();
            }
        });
    }

    private void getNews() {
        swipeRefreshLayout.setRefreshing(true);
        WebServiceCalls.getNews(new NwCall(rlLayout) {
            @Override public void onSuccess(String msg) {
                swipeRefreshLayout.setRefreshing(false);
                PopMessage.makeshorttoast(getActivity(), msg);
            }

            @Override public void onFailure(String msg) {

                swipeRefreshLayout.setRefreshing(false);
                PopMessage.makeshorttoast(getActivity(), msg);
                if(newsList.size()==0)
                {
                    insLinear.setVisibility(View.VISIBLE);
                }
                else
                {
                    insLinear.setVisibility(View.GONE);
                }
            }

            @Override public void onSuccess(Bundle msg) {
                swipeRefreshLayout.setRefreshing(false);
                ArrayList<News> news = msg.getParcelableArrayList("newsList");
                newsList.addAll(news);
                rcNewsList.getAdapter().notifyDataSetChanged();
                if(news.size()==0)
                {
                    insLinear.setVisibility(View.VISIBLE);
                }
                else
                {
                    insLinear.setVisibility(View.GONE);
                }
            }

            @Override public void onFailure(Bundle msg) {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void setUpList() {
        rcNewsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        CommonRecyclerViewAdapter<News> adapter =
                new CommonRecyclerViewAdapter<News>(getActivity(), newsList, R.layout.layout_news_list) {
                    @Override public void onPostBindViewHolder(ViewHolder holder, News news) {
                        holder.setViewText(R.id.tvNewsTittle, news.getNews().replace("&nbsp;"," "))
                                .setViewText(R.id.tvDate, news.getDate())
                                .setViewText(R.id.tvNewsDescription, news.getDescription().replace("&nbsp;"," "))
                                .setViewText(R.id.tvAuthor, news.getAuthor().replace("&nbsp;"," "));
                    }
                };
        adapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override public void onItemClick(View view, int position) {
                Dialog dialog = Utility.createDialog(getActivity(), R.layout.dialog_news, false);

                TextView tvTittle = (TextView) dialog.findViewById(R.id.tvNewsTittle);
                TextView tvDesc = (TextView) dialog.findViewById(R.id.tvNewsDescription);
                TextView tvDate = (TextView) dialog.findViewById(R.id.tvDate);
                TextView tvAuthor = (TextView) dialog.findViewById(R.id.tvAuthor);

                News news = newsList.get(position);
                tvTittle.setText(news.getNews().replace("&nbsp;"," "));
                tvDesc.setText(news.getDescription().replace("&nbsp;"," "));
                tvDate.setText(news.getDate());
                tvAuthor.setText(news.getAuthor().replace("&nbsp;"," "));
            }
        });
        rcNewsList.setAdapter(adapter);
    }

    private void init() {
        rlLayout=(RelativeLayout)view.findViewById(R.id.rlLoading);
        rcNewsList = (RecyclerView) view.findViewById(R.id.rcNews);
        insLinear=(LinearLayout)view.findViewById(R.id.layout_instructions);
        swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
    }
}
