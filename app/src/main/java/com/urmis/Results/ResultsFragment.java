package com.urmis.Results;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.urmis.Authenticate.LoginActivity;
import com.urmis.Network.Callback.NwCall;
import com.urmis.Network.WebServiceCalls;
import com.urmis.R;
import com.urmis.Repository.AppPrefs;
import com.urmis.Repository.Models.Model.GetScenario;
import com.urmis.Repository.Models.Model.GetScenarioResults;
import com.urmis.Repository.Models.Model.GetStationsModel;
import com.urmis.Repository.Models.News;
import com.urmis.Utils.Constants;
import com.urmis.Utils.PopMessage;

/**
 * Created by Brajendr on 4/10/2017.
 */

public class ResultsFragment extends Fragment {
  private View view;
  private RecyclerView rcNewsList;
  private ArrayList<News> newsList = new ArrayList<>();
  private boolean wasAlreadyRunOnce;
  private RelativeLayout rlLayout;
  private SwipeRefreshLayout swipeRefreshLayout;
  private LinearLayout insLinear;
  private TableLayout tableLayout1,tableLayout2;
  private Spinner scenarioSpinner,riverSpinner;
  private TextView txt_discharge_filevalue,txt_location,viewmore1,txtview1,txtview2,txtview3,txtview4,txt1,txt2,txt3,txt4,txt5,txt6,txt7,btnLogin;
    ArrayList<String> scenarioLocationResult,scenarioWaterlevelResult,scenarioDischargeResult,scenarioDurationResult,scenarioResult25year,
            scenarioResult50year,scenarioResult100year,scenarioResult25m3year,scenarioResult50m3year,scenarioResult100m3year,scenarioHflJune2013M,dischargeFileValue,location;
  LinearLayout newsll;
  TableRow tablerow2;
    float scale;
    int padding_5dp;
    TableRow row,row2;
    String riverName,scenarioName,str_dischargeFileValue,str_location;
    String[] RIVER_SPINNER_DATA = {"Alaknanda","Bhagirathi","Kali","Mandakini"};
    ArrayList<String> alaknanda_location,bhagirathi_location,kali_location,mandakini_location;
    ArrayList<GetScenarioResults> scenarioResultList;
    AppPrefs appPrefs;

    @Override
    public void onResume() {
        super.onResume();
        if(appPrefs.getUser().isLoggedIn() == true){
            newsll.setVisibility(View.VISIBLE);
            btnLogin.setVisibility(View.GONE);
        }else{
            newsll.setVisibility(View.GONE);
            btnLogin.setVisibility(View.VISIBLE);
        }
    }



    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    view = inflater.inflate(R.layout.fragment_results, container, false);

    init();
      appPrefs = AppPrefs.getInstance(getActivity());
       scale = getResources().getDisplayMetrics().density;
        padding_5dp = (int) (5 * scale + 0.5f);
    //setUpList();
    setUpSwipeReferesh();

      btnLogin.setOnClickListener(new View.OnClickListener() {
          @Override public void onClick(View view) {
              Intent intent = new Intent(getActivity(), LoginActivity.class);
              intent.putExtra("navigate", Constants.NAVIGATE_TO_NEWS);
              startActivityForResult(intent, Constants.NAVIGATE_TO_NEWS);
          }
      });



      // Spinner Drop down elements
      List<String> scenarioNames = new ArrayList<String>();
        alaknanda_location = new ArrayList<>();
        alaknanda_location.add("Badrinath");alaknanda_location.add("Khiru Village");alaknanda_location.add("Lambagad");alaknanda_location.add("Pandukeshwar");
      alaknanda_location.add("Gobindghat");alaknanda_location.add("Hathiparvat/ VishnuPrayag");alaknanda_location.add("Downstream of Pipalkoti");alaknanda_location.add("Birahi");
      alaknanda_location.add("Chinka");alaknanda_location.add("Kothiyalsain");alaknanda_location.add("Maithana");alaknanda_location.add("Deolibagad");alaknanda_location.add("Kaleshwar");alaknanda_location.add("Devli/ Jakhni");

        bhagirathi_location = new ArrayList<>();
        bhagirathi_location.add("Bharo Jhamp");bhagirathi_location.add("Sukki");bhagirathi_location.add("Jyoti");bhagirathi_location.add("Gangnani");bhagirathi_location.add("Sunagar 2");bhagirathi_location.add("Helgu");bhagirathi_location.add("Singhrali");
        bhagirathi_location.add("Pala");bhagirathi_location.add("Bhatwari/Chadethi");bhagirathi_location.add("Malla");bhagirathi_location.add("Bishanpur");bhagirathi_location.add("Laldang");bhagirathi_location.add("Aungi");bhagirathi_location.add("Silkura");
      bhagirathi_location.add("Heena");bhagirathi_location.add("Naitala");bhagirathi_location.add("Ganeshpur");bhagirathi_location.add("Garampani");bhagirathi_location.add("Gangori");
      bhagirathi_location.add("Tekhla");bhagirathi_location.add("Laksheshwar");bhagirathi_location.add("Uttarkashi Town");bhagirathi_location.add("Badethi Chungi");bhagirathi_location.add("ITBP at Matli");bhagirathi_location.add("Downstream at Matli");bhagirathi_location.add("Dunda Village");bhagirathi_location.add("Dharasu Bend");bhagirathi_location.add("Dharasu Bridge Spot");

    kali_location = new ArrayList<>();
    kali_location.add("Tawaghat");kali_location.add("Chitakot");kali_location.add("Elagaad");kali_location.add("Dobat Road Section 1");kali_location.add("Dobat Road Section 2");kali_location.add("Khotila");
      kali_location.add("Dharchula Kasba");kali_location.add("Nigalpani");kali_location.add("Gothi");kali_location.add("Kalika");kali_location.add("Nayabsti");
      kali_location.add("Mallachharchhum");kali_location.add("Baluwakot");kali_location.add("Ghatibagarh");kali_location.add("Jauljibi");

      mandakini_location = new ArrayList<>();
      mandakini_location.add("Kedarnath");mandakini_location.add("Gaurikund");mandakini_location.add("Sonprayag");mandakini_location.add("Sitapur");mandakini_location.add("Semi");mandakini_location.add("Bhiri");mandakini_location.add("Bansbada");
      mandakini_location.add("Chandrapuri");mandakini_location.add("Gabni gaon");mandakini_location.add("Saudi");mandakini_location.add("Bedu Bagad");mandakini_location.add("Jawahar nagar");mandakini_location.add("Vijay nagar");
      mandakini_location.add("Silli");mandakini_location.add("Chala/Phalat");mandakini_location.add("Tilwara");mandakini_location.add("Tehsil Building");



      ArrayAdapter<String> riveradapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, RIVER_SPINNER_DATA);
      riverSpinner.setAdapter(riveradapter);
      riverSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
             riverName = adapterView.getItemAtPosition(i).toString();
              getScenario (riverName);

          }
          @Override
          public void onNothingSelected(AdapterView<?> adapterView) {
          }
      });
      //createTable();
      viewmore1.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              if(txt1.getVisibility() == View.GONE) {
                  txt1.setVisibility(View.VISIBLE);
                  txt2.setVisibility(View.VISIBLE);
                  txt3.setVisibility(View.VISIBLE);
                  txt4.setVisibility(View.VISIBLE);
                  txt5.setVisibility(View.VISIBLE);
                  txt6.setVisibility(View.VISIBLE);
                  txt7.setVisibility(View.VISIBLE);
                  while (tableLayout1.getChildCount() > 1) {
                      TableRow row =  (TableRow)tableLayout1.getChildAt(1);
                      tableLayout1.removeView(row);
                  }
                    createFullTable(scenarioWaterlevelResult,scenarioDischargeResult,scenarioDurationResult,scenarioResult25year,scenarioResult50year,scenarioResult100year,scenarioResult25m3year,scenarioResult50m3year,scenarioResult100m3year,scenarioHflJune2013M);
                  viewmore1.setText("");
                  viewmore1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.arrow_left, 0, R.drawable.arrow_left, 0);


              }else{
                  txt1.setVisibility(View.GONE);
                  txt2.setVisibility(View.GONE);
                  txt3.setVisibility(View.GONE);
                  txt4.setVisibility(View.GONE);
                  txt5.setVisibility(View.GONE);
                  txt6.setVisibility(View.GONE);
                  txt7.setVisibility(View.GONE);
                  while (tableLayout1.getChildCount() > 1) {
                      TableRow row =  (TableRow)tableLayout1.getChildAt(1);
                      tableLayout1.removeView(row);
                  }
                  createTable(scenarioWaterlevelResult,scenarioDischargeResult,scenarioDurationResult);
                  viewmore1.setText("");
                  viewmore1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.arrow_right, 0, R.drawable.arrow_right, 0);
              }




          }
      });
      return view;
  }

  private void getScenario(final String riverName){
      AppPrefs appPrefs = AppPrefs.getInstance(getActivity());
      WebServiceCalls.getScenarios(riverName,
              new NwCall(rlLayout) {
                  @Override
                  public void onSuccess(String msg) {
                      PopMessage.makeshorttoast(getActivity(), msg);
                  }

                  @Override
                  public void onFailure(String msg) {
                      PopMessage.makeshorttoast(getActivity(), msg);
                  }

                  @Override
                  public void onSuccess(Bundle msg) {
                      ArrayList<GetScenario> scenarioList = (ArrayList<GetScenario>) msg.get("scenarioList")  ;
                      ArrayList<String> scenarios = new ArrayList<String>();
                      dischargeFileValue = new ArrayList<>();
                      location = new ArrayList<>();
                      for (int i = 0; i < scenarioList.size(); i++) {
                         String scenarioName = scenarioList.get(i).getScenario();
                          String[] parts = scenarioName.split("_");
                         dischargeFileValue.add(parts[1]);
                         location.add(parts[2]);
                          scenarios.add(parts[0]);
                      }

                      ArrayAdapter<String> scenarioAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, scenarios);
                      //scenarioSpinner.setVisibility(View.VISIBLE);
                      scenarioSpinner.setAdapter(scenarioAdapter);

                      scenarioSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                          @Override
                          public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                              scenarioName = adapterView.getItemAtPosition(i).toString();
                              getScenarioResults(riverName,scenarioName);
                              setvalues(i,dischargeFileValue,location);

                              }
                          @Override
                          public void onNothingSelected(AdapterView<?> adapterView) {
                          }
                      });

                  }

                  @Override
                  public void onFailure(Bundle msg) {
                      ArrayAdapter<String> staadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, new ArrayList<String>());
                      scenarioSpinner.setAdapter(staadapter);
                  }
              });
  }

    private void setvalues(int i,ArrayList<String> dischargeFileValue,ArrayList<String> location){
        //Toast.makeText(getContext(), "one value"+dischargeFileValue.get(i), Toast.LENGTH_SHORT).show();
        txt_discharge_filevalue.setText(dischargeFileValue.get(i));
        txt_location.setText(location.get(i));

    }
  private void getScenarioResults(String riverName, String scenarioName){

      AppPrefs appPrefs = AppPrefs.getInstance(getActivity());
      WebServiceCalls.getScenarioResults(riverName,scenarioName,
              new NwCall(rlLayout) {
                  @Override
                  public void onSuccess(String msg) {
                      PopMessage.makeshorttoast(getActivity(), msg);
                  }

                  @Override
                  public void onFailure(String msg) {
                      PopMessage.makeshorttoast(getActivity(), msg);
                  }

                  @Override
                  public void onSuccess(Bundle msg) {
                      scenarioResultList = (ArrayList<GetScenarioResults>) msg.get("scenarioResultList")  ;
                      scenarioLocationResult = new ArrayList<String>();
                      scenarioWaterlevelResult = new ArrayList<String>();
                      scenarioDischargeResult = new ArrayList<String>();
                      scenarioDurationResult = new ArrayList<String>();
                      scenarioResult25year = new ArrayList<String>();
                      scenarioResult50year = new ArrayList<String>();
                      scenarioResult100year = new ArrayList<String>();
                      scenarioResult25m3year = new ArrayList<String>();
                      scenarioResult50m3year = new ArrayList<String>();
                      scenarioResult100m3year = new ArrayList<String>();
                      scenarioHflJune2013M = new ArrayList<String>();
                      for (int i = 0; i < scenarioResultList.size(); i++) {
                         // scenarioLocationResult.add(scenarioResultList.get(i).get());
                          scenarioWaterlevelResult.add(scenarioResultList.get(i).getWaterLevelM());
                          scenarioDischargeResult.add(scenarioResultList.get(i).getDischargeM3Sec());
                          scenarioDurationResult.add(scenarioResultList.get(i).getDurationMinutes());
                          scenarioResult25year.add(scenarioResultList.get(i).getJsonMember25YearM());
                          scenarioResult50year.add(scenarioResultList.get(i).getJsonMember50YearM());
                          scenarioResult100year.add(scenarioResultList.get(i).getJsonMember100YearM());
                          scenarioResult25m3year.add(scenarioResultList.get(i).getJsonMember25YearM3Sec());
                          scenarioResult50m3year.add(scenarioResultList.get(i).getJsonMember50YearM3Sec());
                          scenarioResult100m3year.add(scenarioResultList.get(i).getJsonMember100YearM3Sec());
                          scenarioHflJune2013M.add(scenarioResultList.get(i).getHflJune2013M());
                      }
                      while (tableLayout1.getChildCount() > 1) {
                          TableRow row =  (TableRow)tableLayout1.getChildAt(1);
                          tableLayout1.removeView(row);
                      }
                      createTable(scenarioWaterlevelResult,scenarioDischargeResult,scenarioDurationResult);


                  }

                  @Override
                  public void onFailure(Bundle msg) {
                  }
              });


  }


  private void setUpSwipeReferesh() {
    swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
          swipeRefreshLayout.setRefreshing(false);

      }
    });
  }


  private void setUpList() {

        }

    private void createTable(ArrayList<String> scenarioWaterlevelResult,ArrayList<String> scenarioDischargeResult,ArrayList<String> scenarioDurationResult){
        for (int i = 0; i < scenarioWaterlevelResult.size(); i++) {
        row = new TableRow(getContext());
        txtview1 = new TextView(getContext());

        txtview2 = new TextView(getContext());
        txtview3 = new TextView(getContext());
        txtview4 = new TextView(getContext());

        txtview1.setBackground(getResources().getDrawable(R.drawable.cell_shape));
        txtview1.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

        txtview2.setBackground(getResources().getDrawable(R.drawable.cell_shape));
        txtview2.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

        txtview3.setBackground(getResources().getDrawable(R.drawable.cell_shape));
        txtview3.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

        txtview4.setBackground(getResources().getDrawable(R.drawable.cell_shape));
        txtview4.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

        row.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));
        //news1 = (news) newslist.get(i);
            if(riverName.equals("Alaknanda")) {
                txtview1.setText(alaknanda_location.get(i));
            }else if(riverName.equals("Bhagirathi")){
                txtview1.setText(bhagirathi_location.get(i));
            }else if(riverName.equals("Kali")){
                txtview1.setText(kali_location.get(i));
            }else if(riverName.equals("Mandakini")){
                txtview1.setText(mandakini_location.get(i));
            }
            txtview2.setText(scenarioDurationResult.get(i));
            txtview3.setText(scenarioDischargeResult.get(i));
            txtview4.setText(scenarioWaterlevelResult.get(i));


        row.addView(txtview1);
        row.addView(txtview2);
        row.addView(txtview3);
        row.addView(txtview4);
        tableLayout1.addView(row);
    }
}

    private void createFullTable(ArrayList<String> scenarioWaterlevelResult,ArrayList<String> scenarioDischargeResult,ArrayList<String> scenarioDurationResult,ArrayList<String> scenarioResult25year,ArrayList<String> scenarioResult50year,ArrayList<String> scenarioResult100year,ArrayList<String> scenarioResult25m3year,ArrayList<String> scenarioResult50m3year,ArrayList<String> scenarioResult100m3year,ArrayList<String> scenarioHflJune2013M){
        for (int i = 0; i < scenarioWaterlevelResult.size(); i++) {
            row2 = new TableRow(getContext());
            TextView txtview11 = new TextView(getContext());
            TextView txtview22 = new TextView(getContext());
            TextView txtview33 = new TextView(getContext());
            TextView txtview44 = new TextView(getContext());
            TextView txtview55 = new TextView(getContext());
            TextView txtview66 = new TextView(getContext());
            TextView txtview77 = new TextView(getContext());
            TextView txtview88 = new TextView(getContext());
            TextView txtview99 = new TextView(getContext());
            TextView txtview00 = new TextView(getContext());
            TextView txtview1010 = new TextView(getContext());
            txtview11.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            txtview11.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

            txtview22.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            txtview22.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

            txtview33.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            txtview33.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

            txtview44.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            txtview44.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

            txtview55.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            txtview55.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

            txtview66.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            txtview66.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

            txtview77.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            txtview77.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

            txtview88.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            txtview88.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

            txtview99.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            txtview99.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

            txtview00.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            txtview00.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

            txtview1010.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            txtview1010.setPadding(padding_5dp,padding_5dp,padding_5dp,padding_5dp);

            row.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT));
            //news1 = (news) newslist.get(i);
            txtview11.setText(""+i);
            txtview22.setText(scenarioDurationResult.get(i));
            txtview33.setText(scenarioDischargeResult.get(i));
            txtview44.setText(scenarioWaterlevelResult.get(i));
            txtview55.setText(scenarioResult25year.get(i));
            txtview66.setText(scenarioResult50year.get(i));
            txtview77.setText(scenarioResult100year.get(i));
            txtview88.setText(scenarioResult25m3year.get(i));
            txtview99.setText(scenarioResult50m3year.get(i));
            txtview00.setText(scenarioResult100m3year.get(i));
            txtview1010.setText(scenarioHflJune2013M.get(i));

            row2.addView(txtview11);
            row2.addView(txtview22);
            row2.addView(txtview33);
            row2.addView(txtview44);
            row2.addView(txtview55);
            row2.addView(txtview66);
            row2.addView(txtview77);
            row2.addView(txtview88);
            row2.addView(txtview99);
            row2.addView(txtview00);
            row2.addView(txtview1010);
            tableLayout1.addView(row2);
        }
    }

  private void init() {

    insLinear=(LinearLayout)view.findViewById(R.id.layout_instructions);
    swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
    tableLayout1 = (TableLayout)view.findViewById(R.id.tablelayout1);
    scenarioSpinner = (Spinner)view.findViewById(R.id.spinner1);
    riverSpinner = (Spinner)view.findViewById(R.id.spinner2);
    txt_discharge_filevalue = (TextView)view.findViewById(R.id.txt_discharge_filevalue);
    txt_location = (TextView)view.findViewById(R.id.txt_location);
      txt1 = (TextView)view.findViewById(R.id.txt1);
      txt2 = (TextView)view.findViewById(R.id.txt2);
      txt3 = (TextView)view.findViewById(R.id.txt3);
      txt4 = (TextView)view.findViewById(R.id.txt4);
      txt5 = (TextView)view.findViewById(R.id.txt5);
      txt6 = (TextView)view.findViewById(R.id.txt6);
      txt7 = (TextView)view.findViewById(R.id.txt7);
      btnLogin = (TextView)view.findViewById(R.id.btnLogin);
      newsll = (LinearLayout)view.findViewById(R.id.newsll);

    viewmore1 = (TextView)view.findViewById(R.id.viewmore1);
    //viewmore2 = (TextView)view.findViewById(R.id.viewmore2);
  }
    public void reloadFragment(boolean isLoggedIn) {
        if (!isLoggedIn) {
            newsll.setVisibility(View.GONE);
            btnLogin.setVisibility(View.VISIBLE);

        }
    }
}
