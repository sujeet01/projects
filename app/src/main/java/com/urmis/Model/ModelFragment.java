package com.urmis.Model;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.urmis.Authenticate.LoginActivity;
import com.urmis.Map.MainActivity;
import com.urmis.Network.Callback.NwCall;
import com.urmis.Network.WebServiceCalls;
import com.urmis.R;
import com.urmis.Repository.AppPrefs;
import com.urmis.Repository.Models.Model.GetStationsModel;
import com.urmis.Utils.Constants;
import com.urmis.Utils.PopMessage;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;


import java.util.ArrayList;

/**
 * Created by Brajendr on 4/10/2017.
 */

public class ModelFragment extends Fragment {
    private View view;
    private RelativeLayout rlLogin;
    private Button runScenario;
    private EditText txtScenario;
    private EditText txtDischarge;
    private TextView tvLogin,btnlogin;
    LinearLayout modell;
    private AppPrefs appPrefs;
    MaterialBetterSpinner riverSpinner;
    MaterialBetterSpinner stationSpinner;
    String[] RIVER_SPINNER_DATA = {"Alaknanda","Bhagirathi","Kali","Mandakini"};

    private ArrayList<GetStationsModel> stationList = new ArrayList<>();

    @Override
    public void onResume() {
        super.onResume();
        if(appPrefs.getUser().isLoggedIn() == true){
            //Toast.makeText(getContext(), "loggedin", Toast.LENGTH_SHORT).show();
            modell.setVisibility(View.VISIBLE);
            btnlogin.setVisibility(View.GONE);
        }else{
            // Toast.makeText(getContext(), "not loggedin", Toast.LENGTH_SHORT).show();
            modell.setVisibility(View.GONE);
            btnlogin.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_model, container, false);

        appPrefs = AppPrefs.getInstance(getActivity());
        rlLogin = (RelativeLayout) view.findViewById(R.id.rlLogin);
        tvLogin = (TextView) view.findViewById(R.id.btnLogin);

        runScenario = (Button)view.findViewById(R.id.btnRun);

        riverSpinner = (MaterialBetterSpinner)view.findViewById(R.id.input_rivername);
        stationSpinner = (MaterialBetterSpinner)view.findViewById(R.id.input_location);
        txtScenario = (EditText) view.findViewById(R.id.input_scenarioname);
        txtDischarge = (EditText) view.findViewById(R.id.input_dischargeFileValue);
        btnlogin = (TextView)view.findViewById(R.id.btnLogin);
        modell = (LinearLayout) view.findViewById(R.id.modell);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.putExtra("navigate", Constants.NAVIGATE_TO_NEWS);
                startActivityForResult(intent, Constants.NAVIGATE_TO_NEWS);
            }
        });





        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, RIVER_SPINNER_DATA);

        riverSpinner.setAdapter(adapter);

        riverSpinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String river=riverSpinner.getText().toString();
                //Toast.makeText(getContext(), ""+river, Toast.LENGTH_SHORT).show();
                getStations (river);
            }
        });

        runScenario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (! appPrefs.getUser().isLoggedIn())
                {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    intent.putExtra("navigate", Constants.NAVIGATE_TO_MODEL);
                    startActivityForResult(intent, Constants.NAVIGATE_TO_MODEL);
                }
                else
                {
                    String river = riverSpinner.getText().toString();
                    String station = stationSpinner.getText().toString();
                    String scenario = txtScenario.getText().toString();

                    double discharge = 0;

                    try
                    {
                        discharge = Double.parseDouble(txtDischarge.getText().toString());
                    }
                    catch(Exception e)
                    {
                        discharge = 0;
                    }

                    if (scenario.isEmpty())
                    {
                        PopMessage.makeshorttoast(getActivity(), "Please specify scenario name");
                    }
                    else if (river.isEmpty())
                    {
                        PopMessage.makeshorttoast(getActivity(), "Please specify the river");
                    }
                    else if (station.isEmpty())
                    {
                        PopMessage.makeshorttoast(getActivity(), "Please specify the station");
                    }
                    else if (discharge <= 0.0)
                    {
                        PopMessage.makeshorttoast(getActivity(), "Please specify valid discharge value");
                    }
                    else
                    {
                        runScenario(scenario, river, station, discharge);
                        //runScenario("title", "des", "file_type", "id","assign","sub","teach","time");
                    }
                }

            }
        });

        return view;
    }

    private void runScenario(String scenarioName, String riverName, String station, double discharge) {
        //private void runScenario(String title, String description , String file_type, String class_id, String assign_type,String subject_id,String teacher_id,String timestamp) {
        AppPrefs appPrefs = AppPrefs.getInstance(getActivity());
        String emailId = appPrefs.getUser().getEmailId();
        WebServiceCalls.runScenario(scenarioName, riverName, station, discharge,emailId,
                //WebServiceCalls.runScenario(title, description, file_type, class_id,assign_type,subject_id,teacher_id,timestamp,
                new NwCall(rlLogin) {
                    @Override
                    public void onSuccess(String msg) {
                        PopMessage.makeshorttoast(getActivity(), msg);
                    }

                    @Override
                    public void onFailure(String msg) {
                        PopMessage.makeshorttoast(getActivity(), msg);
                    }
                    @Override
                    public void onSuccess(Bundle msg) {
                    }

                    @Override
                    public void onFailure(Bundle msg) {
                    }
                });
    }

    private void getStations(String riverName) {
        AppPrefs appPrefs = AppPrefs.getInstance(getActivity());
        WebServiceCalls.getStations(riverName,
                new NwCall(rlLogin) {
                    @Override
                    public void onSuccess(String msg) {
                        PopMessage.makeshorttoast(getActivity(), msg);
                    }

                    @Override
                    public void onFailure(String msg) {
                        PopMessage.makeshorttoast(getActivity(), msg);
                    }

                    @Override
                    public void onSuccess(Bundle msg) {
                        ArrayList<GetStationsModel> stationList = (ArrayList<GetStationsModel>) msg.get("stationList")  ;
                        ArrayList<String> stations = new ArrayList<String>();
                        for (int i = 0; i < stationList.size(); i++) {
                            stations.add(stationList.get(i).getStationName());
                        }

                        ArrayAdapter<String> staadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, stations);
                        stationSpinner.setVisibility(View.VISIBLE);
                        stationSpinner.setAdapter(staadapter);

                    }

                    @Override
                    public void onFailure(Bundle msg) {
                        ArrayAdapter<String> staadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, new ArrayList<String>());
                        stationSpinner.setAdapter(staadapter);
                    }
                });
    }

    public void reloadFragment(boolean isLoggedIn) {
        if (!isLoggedIn) {
            modell.setVisibility(View.GONE);
            btnlogin.setVisibility(View.VISIBLE);

        }
    }
}
