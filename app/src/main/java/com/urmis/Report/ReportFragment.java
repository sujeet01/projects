package com.urmis.Report;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.urmis.Authenticate.LoginActivity;
import com.urmis.R;
import com.urmis.Repository.AppPrefs;
import com.urmis.Utils.Constants;

/**
 * Created by Brajendr on 4/10/2017.
 */

public class ReportFragment extends Fragment {
  private View view;
  private RelativeLayout rlLogin;
  private TextView tvLogin;
  private AppPrefs appPrefs;

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    view = inflater.inflate(R.layout.fragment_report, container, false);
    appPrefs = AppPrefs.getInstance(getActivity());
    init();
    return view;
  }

  private void init() {

    rlLogin = (RelativeLayout) view.findViewById(R.id.rlLogin);
    tvLogin = (TextView) view.findViewById(R.id.btnLogin);
  }

  public void reloadFragment(boolean isLoggedIn) {
    if (isLoggedIn) {
      rlLogin.setVisibility(View.GONE);
    } else {
      rlLogin.setVisibility(View.VISIBLE);
      tvLogin.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
          Intent intent = new Intent(getActivity(), LoginActivity.class);
          intent.putExtra("navigate", Constants.NAVIGATE_TO_REPORT);
          startActivityForResult(intent, Constants.NAVIGATE_TO_REPORT);
        }
      });
    }

  }
}
