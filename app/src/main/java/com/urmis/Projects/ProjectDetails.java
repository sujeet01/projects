package com.urmis.Projects;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.squareup.picasso.Picasso;

import com.urmis.Network.Callback.NwCall;
import com.urmis.Network.Retrofit.HttpParse;
import com.urmis.Network.WebServiceCalls;
import com.urmis.Repository.Models.EditProgress.ResponseDataForApprove;
import com.urmis.Repository.Models.EditProgress.ResponseEditProgress;
import com.urmis.Projects.Progress.ProjectMonthlyProgressManager;
import com.urmis.Projects.Progress.UpdateProgress;
import com.urmis.Repository.AppPrefs;
import com.urmis.Repository.Models.EditProgress.ResponseUserTypeForProgress;
import com.urmis.Repository.Models.Progress.NewProgressWorkStatement;
import com.urmis.Repository.Models.Project.Image;

import com.urmis.Repository.Models.supervisorList.userSupervisorList;
import com.veer.multiselect.MultiSelectActivity;
import com.veer.multiselect.Util.Constants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.urmis.R;
import com.urmis.Repository.Models.Project.ListOfWorkStatement;
import com.urmis.Repository.Models.Project.Project;
import com.urmis.Repository.Models.Project.SalientFeature;
import com.urmis.Repository.Models.Project.Workitems;
import com.urmis.Utils.PopMessage;
import com.urmis.Utils.RecyclerViewTools.CommonRecyclerViewAdapter;
import com.urmis.Utils.RecyclerViewTools.ViewHolder;
import com.urmis.Utils.Utility;

class PerUtility {
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}

public class ProjectDetails extends AppCompatActivity {
    private static final int SHOW_PROJECT_PROGRESS = 101;
    private TextView tvProjectName, tvDistrict, tvRiver, tvRiverBank, tvTacApproved, tvFundSanctioned,
            tvFundConsumed, tvPercentageCompletion, tvSalientFeatures, tvSructuralDetails, tvGallery, tvUploadImages,
            tvUpdatePercentage;
    private RecyclerView rcSalientFeature;
    private LinearLayout llsalientHeader;
    private RecyclerView rcstructuralDetails;
    private LinearLayout llstructuralDetails, llGetProgress,llGetEditProgress,llGetApproveProgress,llbtn_hide;
    private RelativeLayout rlGetProgress;
    private ExpandableListView lvWorkStatements;
    private Project project;
    private ProgressBar pbar;
    private boolean isVisible = true;
    private ImageView projectImage;
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };
    private static final int RQS_OPEN_IMAGE = 1;
    public static final int RequestPermissionCode = 1;
    private String fundSactioned, cumulativeFinancialProgress, toatalPhysicalProgress, cumulativePhysicalProgress, approvedfinancial, approvedphysical;
    Float totalPhysicalBalance, totalFinancialBalance;
    Float total, cmulative, balance, approved, notapproved;
    PieChart mChart;

    TextView txt;
    String userid,progressSpinnerType,userDesignation,getApproverName,getApproverId,getApproverDesignation;
    Dialog dialog,dialogProgress;

    EditText physicalProgressBalance,financialProgressBalance,physicalProgressDate, financialProgressDate;
    TextView tpp, cpp, fp, cfp,txt_approved,txt_notapproved;
    RecyclerView.LayoutManager recylerViewLayoutManagerEditProgress;
    SwipeRefreshLayout swipeRefreshLayout;
    Uri imguri,cameraUri;
    ArrayList<String> aa = new ArrayList<String>();
    ArrayAdapter<String> approverListAdapter;
    ArrayList<String>listofProgreeApproverAfterProgressEnter;
    Spinner spinnerProgressApprover;
    ArrayList<userSupervisorList> userSupervisorLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_project_details);
       Bundle bundle = getIntent().getExtras();
        project = getIntent().getParcelableExtra("project");
        listofProgreeApproverAfterProgressEnter = (ArrayList<String>) bundle.getSerializable("key");
        initViews();
        setData();
        setUpEvents();
        EnableRuntimePermissionToAccessCamera();
    }

    // Requesting runtime permission to access camera.
    public void EnableRuntimePermissionToAccessCamera() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(ProjectDetails.this,
                Manifest.permission.CAMERA)) {
            // Printing toast message after enabling runtime permission.
            Toast.makeText(ProjectDetails.this, "CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();
            } else {
            ActivityCompat.requestPermissions(ProjectDetails.this, new String[]{Manifest.permission.CAMERA}, RequestPermissionCode);
            }
    }

    private void setUpEvents() {
        tvUpdatePercentage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newShowProgressDialog();
            }
        });

        tvSalientFeatures.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isVisible = !isVisible;
                tvSalientFeatures.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        isVisible ? R.drawable.ic_arrow_drop_up_white_24dp :
                                R.drawable.ic_arrow_drop_down_white_24dp, 0);
                rcSalientFeature.setVisibility(isVisible ? View.VISIBLE : View.GONE);
                llsalientHeader.setVisibility(isVisible ? View.VISIBLE : View.GONE);
                }
        });

        tvSructuralDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isVisible = !isVisible;
                tvSructuralDetails.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        isVisible ? R.drawable.ic_arrow_drop_up_white_24dp :
                                R.drawable.ic_arrow_drop_down_white_24dp, 0);
                rcstructuralDetails.setVisibility(isVisible ? View.VISIBLE : View.GONE);
                llstructuralDetails.setVisibility(isVisible ? View.VISIBLE : View.GONE);


            }
        });

        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (project.getImages().size() != 0) {
                    Dialog dialog = Utility.createDialog(ProjectDetails.this, R.layout.layout_image_gallery, true);
                    RecyclerView rcGrid = (RecyclerView) dialog.findViewById(R.id.rcImageGallery);
                    rcGrid.setLayoutManager(new GridLayoutManager(ProjectDetails.this, 2));
                    CommonRecyclerViewAdapter<Image> imageCommonRecyclerViewAdapter = new CommonRecyclerViewAdapter<Image>(
                            ProjectDetails.this, project.getImages(), R.layout.layout) {
                        @Override
                        public void onPostBindViewHolder(ViewHolder holder, Image image) {
                            ImageView imageView = holder.getView(R.id.image);
                            Picasso.with(ProjectDetails.this).load(image.getUrl().replaceAll(" ", "%20")).resize(200, 150).into(imageView);
                        }
                    };
                    rcGrid.setAdapter(imageCommonRecyclerViewAdapter);
                    imageCommonRecyclerViewAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Dialog dialog1 = Utility.createDialog(ProjectDetails.this, R.layout.layout_full_screen, false);
                            dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            ImageView imageView = (ImageView) dialog1.findViewById(R.id.image);
                            Picasso.with(ProjectDetails.this).load(project.getImages().get(position).getUrl().replaceAll(" ", "%20")).resize(200, 150).into(imageView);
                        }
                    });
                } else {
                    PopMessage.makeshorttoast(ProjectDetails.this, "There no images for this project");
                }
            }
        });
        tvUploadImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPictureDialog();

            }
        });
    }

    private void showPictureDialog() {
        PerUtility.checkPermission(this);
        verifyStoragePermissions(this);
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};

        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
//        Intent intent = new Intent(ProjectDetails.this, MultiSelectActivity.class);
//        intent.putExtra(com.veer.multiselect.Util.Constants.LIMIT, 2);
//        intent.putExtra(com.veer.multiselect.Util.Constants.SELECT_TYPE, Constants.PATH_IMAGE);
//        startActivityForResult(intent,
//                com.veer.multiselect.Util.Constants.REQUEST_CODE_MULTISELECT);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/jpeg");
        startActivityForResult(intent, RQS_OPEN_IMAGE);


    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 0);


    }

    private void newShowProgressDialog() {

        final String id = project.getId();
        final String projectName = project.getName();
        dialogProgress = Utility.createDialog(this, R.layout.layoutnew_show_progress, true);

        final TextView setProjectName = (TextView) dialogProgress.findViewById(R.id.setProjectName);
        final Spinner progressType = (Spinner) dialogProgress.findViewById(R.id.progressType);
        final TextView updateProgress = (TextView) dialogProgress.findViewById(R.id.updateProgress);
        //final TextView editProgress = (TextView) dialogProgress.findViewById(R.id.txt_editprogress);
        final TextView approveProgress = (TextView) dialogProgress.findViewById(R.id.txt_approveprogress);
        swipeRefreshLayout = (SwipeRefreshLayout)dialogProgress.findViewById(R.id.refreshlayout_progressdialog);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProgress(id, progressSpinnerType);
                //Toast.makeText(ProjectDetails.this, "refresh layout", Toast.LENGTH_SHORT).show();
            }
        });

        txt = (TextView) dialogProgress.findViewById(R.id.txt);
        mChart = (PieChart) dialogProgress.findViewById(R.id.pcProgress);


        AppPrefs appPrefs = AppPrefs.getInstance(ProjectDetails.this);
        userid = appPrefs.getUser().getUser_id();
        userDesignation = appPrefs.getUser().getDesignationName();

        if(userid.equals(project.getProject_creator())){
            updateProgress.setVisibility(View.VISIBLE);
        }
        if(userid.equals(project.getProgress_approver())|| listofProgreeApproverAfterProgressEnter.contains(userid)){
            approveProgress.setVisibility(View.VISIBLE);
        }

//        WebServiceCalls.getUserTypeForProgress(id,
//                new NwCall(llbtn_hide) {
//                    @Override
//                    public void onSuccess(String msg) {
//                        PopMessage.makeshorttoast(ProjectDetails.this, msg); }
//
//                    @Override
//                    public void onFailure(String msg) {
//                        PopMessage.makeshorttoast(ProjectDetails.this, msg); }
//
//                    @Override
//                    public void onSuccess(Bundle msg) {
//                        ArrayList<ResponseUserTypeForProgress> responseUserTypeForProgress = msg.getParcelableArrayList("responseUserTypeForProgresses");
//
//                        String isCompleted = responseUserTypeForProgress.get(0).getIscompleted();
//                        String project_handler_id = responseUserTypeForProgress.get(0).getProject_handler_id();
//                        String project_creator_id = responseUserTypeForProgress.get(0).getProject_creator_id();
//                        String project_approver_id = responseUserTypeForProgress.get(0).getProject_approver_id();
//
//                        if(userid.equals(project_handler_id) && isCompleted.equals("False")){
//                           // editProgress.setVisibility(View.VISIBLE);
//                            updateProgress.setVisibility(View.VISIBLE);
//                        }
//                        if(userid.equals(project_approver_id)){
//                            approveProgress.setVisibility(View.VISIBLE);
//                        }
//                        }
//
//                    @Override
//                    public void onFailure(Bundle msg) {
//
//                    }
//                });

            updateProgress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //showupdateProjectProgressUI();
                    ShowUpdateProgressTypeUI();
                }
            });


//            editProgress.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    ShowEditProgressTypeUI();
//                }
//            });
            approveProgress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShowApproveProgressTypeUI();
                }
            });

            List<String> progressTypeSpinner = new ArrayList<>();
            progressTypeSpinner.add("Physical Progress");
            progressTypeSpinner.add("Financial Progress");

        ArrayAdapter<String> progressSpinnerAdapter = new ArrayAdapter<String>(ProjectDetails.this, android.R.layout.simple_spinner_item, progressTypeSpinner);
        progressSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        progressType.setAdapter(progressSpinnerAdapter);

        setProjectName.setText(projectName);
        progressType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                progressSpinnerType = progressType.getSelectedItem().toString();
                getProgress(id, progressSpinnerType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getProgress(final String id, final String progressSpinnerType) {

        AppPrefs appPrefs = AppPrefs.getInstance(this);
        WebServiceCalls.getProgress(id,
                new NwCall(llGetProgress) {
                    @Override
                    public void onSuccess(String msg) {
                        swipeRefreshLayout.setRefreshing(false);
                        PopMessage.makeshorttoast(ProjectDetails.this, msg);
                        }
                    @Override
                    public void onFailure(String msg) {
                        swipeRefreshLayout.setRefreshing(false);
                        PopMessage.makeshorttoast(ProjectDetails.this, msg);
                        }

                    @Override
                    public void onSuccess(Bundle msg) {
                        swipeRefreshLayout.setRefreshing(false);
                        NewProgressWorkStatement newProgressWorkStatements = (NewProgressWorkStatement) msg.get("progressdata");

                        fundSactioned = newProgressWorkStatements.getFundSanctioned();
                        cumulativeFinancialProgress = newProgressWorkStatements.getCumulativeFinancialProgress();
                        toatalPhysicalProgress = newProgressWorkStatements.getTotalPhysicalProgress();
                        cumulativePhysicalProgress = newProgressWorkStatements.getCumulativePhysicalProgress();
                        approvedfinancial = newProgressWorkStatements.getApprovedfinancial();
                        approvedphysical = newProgressWorkStatements.getApprovedphysical();

                        totalPhysicalBalance = Float.parseFloat(toatalPhysicalProgress) - Float.parseFloat(cumulativePhysicalProgress);
                        totalFinancialBalance = Float.parseFloat(fundSactioned) - Float.parseFloat(cumulativeFinancialProgress);

                        if (progressSpinnerType.equals("Physical Progress")) {
                          //  showPieChartsData(fundSactioned, cumulativeFinancialProgress, approvedfinancial);

                            showPieChartsData(toatalPhysicalProgress, cumulativePhysicalProgress, approvedphysical);
                        }if (progressSpinnerType.equals("Financial Progress")) {
                            showPieChartsData(fundSactioned, cumulativeFinancialProgress, approvedfinancial);
                        }
                        tpp.setText("Total :" + " " + toatalPhysicalProgress);
                        cpp.setText("Balance :" + " " + totalPhysicalBalance);

                        fp.setText("Total :" + " " + fundSactioned);
                        cfp.setText("Balance :" + " " + totalFinancialBalance);
                    }

                    @Override
                    public void onFailure(Bundle msg) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
    }

    private void showPieChartsData(String totalProgress, String cumulativeProgress, String approvedbalance) {
        if (approvedbalance.equals("")) {
            approvedbalance = "0";
        }

        //mChart.setUsePercentValues(true);
        // mChart.setDescription("");
        mChart.getDescription().setText("");
        mChart.setExtraOffsets(5, 10, 5, 5);
        mChart.setDragDecelerationFrictionCoef(0.95f);

        // enable hole and configure
        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);
        mChart.setHighlightPerTapEnabled(true);
        mChart.setHoleRadius(7);
        mChart.setTransparentCircleRadius(10);

        // enable rotation of the chart by touch
       mChart.setRotationAngle(0);
       mChart.setRotationEnabled(true);
        mChart.setHighlightPerTapEnabled(true);
        mChart.setEntryLabelColor(Color.BLACK);
        mChart.setEntryLabelTextSize(12f);

        //SET ADAPTERS
        String[] x = {"Balance amount", "Not Approved Amount", "Approved amount"};
        total = Float.parseFloat(totalProgress);
        cmulative = Float.parseFloat(cumulativeProgress);
        approved = Float.parseFloat(approvedbalance);
        notapproved = cmulative - approved;
        balance = total - cmulative;
        Float[] y = {balance
                , notapproved, approved};
        txt.setText("Balance: " + balance + "   Approved: " + approved + "   Not Approved: "+notapproved);
        Utility.addChartData(mChart, y, x);
    }


    private void ShowUpdateProgressTypeUI() {

         dialog = Utility.createDialog(this, R.layout.layoutnew_update_progress_type, true);
        final TextView setProjectName = (TextView) dialog.findViewById(R.id.setProjectName);
        final Button btnProceed = (Button) dialog.findViewById(R.id.btnsave);
        final Button btnCclose = (Button) dialog.findViewById(R.id.btnclose);
        txt_approved = (TextView)dialog.findViewById(R.id.txt_approved);
        txt_notapproved = (TextView)dialog.findViewById(R.id.txt_notapproved);
        physicalProgressBalance = (EditText) dialog.findViewById(R.id.edprogress);
        financialProgressBalance = (EditText) dialog.findViewById(R.id.fpprogress);
        physicalProgressDate = (EditText) dialog.findViewById(R.id.edprogressDate1);
        financialProgressDate = (EditText) dialog.findViewById(R.id.fpprogressDate1);
        tpp = (TextView) dialog.findViewById(R.id.tvttotal);
        cpp = (TextView) dialog.findViewById(R.id.tvbalance);
        fp = (TextView) dialog.findViewById(R.id.fpttotal);
        cfp = (TextView) dialog.findViewById(R.id.fpbalance);


        setProjectName.setText(project.getName());
        getProgress(project.getId(), project.getId());

        txt_approved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowEditProgressTypeUI("approved");
            }
        });
        txt_notapproved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowEditProgressTypeUI("notapproved");
            }
        });

        physicalProgressDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog;
                // Get current year, month and day.
                Calendar now = Calendar.getInstance();
                int myear = now.get(java.util.Calendar.YEAR);
                int mmonth = now.get(java.util.Calendar.MONTH);
                int mday = now.get(java.util.Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(ProjectDetails.this, android.R.style.Theme_Holo_Dialog, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String month1= String.valueOf(monthOfYear + 1);
                        String day1= String.valueOf((dayOfMonth));

                        if(monthOfYear>=0 && monthOfYear<9){
                            month1="0"+(monthOfYear+1);
                        }
                        if(dayOfMonth <=9){
                            day1="0"+(dayOfMonth);
                        }
                         physicalProgressDate.setText(day1 + "/" + month1 + "/" + year);

                    }
                }, myear, mmonth, mday);
               // datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + (1000 * 60 * 60 * 24 * 0));
                datePickerDialog.show();
            }
        });

        financialProgressDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog;
                // Get current year, month and day.
                Calendar now = Calendar.getInstance();
                int myear = now.get(java.util.Calendar.YEAR);
                int mmonth = now.get(java.util.Calendar.MONTH);
                int mday = now.get(java.util.Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(ProjectDetails.this, android.R.style.Theme_Holo_Dialog, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String month1= String.valueOf(monthOfYear + 1);
                        String day1= String.valueOf((dayOfMonth));

                        if(monthOfYear>=0 && monthOfYear<9){
                            month1="0"+(monthOfYear+1);
                        }
                        if(dayOfMonth <=9){
                            day1="0"+(dayOfMonth);
                        }
                        financialProgressDate.setText(day1 + "/" + month1 + "/" + year);

                    }
                }, myear, mmonth, mday);
                //datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + (1000 * 60 * 60 * 24 * 0));
                datePickerDialog.show();
            }

        });
        btnCclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String physicalBalance = physicalProgressBalance.getText().toString();
                String financialBalance = financialProgressBalance.getText().toString();
                String physicalDate = physicalProgressDate.getText().toString();
                String financialDate = financialProgressDate.getText().toString();

                if(physicalBalance.trim().length() == 0){
                    Toast.makeText(getApplicationContext(), "Please enter Physical Balance", Toast.LENGTH_SHORT).show();
                }else if(financialBalance.trim().length() == 0){
                    Toast.makeText(getApplicationContext(), "Please enter Financial Balance", Toast.LENGTH_SHORT).show();
                }else if (physicalDate.trim().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Please enter Physical Progress Date", Toast.LENGTH_SHORT).show();
                } else if (financialDate.trim().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Please enter Financial Progress Date", Toast.LENGTH_SHORT).show();
                }
                else {

                    List<ProjectMonthlyProgressManager> progressWorkList = new ArrayList<ProjectMonthlyProgressManager>();

                    Float finalCpp = Float.parseFloat(cumulativePhysicalProgress) + Float.parseFloat(physicalBalance);
                    Float finalCfp = Float.parseFloat(cumulativeFinancialProgress) + Float.parseFloat(financialBalance);
                    ProjectMonthlyProgressManager pp = new ProjectMonthlyProgressManager();
                    pp.setProjectId(project.getId());
                    pp.setProgressValue(physicalBalance);
                    pp.setProgressFinancialValue(financialBalance);
                    pp.setCumulativeFinacialQuantity(finalCfp.toString());
                    pp.setCumulativePhysicalQuantity(finalCpp.toString());
                    pp.setFinancialProgressDate(financialDate);
                    pp.setPhysicalProgressDate(physicalDate);
                    progressWorkList.add(pp);


                    if (Float.parseFloat(physicalBalance) > totalPhysicalBalance) {
                        Toast.makeText(ProjectDetails.this, "Value must be less than" + totalPhysicalBalance, Toast.LENGTH_SHORT).show();
                    } else if (Float.parseFloat(financialBalance) > totalFinancialBalance) {
                        Toast.makeText(ProjectDetails.this, "Value must be less than" + totalFinancialBalance, Toast.LENGTH_SHORT).show();
                    }
                    //edAmount, fpBalanceAmount, cumulativeFinancialProgress,cumulativePhysicalProgress, financialProgressDate, physicalProgressDate
                    else {
                        updateProgressTypeUI(project.getId(), progressWorkList,project.getProgress_approver(),project.getProgress_approver_designation());
                    }
                }
            }
        });
    }
        public void showDatePicker(){
            DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                    StringBuffer strBuf = new StringBuffer();
                    //strBuf.append("You select date is ");
                    strBuf.append(dayOfMonth);
                    strBuf.append("/");
                    strBuf.append(month + 1);
                    strBuf.append("/");
                    strBuf.append(year);

                    // TextView datePickerValueTextView = (TextView)findViewById(R.id.datePickerValue);
                    physicalProgressDate.setText(strBuf.toString());
                }
            };


            // Get current year, month and day.
            Calendar now = Calendar.getInstance();
            int year = now.get(java.util.Calendar.YEAR);
            int month = now.get(java.util.Calendar.MONTH);
            int day = now.get(java.util.Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(ProjectDetails.this, android.R.style.Theme_Holo_Dialog, onDateSetListener, year, month, day);
            datePickerDialog.setCancelable(false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int daySpinnerId = Resources.getSystem().getIdentifier("day", "id", "android");
                if (daySpinnerId != 0) {

                    View daySpinner = datePickerDialog.findViewById(daySpinnerId);
                    if (daySpinner != null) {
                        daySpinner.setVisibility(View.GONE);
                    }
                }
            } else {
                Field f[] = datePickerDialog.getClass().getDeclaredFields();
                for (Field field : f) {
                    if (field.getName().equals("mDayPicker") || field.getName().equals("mDaySpinner")) {
                        field.setAccessible(true);
                        Object dayPicker = null;
                        try {
                            dayPicker = field.get(datePickerDialog);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                        ((View) dayPicker).setVisibility(View.GONE);
                    }
                }
            }
            datePickerDialog.show();
        }

    public void updateProgressTypeUI(final String id, final List<ProjectMonthlyProgressManager> progressWorkList,String progressApproverId,String progressApproverDesignation) {

        UpdateProgress updateProgress = new UpdateProgress();
        updateProgress.setProjectId(id);
        updateProgress.setprojectMonthlyProgressManager(progressWorkList);


        WebServiceCalls.saveProgress(id, progressWorkList,progressApproverId,progressApproverDesignation,
                new NwCall(rlGetProgress) {
                    @Override
                    public void onSuccess(String msg) {

                        PopMessage.makeshorttoast(ProjectDetails.this, msg);
                        dialog.dismiss();

                    }

                    @Override
                    public void onFailure(String msg) {

                        PopMessage.makeshorttoast(ProjectDetails.this, msg);
                        dialog.dismiss();
                    }

                    @Override
                    public void onSuccess(Bundle msg) {

                    }

                    @Override
                    public void onFailure(Bundle msg) {

                    }
                });
    }

    private void ShowEditProgressTypeUI(final String approvedStatus){
        final Dialog dialogEdit = Utility.createDialog(this, R.layout.layoutnew_edit_progress_type, true);
        final TextView setProjectName = (TextView) dialogEdit.findViewById(R.id.setProjectName);

        final Button btnCcloseEditProgress = (Button) dialogEdit.findViewById(R.id.btnclose_editprogress);
        final RecyclerView recyclerViewEditProgress = (RecyclerView) dialogEdit.findViewById(R.id.editprogressList);
        final TextView editHideText = (TextView)dialogEdit.findViewById(R.id.edittexthide);
        setProjectName.setText(project.getName());
        final String id = project.getId();


        WebServiceCalls.getEditProgress(id,
                new NwCall(llGetEditProgress) {
                    @Override
                    public void onSuccess(String msg) {
                        PopMessage.makeshorttoast(ProjectDetails.this, msg);

                    }


                    @Override
                    public void onFailure(String msg) {
                        PopMessage.makeshorttoast(ProjectDetails.this, msg);

                    }

                    @Override
                    public void onSuccess(Bundle msg) {
                        ArrayList<ResponseEditProgress>  progressApprove =new ArrayList<>();
                        ArrayList<ResponseEditProgress>  progressNotApprove =new ArrayList<>();
                        ArrayList<ResponseEditProgress> responseEditProgress = msg.getParcelableArrayList("editprogressdata");

                        for (int i=0;i<responseEditProgress.size();i++){
                            if(responseEditProgress.get(i).getProgressapproved().equals("True")){
                                progressApprove.add(responseEditProgress.get(i));
                            }else{
                                progressNotApprove.add(responseEditProgress.get(i));
                            }
                        }
                        recylerViewLayoutManagerEditProgress = new LinearLayoutManager(ProjectDetails.this);
                        recyclerViewEditProgress.setLayoutManager(recylerViewLayoutManagerEditProgress);
                        if (progressApprove.size() == 0 && approvedStatus.equals("approved")){
                            editHideText.setVisibility(View.VISIBLE);
                            editHideText.setText("No Approved Data Available");
                        }else if(progressNotApprove.size() == 0 && approvedStatus.equals("notapproved")){
                            editHideText.setVisibility(View.VISIBLE);
                            editHideText.setText("No Not Approved Data Available");
                        }
                        else {
                            recyclerViewEditProgress.setVisibility(View.VISIBLE);
                            if (approvedStatus.equals("approved")) {
                                RecyclerView.Adapter recyclerViewAdapterEditProgress = new RecyclerViewAdapterEditProgress(ProjectDetails.this, progressApprove, id,totalFinancialBalance,totalPhysicalBalance,project.getProgress_approver(),project.getProgress_approver_designation());
                                recyclerViewEditProgress.setAdapter(recyclerViewAdapterEditProgress);
                            }else{
                                RecyclerView.Adapter recyclerViewAdapterEditProgress = new RecyclerViewAdapterEditProgress(ProjectDetails.this, progressNotApprove, id,totalFinancialBalance,totalPhysicalBalance,project.getProgress_approver(),project.getProgress_approver_designation());
                                recyclerViewEditProgress.setAdapter(recyclerViewAdapterEditProgress);
                            }

                        }
                    }

                    @Override
                    public void onFailure(Bundle msg) {

                    }
                });

        btnCcloseEditProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogEdit.dismiss();
            }
        });


    }
    private void getSupervisorList(){

        //Toast.makeText(this, "hello gett user list", Toast.LENGTH_SHORT).show();
        aa = new ArrayList<String>();
        WebServiceCalls.getSupervisorList(userid,
                new NwCall(llGetApproveProgress) {

                    @Override
                    public void onSuccess(String msg) {

                        PopMessage.makeshorttoast(ProjectDetails.this, msg);
                        //approverListAdapter.notifyDataSetChanged();
                    }


                    @Override
                    public void onFailure(String msg) {
                        PopMessage.makeshorttoast(ProjectDetails.this, msg);

                    }

                    @Override
                    public void onSuccess(Bundle msg) {

                         userSupervisorLists = msg.getParcelableArrayList("supervisorList");
                        if(userSupervisorLists.size()>0){

                            for(int k=0; k< userSupervisorLists.size(); k++) {
                                aa.add(userSupervisorLists.get(k).getUsername() + "  ("+ userSupervisorLists.get(k).getDesignationname()+")");
                            }
                        }else{
                            Toast.makeText(ProjectDetails.this, "error", Toast.LENGTH_SHORT).show();
                        }
                        approverListAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, aa);
                        approverListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerProgressApprover.setAdapter(approverListAdapter);

                    }

                    @Override
                    public void onFailure(Bundle msg) {

                    }
                });

    }


    private void ShowApproveProgressTypeUI(){
        final Dialog dialogApprove = Utility.createDialog(this, R.layout.layoutnew_approve_progress, true);
        final TextView setProjectName = (TextView) dialogApprove.findViewById(R.id.setProjectName);
        spinnerProgressApprover = (Spinner)dialogApprove.findViewById(R.id.spinner_progress_approver);
        final Button btnCcloseApproveProgress = (Button) dialogApprove.findViewById(R.id.btnclose_approveprogress);
        final RecyclerView recyclerViewApproveProgress = (RecyclerView) dialogApprove.findViewById(R.id.approveprogressList);
        final TextView progresstext = (TextView) dialogApprove.findViewById(R.id.approvalid);
        setProjectName.setText(project.getName());
        final String id = project.getId();
        getSupervisorList();



        spinnerProgressApprover.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getApproverName=   spinnerProgressApprover.getItemAtPosition(spinnerProgressApprover.getSelectedItemPosition()).toString();
                getApproverId = userSupervisorLists.get(i).getUserid();
                getApproverDesignation = userSupervisorLists.get(i).getDesignationname();
                //Toast.makeText(ProjectDetails.this, ""+getApproverId+"   "+getApproverDesignation, Toast.LENGTH_SHORT).show();

                final ProgressDialog progressDoalog;
                progressDoalog = new ProgressDialog(ProjectDetails.this);
                //progressDoalog.setMax(100);
                progressDoalog.setMessage("Its loading....");
                progressDoalog.setTitle("Please wait it take some time ");
                //progressDoalog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                // show it
                progressDoalog.show();
                WebServiceCalls.getApproveProgress(id,userid,userDesignation,
                        new NwCall(llGetApproveProgress) {

                            @Override
                            public void onSuccess(String msg) {
                                PopMessage.makeshorttoast(ProjectDetails.this, msg);

                            }


                            @Override
                            public void onFailure(String msg) {
                                PopMessage.makeshorttoast(ProjectDetails.this, msg);

                            }

                            @Override
                            public void onSuccess(Bundle msg) {
                                ArrayList<ResponseDataForApprove> responseDataForApprove = msg.getParcelableArrayList("approveprogressdata");
                                LinearLayoutManager recylerViewLayoutManagerApproveProgress = new LinearLayoutManager(ProjectDetails.this);
                                recyclerViewApproveProgress.setLayoutManager(recylerViewLayoutManagerApproveProgress);

                                if(responseDataForApprove.size() == 0){
                                    progresstext.setVisibility(View.VISIBLE);
                                    progressDoalog.dismiss();

                                }else {
                                    recyclerViewApproveProgress.setVisibility(View.VISIBLE);
                                    //Toast.makeText(ProjectDetails.this, "wowwwww", Toast.LENGTH_SHORT).show();
                                    RecyclerView.Adapter recyclerViewAdapterApproveProgress = new RecyclerViewAdapterApproveProgress(ProjectDetails.this, responseDataForApprove, dialogApprove,id,userDesignation,getApproverId,getApproverDesignation);
                                    recyclerViewApproveProgress.setAdapter(recyclerViewAdapterApproveProgress);
                                    progressDoalog.dismiss();
                                    dialogProgress.show();
                                }
                            }

                            @Override
                            public void onFailure(Bundle msg) {
                                progressDoalog.dismiss();
                            }
                        });


                btnCcloseApproveProgress.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogApprove.dismiss();
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });




        }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        InputStream in;

        if (resultCode == Activity.RESULT_OK) {
            Bitmap bm = null;
            if (requestCode == 1) {
                bm = onSelectFromGalleryResult(data);
                imguri = data.getData();


            } else if (requestCode == 0) {
                bm = onCaptureImageResult(data);
                imguri = cameraUri;
            }

            if(imguri !=null){
                ParcelFileDescriptor parcelFileDescriptor = null;

            /*
            How to convert the Uri to FileDescriptor, refer to the example in the document:
            https://developer.android.com/guide/topics/providers/document-provider.html
             */
                try {
//                parcelFileDescriptor = getContentResolver().openFileDescriptor(imguri, "r");
//                FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
                    in = getContentResolver().openInputStream(imguri);
                    ExifInterface exifInterface = new ExifInterface(in);

                    // ExifInterface exifInterface = new ExifInterface(fileDescriptor);
                    String exif="Exif: " + in.toString();
                    exif += "\nIMAGE_LENGTH: " +
                            exifInterface.getAttribute(ExifInterface.TAG_IMAGE_LENGTH);
                    exif += "\n TAG_GPS_LATITUDE: " +
                            exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
                    exif += "\n TAG_GPS_LATITUDE_REF: " +
                            exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
                    exif += "\n TAG_GPS_LONGITUDE: " +
                            exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
                    exif += "\n TAG_GPS_LONGITUDE_REF: " +
                            exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);

                   //
                    //Toast.makeText(this, "hsgdhgf"+exif, Toast.LENGTH_LONG).show();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Something wrong:\n" + e.toString(),
                            Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Something wrong:\n" + e.toString(),
                            Toast.LENGTH_LONG).show();
                }
            }
            if (bm != null){


                String filename = SaveImage(bm);
                ArrayList<String> paths = new ArrayList<String>(2);
                paths.add(filename);

                Intent intent = new Intent(ProjectDetails.this, UploadingImageService.class);
                intent.putExtra("paths", paths);
                intent.putExtra("project_id", project.getId());
                startService(intent);



                List<Image> images = project.getImages();
                Image newImage = new Image();
                newImage.setUrl("file://" + paths.get(0));
                newImage.setTittle("ProjectImage");
                images.add(newImage);
                project.setImages(images);

                return;
            }
        }

        if (requestCode == com.veer.multiselect.Util.Constants.REQUEST_CODE_MULTISELECT
                && resultCode == RESULT_OK) {
            ArrayList<String> paths = data.getStringArrayListExtra(com.veer.multiselect.Util.Constants.GET_PATHS);
            Intent intent = new Intent(ProjectDetails.this, UploadingImageService.class);
            intent.putExtra("paths", paths);
            intent.putExtra("project_id", project.getId());
            startService(intent);

            List<Image> images = project.getImages();
            Image newImage = new Image();
            newImage.setUrl("file://" + paths.get(0));
            newImage.setTittle("ProjectImage");
            images.add(newImage);
            project.setImages(images);
        }
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


    private String SaveImage(Bitmap finalBitmap) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/urmis");
        myDir.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fname = "Image-" + timeStamp + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return myDir + "/" + fname;
    }

    @SuppressWarnings("deprecation")
    private Bitmap onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return bm;
        // ivImage.setImageBitmap(bm);
    }

    private Bitmap onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        MediaScannerConnection.scanFile(this, new String[] { destination.getPath() }, new String[] { "image/jpeg" }, null);
        FileOutputStream fo;
        cameraUri = Uri.fromFile(destination);
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return thumbnail;

    }

    private void setData() {
        tvProjectName.setText(project.getName());
        tvDistrict.setText(project.getDistrict());
        tvRiver.setText(project.getRiver());
        tvRiverBank.setText(project.getRiverBank());
        tvTacApproved.setText(project.getTacApproved());
        tvFundConsumed.setText("Rs." + project.getFundConsumed());
        tvFundSanctioned.setText("Rs." + project.getFundSanction());
        tvPercentageCompletion.setText(project.getPercentage_completed() + "%");

        if (project.getImages().size() != 0) {
            try {
                Picasso.with(this).load(project.getImages().get(0).getUrl().replaceAll(" ", "%20")).placeholder(R.drawable.placeholder).into(projectImage);
            } catch (Exception e) {

            }
        }

        setUpSalientFeatureList();
        setUpStructuralDetailsList();
        //setUpWorkStatementList();
    }



    private void setUpSalientFeatureList() {
        rcSalientFeature.setLayoutManager(new LinearLayoutManager(this));
        CommonRecyclerViewAdapter<SalientFeature> adapter =
                new CommonRecyclerViewAdapter<SalientFeature>(this, project.getSalientFeatures(),
                        R.layout.layout_list_salient_feature) {
                    @Override
                    public void onPostBindViewHolder(ViewHolder holder, SalientFeature salientFeature) {
                        holder.setViewText(R.id.attribute, salientFeature.getAttribute()).
                                setViewText(R.id.value, salientFeature.getValue());
                    }
                };

        rcSalientFeature.setAdapter(adapter);

    }

    private void setUpStructuralDetailsList() {
        rcstructuralDetails.setLayoutManager(new LinearLayoutManager(this));
        CommonRecyclerViewAdapter<ListOfWorkStatement> adapterListofWorkStatement =
                new CommonRecyclerViewAdapter<ListOfWorkStatement>(this, project.getListOfWorkStatement(),
                        R.layout.layout_list_structural_details) {

                    @Override
                    public void onPostBindViewHolder(ViewHolder holder, ListOfWorkStatement listOfWorkStatement) {
                        holder.setViewText(R.id.name, listOfWorkStatement.getTittle()).
                                setViewText(R.id.valuestructuraldetails, listOfWorkStatement.getValue());
                    }
                };

        rcstructuralDetails.setAdapter(adapterListofWorkStatement);

    }

    private void initViews() {
        tvProjectName = (TextView) findViewById(R.id.tittle);
        tvDistrict = (TextView) findViewById(R.id.district);
        tvRiver = (TextView) findViewById(R.id.river);
        tvRiverBank = (TextView) findViewById(R.id.riverbank);
        tvTacApproved = (TextView) findViewById(R.id.tac);
        tvFundSanctioned = (TextView) findViewById(R.id.fundSanctioned);
        tvFundConsumed = (TextView) findViewById(R.id.fundConsumed);
        tvPercentageCompletion = (TextView) findViewById(R.id.percentage);
        tvUpdatePercentage = (TextView) findViewById(R.id.viewProgress);
        tvSalientFeatures = (TextView) findViewById(R.id.salientFeatture);
        tvGallery = (TextView) findViewById(R.id.gallery);
        tvUploadImages = (TextView) findViewById(R.id.uploadImages);
        pbar = (ProgressBar) findViewById(R.id.pbar);

        llsalientHeader = (LinearLayout) findViewById(R.id.salientHeader);
        projectImage = (ImageView) findViewById(R.id.image);
        rcSalientFeature = (RecyclerView) findViewById(R.id.rcSalientFeature);

        llstructuralDetails = (LinearLayout) findViewById(R.id.llstructuralDetails);
        rcstructuralDetails = (RecyclerView) findViewById(R.id.rcStructuralDetails);
        tvSructuralDetails = (TextView) findViewById(R.id.structuralDetails);

        lvWorkStatements = (ExpandableListView) findViewById(R.id.rcWorkStatments);

        rlGetProgress = (RelativeLayout) findViewById(R.id.rlGetProgress);
        llGetProgress = (LinearLayout) findViewById(R.id.llGetProgress);
        llGetEditProgress = (LinearLayout) findViewById(R.id.llGetEditProgress);
        llGetApproveProgress = (LinearLayout) findViewById(R.id.llGetApproveProgress);
        llbtn_hide = (LinearLayout) findViewById(R.id.llbtn_hide);


    }


}