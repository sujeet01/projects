package com.urmis.Projects;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.urmis.Network.ProgressRequestBody;
import com.urmis.Network.Retrofit.RFClient;
import com.urmis.Network.Retrofit.RFInterface;
import com.urmis.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by KaranVeer on 17-05-2017.
 */

public class UploadingImageService extends IntentService {


    public UploadingImageService(String name) {
        super(name);
    }

    public UploadingImageService() {
        super("UploadingImageService");
    }

    private static final int MY_NOTIFICATION_ID = 1;
    NotificationManager notificationManager;
    Notification myNotification;
    private NotificationCompat.Builder mBuilder;
    ProgressDialog pd;


    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ArrayList<String> paths = intent.getStringArrayListExtra("paths");
        String project_id = intent.getStringExtra("project_id");
        sendImages(paths, project_id);
        //send(paths,project_id);
        //senda(paths,project_id);
        //sendB(paths,project_id);
    }

    private void sendB(ArrayList<String> paths, String project_id) {
        final File file = new File(paths.get(0));
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
        MultipartBody.Part filePart1 = MultipartBody.Part.createFormData("file", project_id, RequestBody.create(MediaType.parse("text/plain"), project_id));
        RFInterface rfInterface = RFClient.getClient().create(RFInterface.class);
        rfInterface.uploadAttachment(filePart1,filePart).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("body",new Gson().toJson(response));
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("error","r"+t.getMessage());
            }
        });
    }

    private void senda(ArrayList<String> paths, String project_id) {
        final File file = new File(paths.get(0));
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), project_id);
        RequestBody fbody = RequestBody.create(MediaType.parse("image/*"), file);
        RFInterface rfInterface = RFClient.getClient().create(RFInterface.class);
        rfInterface.uploadImages(project_id,name).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("body",new Gson().toJson(response));
                //Toast.makeText(UploadingImageService.this, "image is successfully uploaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("error","r"+t.getMessage());
                //Toast.makeText(UploadingImageService.this, "error in image upload", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void send(ArrayList<String> paths, String project_id) {
        final File file = new File(paths.get(0));
        ProgressRequestBody progressRequestBody =
                new ProgressRequestBody(MediaType.parse("image/*"), file,
                        new ProgressRequestBody.UploadCallbacks() {
                            @Override
                            public void onProgressUpdate(int percentage) {
                                sendNotification("Uploading Image" + file.getName(),
                                        "uploading..." + percentage);


                            }

                            @Override
                            public void onError() {
                                sendNotification("Uploading Image " + file.getName(), "Error");
                            }

                            @Override
                            public void onFinish() {
                                sendNotification("Uploading Image " + file.getName(),
                                        "uploading..." + 100);
                            }
                        });

        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), project_id);
        RFInterface rfInterface = RFClient.getClient().create(RFInterface.class);
        rfInterface.uploadImages(name,progressRequestBody).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("body",new Gson().toJson(response));

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("error","r"+t.getMessage());
            }
        });
    }


    public void showToast(String message) {
        final String msg = message;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void sendImages(ArrayList<String> paths, String project_id) {
        MultipartBody imageMap = getImageMap(paths, project_id);
        RFInterface rfInterface = RFClient.getClient().create(RFInterface.class);
        rfInterface.uploadProjectImages(imageMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("body",new Gson().toJson(response));
                Toast.makeText(UploadingImageService.this, "image is successfully uploaded", Toast.LENGTH_SHORT).show();
               // showToast("hello");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("error","r"+t.getMessage());
                Toast.makeText(UploadingImageService.this, "error in image upload", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private MultipartBody getImageMap(ArrayList<String> paths, String id) {

        MultipartBody.Builder requestBodyBuilder =
                new MultipartBody.Builder().setType(MultipartBody.FORM);

        final String image = paths.get(0);
        final File file = new File(image);
        ProgressRequestBody progressRequestBody =
                new ProgressRequestBody(MediaType.parse("image/*"), file,
                        new ProgressRequestBody.UploadCallbacks() {
                            @Override
                            public void onProgressUpdate(int percentage) {

                                sendNotification("Uploading Image" + file.getName(),
                                        "uploading..." + percentage);
                            }

                            @Override
                            public void onError() {
                                sendNotification("Uploading Image " + file.getName(), "Error");
                            }

                            @Override
                            public void onFinish() {
                                sendNotification("Uploading Image " + file.getName(),
                                        "uploading..." + 100);
                            }
                        });


        requestBodyBuilder.addFormDataPart("userfile", "photo" +    file.getName()+ ".jpg",
                progressRequestBody);
        RequestBody txt_id = RequestBody.create(MediaType.parse("text/plain"), id);
        requestBodyBuilder.addFormDataPart("project_id", null, txt_id);
        return requestBodyBuilder.build();
    }

    private void sendNotification(String tittle, String msg) {
        myNotification = new NotificationCompat.Builder(getApplicationContext()).setContentTitle(tittle)
                .setContentText(msg)
                .setTicker(tittle)
                .setSmallIcon(R.drawable.ic_cloud_upload_white_24dp)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .build();

        notificationManager.notify(MY_NOTIFICATION_ID, myNotification);






    }


}
