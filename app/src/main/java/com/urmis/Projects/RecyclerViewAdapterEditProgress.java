package com.urmis.Projects;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.urmis.Authenticate.LoginActivity;
import com.urmis.Network.Callback.NwCall;
import com.urmis.Network.WebServiceCalls;
import com.urmis.Repository.AppPrefs;
import com.urmis.Repository.Models.EditProgress.ResponseEditProgress;
import com.urmis.R;
import com.urmis.Repository.Models.User;
import com.urmis.Utils.PopMessage;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by JUNED on 6/10/2016.
 */
public class RecyclerViewAdapterEditProgress extends RecyclerView.Adapter<RecyclerViewAdapterEditProgress.ViewHolder>{


    private static Context mcontext;
    View view1;
    ViewHolder viewHolder1;
    static TextView textView;
    Float totalFinancialBalance,totalPhysicalBalance;
    private ArrayList<ResponseEditProgress> responseEditProgress;
    int size;
    String projectid,monthlyprogressid,valuefinancial,valuephysical,datefinancial,datephysical;
    String financialvalue,physicalvalue,ProgressApproverId,ProgressApproverDesignation;


    public RecyclerViewAdapterEditProgress(Context context1 , ArrayList<ResponseEditProgress> responseEditProgress,String projectid,Float totalFinancialBalance,Float totalPhysicalBalance,String ProgressApproverId,String ProgressApproverDesignation){

        this.responseEditProgress = responseEditProgress;
        this.mcontext = context1;
        this.projectid = projectid;
        this.totalFinancialBalance = totalFinancialBalance;
        this.totalPhysicalBalance = totalPhysicalBalance;
        this.ProgressApproverId = ProgressApproverId;
        this.ProgressApproverDesignation = ProgressApproverDesignation;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txt_date,txt_financialvalue,txt_physicalvalue,txt_opr,txt_cancel,txt_update,txt_balanceFinancial,txt_balancePhysical;
        public EditText edt_financialvalue,edt_physicalvalue;
        public LinearLayout layoutshow,layouthide;


        public ViewHolder(View v){

            super(v);
            txt_date = (TextView)v.findViewById(R.id.txt_date);
            txt_financialvalue = (TextView)v.findViewById(R.id.txt_financialvalue);
            txt_physicalvalue = (TextView)v.findViewById(R.id.txt_physicalvalue);
            edt_financialvalue = (EditText)v.findViewById(R.id.edt_financialvalue);
            edt_physicalvalue = (EditText)v.findViewById(R.id.edt_physicalvalue);
            txt_opr = (TextView)v.findViewById(R.id.txt_opr);
            txt_cancel = (TextView)v.findViewById(R.id.txt_cancel);
            txt_update = (TextView)v.findViewById(R.id.txt_update);
            layouthide = (LinearLayout)v.findViewById(R.id.layouthide);
            layoutshow = (LinearLayout)v.findViewById(R.id.layoutshow);

            txt_balanceFinancial= (TextView)v.findViewById(R.id.txt_balancefinancialvalue);
            txt_balancePhysical = (TextView)v.findViewById(R.id.txt_balancephysicalvalue);



            //txt_details.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrow_down,0);


        }
    }

    @Override
    public RecyclerViewAdapterEditProgress.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        view1 = LayoutInflater.from(mcontext).inflate(R.layout.recyclerview_editprogress,parent,false);

        viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position){

       // String tpb = Float.toString(totalPhysicalBalance);

        String date = responseEditProgress.get(position).getPhysicalProgressDate();
        financialvalue = responseEditProgress.get(position).getFinancialProgressQuantityValue();
        physicalvalue = responseEditProgress.get(position).getPhysicalProgressQuantityValue();
        holder.txt_date.setText("Date:"+" "+date);
        holder.txt_financialvalue.setText(financialvalue);
        holder.txt_physicalvalue.setText(physicalvalue);
        holder.txt_balancePhysical.setText(totalPhysicalBalance.toString());
        holder.txt_balanceFinancial.setText(totalFinancialBalance.toString());


        if(responseEditProgress.get(position).getProgressapproved().equals("True")){
            holder.txt_opr.setVisibility(View.GONE);

        }
        holder.txt_opr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.layouthide.getVisibility() != View.VISIBLE){
                    holder.layouthide.setVisibility(View.VISIBLE);
                    holder.layoutshow.setVisibility(View.GONE);
                    holder.edt_financialvalue.setText(holder.txt_financialvalue.getText());
                    holder.edt_physicalvalue.setText(holder.txt_physicalvalue.getText());
                    holder.txt_opr.setVisibility(View.GONE);
                    holder.txt_update.setVisibility(View.VISIBLE);
                    holder.txt_cancel.setVisibility(View.VISIBLE);
                }
            }
        });
        holder.txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.layouthide.setVisibility(View.GONE);
                holder.layoutshow.setVisibility(View.VISIBLE);
                holder.txt_cancel.setVisibility(View.GONE);
                holder.txt_update.setVisibility(View.GONE);
                holder.txt_opr.setVisibility(View.VISIBLE);
            }
        });
        holder.txt_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                monthlyprogressid = responseEditProgress.get(position).getMonthlyProgressId();
                datefinancial = responseEditProgress.get(position).getFinancialProgressDate();
                datephysical = responseEditProgress.get(position).getPhysicalProgressDate();
                valuefinancial = holder.edt_financialvalue.getText().toString();
                valuephysical = holder.edt_physicalvalue.getText().toString();

                Float compareFinancialValue = totalFinancialBalance + Float.parseFloat(holder.txt_financialvalue.getText().toString());
                Float comparePhysicalValue = totalPhysicalBalance + Float.parseFloat(holder.txt_physicalvalue.getText().toString());

               // Toast.makeText(mcontext, "finacial"+compareFinancialValue, Toast.LENGTH_SHORT).show();

               // Toast.makeText(mcontext, "physical"+comparePhysicalValue, Toast.LENGTH_SHORT).show();

                if (valuefinancial.trim().length() == 0) {
                    Toast.makeText(mcontext, "Please enter Financial Value", Toast.LENGTH_SHORT).show();

                } else if (valuephysical.trim().length() == 0) {
                    Toast.makeText(mcontext, "Please enter Physical Value", Toast.LENGTH_SHORT).show();

                } else if(Float.parseFloat(valuefinancial) > compareFinancialValue)
                {
                    Toast.makeText(mcontext, "Financial value must be less than"+compareFinancialValue, Toast.LENGTH_SHORT).show();
                }else if(Float.parseFloat(valuephysical) > comparePhysicalValue)
                {
                    Toast.makeText(mcontext, "Physical value must be less than"+comparePhysicalValue, Toast.LENGTH_SHORT).show();
                }else{

                    updateProjectMonthlyProgress(projectid, monthlyprogressid, valuefinancial, valuephysical, datefinancial, datephysical,ProgressApproverId,ProgressApproverDesignation);
                }
            }
        });
    }

    private void updateProjectMonthlyProgress(String projectid,String monthlyprogressid,String valuefinancial,String valuephysical,String datefinancial,String datephysical,String progressApproverId,String progressApproverDesignation) {
        WebServiceCalls.updateProjectMonthlyProgress(projectid, monthlyprogressid, valuefinancial,valuephysical,datefinancial,datephysical,progressApproverId,progressApproverDesignation, new NwCall(mcontext, "Updating..") {
            @Override public void onSuccess(String msg) {
                PopMessage.makeshorttoast(mcontext, msg);

            }

            @Override public void onFailure(String msg) {
                PopMessage.makeshorttoast(mcontext, msg);
            }

            @Override public void onSuccess(Bundle msg) {

                PopMessage.makeshorttoast(mcontext, "Successfully Updated..");

            }

            @Override public void onFailure(Bundle msg) {

            }
        });
    }

    @Override
    public int getItemCount(){

        return responseEditProgress.size();
    }
}
