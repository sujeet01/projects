package com.urmis.Projects;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.urmis.Authenticate.LoginActivity;
import com.urmis.Network.Callback.NwCall;
import com.urmis.Network.WebServiceCalls;
import com.urmis.Repository.AppPrefs;
import com.urmis.Repository.Models.EditProgress.ResponseDataForApprove;
import com.urmis.Repository.Models.EditProgress.ResponseEditProgress;
import com.urmis.R;
import com.urmis.Repository.Models.User;
import com.urmis.Utils.PopMessage;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by JUNED on 6/10/2016.
 */
public class RecyclerViewAdapterApproveProgress extends RecyclerView.Adapter<RecyclerViewAdapterApproveProgress.ViewHolder>{


    private static Context mcontext;
    View view1;
    ViewHolder viewHolder1;
    static TextView textView;
    private ArrayList<ResponseDataForApprove> responseDataForApprove;
    int size;
    String projectid,monthlyprogressid,userDesignation,progressApproverId,progressApproverDesignation;
    Dialog dialogApprove;


    public RecyclerViewAdapterApproveProgress(Context context1 , ArrayList<ResponseDataForApprove> responseDataForApprove,Dialog dialogApprove,String projectid,String userDesignation,String progressApproverId,String progressApproverDesignation){

        this.responseDataForApprove = responseDataForApprove;
        this.mcontext = context1;
        this.projectid = projectid;
        this.dialogApprove = dialogApprove;
        this.userDesignation = userDesignation;
        this.progressApproverId= progressApproverId;
        this.progressApproverDesignation = progressApproverDesignation;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txt_date,txt_financialvalue,txt_physicalvalue,txt_oprapprove,txt_opr,txt_oprnotapprove;
        LinearLayout llbalance_heading,llbalance_headingvalue;



        public ViewHolder(View v){

            super(v);
            txt_date = (TextView)v.findViewById(R.id.txt_date);
            txt_financialvalue = (TextView)v.findViewById(R.id.txt_financialvalue);
            txt_physicalvalue = (TextView)v.findViewById(R.id.txt_physicalvalue);
            txt_oprapprove = (TextView)v.findViewById(R.id.txt_oprapprove);
            txt_oprnotapprove = (TextView)v.findViewById(R.id.txt_oprnotapprove);
            txt_opr = (TextView)v.findViewById(R.id.txt_opr);
            llbalance_heading = (LinearLayout)v.findViewById(R.id.llbalance_heading);
            llbalance_headingvalue = (LinearLayout)v.findViewById(R.id.llbalance_headingvalue);

        }
    }

    @Override
    public RecyclerViewAdapterApproveProgress.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        view1 = LayoutInflater.from(mcontext).inflate(R.layout.recyclerview_editprogress,parent,false);

        viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position){

        holder.txt_opr.setVisibility(View.GONE);
        holder.txt_oprapprove.setVisibility(View.VISIBLE);
        holder.txt_oprnotapprove.setVisibility(View.VISIBLE);
        String date = responseDataForApprove.get(position).getPhysicaldate();
        final String financialvalue = responseDataForApprove.get(position).getFinancialvalue();
        final String physicalvalue = responseDataForApprove.get(position).getPhysicalvalue();
        holder.txt_date.setText("Date:"+" "+date);
        holder.txt_financialvalue.setText(financialvalue);
        holder.txt_physicalvalue.setText(physicalvalue);
        holder.llbalance_heading.setVisibility(View.GONE);
        holder.llbalance_headingvalue.setVisibility(View.GONE);

        monthlyprogressid = responseDataForApprove.get(position).getMonthlyProgressId();

        holder.txt_oprapprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approvedProjectMonthlyProgress(projectid,monthlyprogressid,userDesignation,progressApproverId,progressApproverDesignation);
            }
        });

        holder.txt_oprnotapprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notApprovedProjectMonthlyProgress(projectid,monthlyprogressid);
            }
        });

    }

    private void approvedProjectMonthlyProgress(String projectid,String monthlyprogressid,String userDesignation,String progressApproverId,String progressApproverDesignation) {
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(mcontext);
        progressDoalog.setMessage("Its loading....");
        progressDoalog.setTitle("Please wait it take some time ");
        progressDoalog.show();
       // Toast.makeText(mcontext, "hello"+projectid+"      "+monthlyprogressid+"   "+userDesignation+progressApproverId+progressApproverDesignation, Toast.LENGTH_SHORT).show();
        WebServiceCalls.approveProjectMonthlyProgress(projectid, monthlyprogressid,userDesignation,progressApproverId,progressApproverDesignation, new NwCall(mcontext, "Approving..") {

            @Override public void onSuccess(String msg) {
                PopMessage.makeshorttoast(mcontext, msg);
                progressDoalog.dismiss();
                dialogApprove.dismiss();
            }

            @Override public void onFailure(String msg) {
                PopMessage.makeshorttoast(mcontext, msg);
                progressDoalog.dismiss();

            }


            @Override public void onSuccess(Bundle msg) {
                PopMessage.makeshorttoast(mcontext, "Successfully Approved..");

                progressDoalog.dismiss();
            }

            @Override public void onFailure(Bundle msg) {
                progressDoalog.dismiss();
            }
        });
   }


    private void notApprovedProjectMonthlyProgress(String projectid,String monthlyprogressid) {
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(mcontext);
        progressDoalog.setMessage("Its loading....");
        progressDoalog.setTitle("Please wait it take some time ");
        progressDoalog.show();
        WebServiceCalls.notApproveProjectMonthlyProgress(projectid, monthlyprogressid, new NwCall(mcontext, "Not Approving..") {
            @Override public void onSuccess(String msg) {
                PopMessage.makeshorttoast(mcontext, msg);
                progressDoalog.dismiss();
                dialogApprove.dismiss();
            }

            @Override public void onFailure(String msg) {
                PopMessage.makeshorttoast(mcontext, msg);
                progressDoalog.dismiss();

            }


            @Override public void onSuccess(Bundle msg) {
                PopMessage.makeshorttoast(mcontext, "Not Approved Successfully..");

                progressDoalog.dismiss();
            }

            @Override public void onFailure(Bundle msg) {
                progressDoalog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount(){

        return responseDataForApprove.size();
    }
}
