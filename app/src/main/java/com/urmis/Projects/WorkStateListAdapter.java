package com.urmis.Projects;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import com.urmis.R;
import com.urmis.Repository.Models.Project.ListOfWorkStatement;
import com.urmis.Repository.Models.Project.Workitems;

public class WorkStateListAdapter extends BaseExpandableListAdapter {

  private Context _context;
  private List<ListOfWorkStatement> _listDataHeader; // header titles
  // child data in format of header title, child title
  //private HashMap<String, List<String>> _listDataChild;

  public WorkStateListAdapter(Context context, List<ListOfWorkStatement> listDataHeader) {
    this._context = context;
    this._listDataHeader = listDataHeader;
  }

  @Override public Object getChild(int groupPosition, int childPosititon) {
    return this._listDataHeader.get(groupPosition).getWorkItems().get(childPosititon);
  }

  @Override public long getChildId(int groupPosition, int childPosition) {
    return childPosition;
  }

  @Override
  public View getChildView(int groupPosition, final int childPosition, boolean isLastChild,
      View convertView, ViewGroup parent) {

    LayoutInflater infalInflater =
            (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if(childPosition == 0)
      return convertView = infalInflater.inflate(R.layout.layout_child_header, null);

    final Workitems child = (Workitems) getChild(groupPosition, childPosition-1);

    if (convertView == null) {

      convertView = infalInflater.inflate(R.layout.layout_workstatelist_child, null);
    }

    TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
    TextView quantity = (TextView) convertView.findViewById(R.id.quantity);
    TextView rate = (TextView) convertView.findViewById(R.id.rate);
    TextView cost = (TextView) convertView.findViewById(R.id.cost);


    quantity.setText(child.getQuantity());
    rate.setText(child.getRate());

    Float fQuant=Float.parseFloat(child.getQuantity());
    Float fRate=Float.parseFloat(child.getRate());
    Float fcost=fQuant*fRate;
    cost.setText(String.valueOf(fcost));
    txtListChild.setText(child.getName());
    return convertView;
  }

  @Override public int getChildrenCount(int groupPosition) {

    return this._listDataHeader.get(groupPosition).getWorkItems().size()+1;
  }

  @Override public Object getGroup(int groupPosition) {
    return this._listDataHeader.get(groupPosition);
  }

  @Override public int getGroupCount() {

    //Log.e("size", this._listDataHeader.size() + " ");
    return this._listDataHeader.size();
  }

  @Override public long getGroupId(int groupPosition) {
    return groupPosition;
  }

  @Override public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
      ViewGroup parent) {

      ListOfWorkStatement header = (ListOfWorkStatement) getGroup(groupPosition);
      if (convertView == null) {

          LayoutInflater infalInflater =
                  (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
          convertView = infalInflater.inflate(R.layout.layout_workstatelist_header, null);


    }
    TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
    TextView lblListValue = (TextView) convertView.findViewById(R.id.value);
    lblListHeader.setTypeface(null, Typeface.BOLD);
    lblListHeader.setText(header.getTittle());
    lblListValue.setText(header.getValue());

    if (isExpanded) {
      lblListValue.setCompoundDrawablesWithIntrinsicBounds(0,0,
          R.drawable.ic_arrow_drop_up_white_24dp, 0);
    } else {

      lblListValue.setCompoundDrawablesWithIntrinsicBounds(0,
            0, R.drawable.ic_arrow_drop_down_white_24dp, 0);

    }
    if(_listDataHeader.get(groupPosition).getWorkItems().size()==0)
    {
      lblListValue.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
    }
    return convertView;
  }

  @Override public boolean hasStableIds() {
    return false;
  }

  @Override public boolean isChildSelectable(int groupPosition, int childPosition) {
    return true;
  }
}