package com.urmis.Projects;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import com.urmis.Authenticate.LoginActivity;
import com.urmis.Network.Callback.NwCall;
import com.urmis.Network.WebServiceCalls;
import com.urmis.R;
import com.urmis.Repository.AppPrefs;
import com.urmis.Repository.Models.News;
import com.urmis.Repository.Models.Project.Project;
import com.urmis.Utils.Constants;
import com.urmis.Utils.PopMessage;
import com.urmis.Utils.RecyclerViewTools.CommonRecyclerViewAdapter;
import com.urmis.Utils.RecyclerViewTools.ViewHolder;

/**
 * Created by Brajendr on 4/10/2017.
 */

public class ProjectFragment extends Fragment {
    private View view;
    private RecyclerView rcProjects;
    private ArrayList<Project> projectList = new ArrayList<>();
    private RelativeLayout rlLogin;
    private TextView tvLogin;
    private AppPrefs appPrefs;
    private RelativeLayout rlLayout;
    private boolean wasAlreadyRunOnce;
    private LinearLayout insLinear;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String user_id;




    @Override
    public void onResume() {
        super.onResume();
        if(appPrefs.getUser().isLoggedIn() == true){
            rcProjects.setVisibility(View.VISIBLE);
            rlLogin.setVisibility(View.GONE);

        }else{
            rcProjects.setVisibility(View.GONE);
            rlLogin.setVisibility(View.VISIBLE);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_project, container, false);

        appPrefs = AppPrefs.getInstance(getActivity());
        init();
        getProjects();
        setUpList();
        setUpSwipeReferesh();

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.putExtra("navigate", Constants.NAVIGATE_TO_PROJECT);
                startActivityForResult(intent, Constants.NAVIGATE_TO_PROJECT);
            }
        });

       if(appPrefs.getUser().isLoggedIn() == true){
           rcProjects.setVisibility(View.VISIBLE);
           rlLogin.setVisibility(View.GONE);

       }else{
           rcProjects.setVisibility(View.GONE);
           rlLogin.setVisibility(View.VISIBLE);
       }



        return view;
    }

    private void setUpSwipeReferesh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if(appPrefs.getUser().isLoggedIn() == true){
                    rcProjects.setVisibility(View.VISIBLE);
                    rlLogin.setVisibility(View.GONE);

                }else{
                    rcProjects.setVisibility(View.GONE);
                    rlLogin.setVisibility(View.VISIBLE);
                }

                projectList = new ArrayList<>();
                setUpList();
                getProjects();
            }
        });
    }

    private void getProjects() {
        swipeRefreshLayout.setRefreshing(true);
        AppPrefs appPrefs = AppPrefs.getInstance(getActivity());
        user_id = appPrefs.getUser().getUser_id();
        WebServiceCalls.getProject(appPrefs.getUser().getUser_id(),
                new NwCall(rlLayout) {
                    @Override
                    public void onSuccess(String msg) {
                        swipeRefreshLayout.setRefreshing(false);
                        PopMessage.makeshorttoast(getActivity(), msg);
                    }

                    @Override
                    public void onFailure(String msg) {
                        swipeRefreshLayout.setRefreshing(false);
                        PopMessage.makeshorttoast(getActivity(), msg);
                        if(projectList.size()==0)
                        {
                            insLinear.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            insLinear.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onSuccess(Bundle msg) {
                        ArrayList<String> projectAccessors = new ArrayList<String>();
                        ArrayList<String> progressApproverAfterProgressenter = new ArrayList<String>();
                        swipeRefreshLayout.setRefreshing(false);
                        ArrayList<Project> projects = msg.getParcelableArrayList("projectList");
                        for(int i=0;i<projects.size();i++) {
                            projectAccessors = projects.get(i).getList_of_project_accessors();
                            progressApproverAfterProgressenter = projects.get(i).getlist_of_progress_approver_after_progressenter();
                            if (projectAccessors.contains(user_id) ||projects.get(i).getProject_creator().equals(user_id) || projects.get(i).getProgress_approver().equals(user_id) || progressApproverAfterProgressenter.contains(user_id)) {
                               projectList.add(projects.get(i));
                            }
                        }
                       // projectList.addAll(projects);
                        rcProjects.getAdapter().notifyDataSetChanged();
                        if(projects.size()==0)
                        {
                         insLinear.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            insLinear.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Bundle msg) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
    }

    private void setUpList() {
        rcProjects.setLayoutManager(new LinearLayoutManager(getActivity()));
        CommonRecyclerViewAdapter<Project> adapter =
                new CommonRecyclerViewAdapter<Project>(getActivity(), projectList,
                        R.layout.layout_project_list) {
                    @Override
                    public void onPostBindViewHolder(ViewHolder holder, final Project project) {
                        holder.setViewText(R.id.tvProjectTittle, project.getName())
                                .setViewText(R.id.district, project.getDistrict())
                                .setViewText(R.id.river, project.getRiver())
                                .setViewText(R.id.progress, project.getPercentage_completed() + "%");
                        holder.getView(R.id.tvViewDetails).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Bundle b=new Bundle();
                              b.putSerializable("key",project.getlist_of_progress_approver_after_progressenter());
                                Intent intent = new Intent(getActivity(), ProjectDetails.class);
                                intent.putExtra("project", project);
                               intent.putExtra("project_creator",b);
                               intent.putExtras(b);
//                                intent.putExtra("progress_approver",project.getProgress_approver());
                             //   intent.putExtra("progress_approver_name",project.getProgress_approver_name());
                      //       intent.putExtra("progress_approver_designation",project.getProgress_approver_designation());
                                startActivity(intent);
                            }
                        });
                    }
                };
        rcProjects.setAdapter(adapter);
    }

    private void init() {
        rcProjects = (RecyclerView) view.findViewById(R.id.rcProject);
        rlLogin = (RelativeLayout) view.findViewById(R.id.rlLogin);
        tvLogin = (TextView) view.findViewById(R.id.btnLogin);
        swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        insLinear=(LinearLayout)view.findViewById(R.id.layout_instructions);
    }

    public void reloadFragment(boolean isLoggedIn) {

        if (isLoggedIn) {
            rlLogin.setVisibility(View.GONE);
            rcProjects.setVisibility(View.VISIBLE);
            getProjects();
        } else {
            rlLogin.setVisibility(View.VISIBLE);
            rcProjects.setVisibility(View.GONE);

        }
    }
}
