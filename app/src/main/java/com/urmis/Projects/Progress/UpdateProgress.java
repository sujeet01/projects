package com.urmis.Projects.Progress;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateProgress implements Parcelable {



@SerializedName("id")
@Expose
private String id;

    @SerializedName("projectMonthlyProgressManager")
    @Expose
    private List<ProjectMonthlyProgressManager> projectMonthlyProgressManager;

    public List<ProjectMonthlyProgressManager> getprojectMonthlyProgressManager() {
        return projectMonthlyProgressManager;
    }

    public void setprojectMonthlyProgressManager(List<ProjectMonthlyProgressManager> projectMonthlyProgressManager) {
        this.projectMonthlyProgressManager = projectMonthlyProgressManager;
    }



public String getProjectId() {
return id;
}
public void setProjectId(String projectId) {
this.id = projectId;
}



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(this.id);
        dest.writeTypedList(this.projectMonthlyProgressManager);

    }

    public UpdateProgress() {
    }

    protected UpdateProgress(Parcel in) {
        this.id = in.readString();
        this.projectMonthlyProgressManager = in.createTypedArrayList(ProjectMonthlyProgressManager.CREATOR);

    }

    public static final Parcelable.Creator<UpdateProgress> CREATOR = new Parcelable.Creator<UpdateProgress>() {
        @Override
        public UpdateProgress createFromParcel(Parcel source) {
            return new UpdateProgress(source);
        }

        @Override
        public UpdateProgress[] newArray(int size) {
            return new UpdateProgress[size];
        }
    };
}