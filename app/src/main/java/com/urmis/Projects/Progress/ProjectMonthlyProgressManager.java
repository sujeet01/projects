package com.urmis.Projects.Progress;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProjectMonthlyProgressManager implements Parcelable{

    @SerializedName("ProjectId")
    @Expose
    private String ProjectId;


    @SerializedName("ProgressValue")
@Expose
private String ProgressValue;
    @SerializedName("ProgressFinancialValue")
    @Expose
    private String ProgressFinancialValue;
@SerializedName("CumulativeFinacialQuantity")
@Expose
private String CumulativeFinacialQuantity;
@SerializedName("CumulativePhysicalQuantity")
    @Expose
    private String CumulativePhysicalQuantity;

    @SerializedName("FinancialProgressDate")
    @Expose
    private String FinancialProgressDate;

    @SerializedName("PhysicalProgressDate")
    @Expose
    private String PhysicalProgressDate;



    public String getProjectId() {
        return ProjectId;
    }
    public void setProjectId(String ProjectId) {
        this.ProjectId = ProjectId;
    }

    public String getProgressValue() {
return ProgressValue;
}
public void setProgressValue(String ProgressValue) {
this.ProgressValue = ProgressValue;
}

    public String getProgressFinancialValue() {
        return ProgressFinancialValue;
    }

    public void setProgressFinancialValue(String ProgressFinancialValue) {
        this.ProgressFinancialValue = ProgressFinancialValue;
    }


    public String getCumulativeFinacialQuantity() {
return CumulativeFinacialQuantity;
}

public void setCumulativeFinacialQuantity(String CumulativeFinacialQuantity) {
this.CumulativeFinacialQuantity = CumulativeFinacialQuantity;
}


    public String getCumulativePhysicalQuantity() {
        return CumulativePhysicalQuantity;
    }

    public void setCumulativePhysicalQuantity(String CumulativePhysicalQuantity) {
        this.CumulativePhysicalQuantity = CumulativePhysicalQuantity;
    }

    public String getFinancialProgressDate() {
        return FinancialProgressDate;
    }

    public void setFinancialProgressDate(String FinancialProgressDate) {
        this.FinancialProgressDate = FinancialProgressDate;
    }


    public String getPhysicalProgressDate() {
        return PhysicalProgressDate;
    }

    public void setPhysicalProgressDate(String PhysicalProgressDate) {
        this.PhysicalProgressDate = PhysicalProgressDate;
    }

    public ProjectMonthlyProgressManager() {
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ProjectId);
        dest.writeString(this.ProgressValue);
        dest.writeString(this.ProgressFinancialValue);

        dest.writeString(this.CumulativeFinacialQuantity);
        dest.writeString(this.CumulativePhysicalQuantity);
        dest.writeString(this.FinancialProgressDate);
        dest.writeString(this.PhysicalProgressDate);
    }

    protected ProjectMonthlyProgressManager(Parcel in) {


        this.ProjectId = in.readString();
        this.ProgressValue = in.readString();
        this.ProgressFinancialValue = in.readString();
        this.CumulativeFinacialQuantity = in.readString();
        this.CumulativePhysicalQuantity = in.readString();
        this.FinancialProgressDate = in.readString();
        this.PhysicalProgressDate = in.readString();
    }

    public static final Parcelable.Creator<ProjectMonthlyProgressManager> CREATOR = new Parcelable.Creator<ProjectMonthlyProgressManager>() {
        @Override
        public ProjectMonthlyProgressManager createFromParcel(Parcel source) {
            return new ProjectMonthlyProgressManager(source);
        }

        @Override
        public ProjectMonthlyProgressManager[] newArray(int size) {
            return new ProjectMonthlyProgressManager[ size];
        }

    };
}

