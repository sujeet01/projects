package com.urmis.Map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.Manifest;
import android.widget.Toast;


import java.util.ArrayList;

import com.urmis.Alerts.AlertFragment;
import com.urmis.Application.URMIS;
import com.urmis.Authenticate.LoginActivity;

import com.urmis.Model.ModelFragment;
import com.urmis.Network.ConnectivityReceiver;
import com.urmis.Results.ResultsFragment;
import com.urmis.Projects.ProjectFragment;

import com.urmis.R;
import com.urmis.Report.ReportFragment;
import com.urmis.Repository.AppPrefs;
import com.urmis.Repository.Models.Shape.ShapeFileList;
import com.urmis.Repository.Models.User;
import com.urmis.Utils.Colors.Colors;
import com.urmis.Utils.Constants;

public class MainActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {



    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static final String[] tabNames = {"Map", "Alerts", "Project", "Model", "Results"};
    private static final int[] activeIcons = {
            R.drawable.ic_mapmarkerwhite, R.drawable.ic_alertwhite, R.drawable.ic_pen,
            R.drawable.ic_progressreport, R.drawable.ic_newswhite
    };
    private static final int[] inActiveIcons = {
            R.drawable.ic_mapmarkerblue, R.drawable.ic_alertblue, R.drawable.ic_penblue,
            R.drawable.ic_progressreportblue, R.drawable.ic_newsblue
    };
    private ArrayList<ShapeFileList> selectedShapeFiles = new ArrayList<>();
    private RelativeLayout rcLoading;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private TextView tvTittle;
    private Object provider;
    private RelativeLayout rlLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        FloatingActionButton floatingActionButton = (FloatingActionButton)findViewById(R.id.fab);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (toolbar != null) {
                    toolbar.setVisibility(View.VISIBLE);
                    hideLoadingView();
                    setSupportActionBar(toolbar);
                    tvTittle = (TextView) toolbar.findViewById(R.id.tvTittle);
                    tvTittle.setText("Urmis");
                    toolbar.setTitle(" ");
                }
            }
        }, 2000);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        rcLoading = (RelativeLayout) findViewById(R.id.rcLoading);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        rlLayout = (RelativeLayout) findViewById(R.id.rllayout);
        setUpTabLayout();
        initShapeFileList();
        onNewIntent(getIntent());
        checkLocationPermission();
        checkConnection();

    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;
        } else {
            message = "No network found. Please check your internet connection.";
            color = Color.RED;
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.fab), message, Snackbar.LENGTH_LONG);

            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        URMIS.getInstance().setConnectivityListener(this);
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle(R.string.title_location_permission)
                        .setMessage(R.string.text_location_permission)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

//    @Override protected void onNewIntent(Intent intent) {
//        if (intent != null) {
//            try {
//                Bundle extra=intent.getExtras();
//                String messageType = extra.getString("messageType");
//                if (messageType.equals("news")) {
//                    mViewPager.setCurrentItem(4);
//                } else if (messageType.equals("alert")) {
//                    mViewPager.setCurrentItem(1);
//                }
//            } catch (Exception e) {
//                Log.e("da", e.getMessage());
//            }
//        }
//    }

    
    public void hideLoadingView() {
        rcLoading.setVisibility(View.GONE);
    }

    private void initShapeFileList() {
        try {

            String[] shapes = getAssets().list("Shape");
            for (int i = 0; i < shapes.length; i++) {
                ShapeFileList shapeFileList = new ShapeFileList();
                shapeFileList.setChecked(true);
                shapeFileList.setName(shapes[i]);
                shapeFileList.setColor(Colors.getColor(i));
                selectedShapeFiles.add(shapeFileList);
            }
        } catch (Exception e) {
            Log.e("das", e.getMessage());
        }
    }

    private void setUpTabLayout() {
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager) {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setTittle(tabNames[tab.getPosition()]);
                updateTabView();

                AppPrefs appPrefs = AppPrefs.getInstance(MainActivity.this);
                if (!appPrefs.getUser().isLoggedIn()) {
                          if (tab.getPosition() == 2) {
                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            intent.putExtra("navigate", Constants.NAVIGATE_TO_PROJECT);
                            startActivity(intent);
                          } else if (tab.getPosition() == 3) {
                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            intent.putExtra("navigate", Constants.NAVIGATE_TO_MODEL);
                            startActivity(intent);
                          } else if (tab.getPosition() == 4) {
                              Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                              intent.putExtra("navigate", Constants.NAVIGATE_TO_NEWS);
                              startActivity(intent);
                          }
                        }

                if (tab.getPosition() != 2 || appPrefs.getUser().isLoggedIn())
                {
                    super.onTabSelected(tab);
                    setTittle(tabNames[tab.getPosition()]);
                    updateTabView();
                }
                else {
                    //finish();
                    Intent intentprojects = new Intent(MainActivity.this, LoginActivity.class);
                    intentprojects.putExtra("abc", "ur");
                    startActivityForResult(intentprojects, Constants.NAVIGATE_TO_PROJECT);
                    Toast.makeText(MainActivity.this, "You have to login first to view projects", Toast.LENGTH_LONG).show();
                }
                if (tab.getPosition() != 3 || appPrefs.getUser().isLoggedIn())
                {
                    super.onTabSelected(tab);
                    setTittle(tabNames[tab.getPosition()]);
                    updateTabView();
                }
                else {
                    //finish();
                    Intent intentmodel = new Intent(MainActivity.this, LoginActivity.class);
                    intentmodel.putExtra("abc", "ur");
                    startActivityForResult(intentmodel, Constants.NAVIGATE_TO_MODEL);
                    Toast.makeText(MainActivity.this, "You have to login first to view model", Toast.LENGTH_LONG).show();
                }

                if (tab.getPosition() != 4 || appPrefs.getUser().isLoggedIn())
                {
                    super.onTabSelected(tab);
                    setTittle(tabNames[tab.getPosition()]);
                    updateTabView();
                }
                else {
                    //finish();
                    Intent intentnews = new Intent(MainActivity.this, LoginActivity.class);
                    intentnews.putExtra("abc", "ur");
                    startActivityForResult(intentnews, Constants.NAVIGATE_TO_NEWS);
                    Toast.makeText(MainActivity.this, "You have to login first to view results", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });
        setTabView();
    }

    private void updateTabView() {
        int length = tabLayout.getTabCount();
        int pos = tabLayout.getSelectedTabPosition();
        for (int i = 0; i < length; i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            View view = tab.getCustomView();
            TextView tabOne = (TextView) view.findViewById(R.id.tabLabel);
            RelativeLayout rl = (RelativeLayout) view.findViewById(R.id.rl);
            if (i == pos) {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, activeIcons[i], 0, 0);
                tabOne.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                tabOne.setTextColor(getResources().getColor(R.color.colorAccent));
                rl.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            } else {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, inActiveIcons[i], 0, 0);
                tabOne.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                tabOne.setTextColor(getResources().getColor(R.color.colorPrimary));
                rl.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            }
        }
       // disableTabAt(3);
    }

    private void setTabView() {

        int length = tabLayout.getTabCount();
        int pos = tabLayout.getSelectedTabPosition();
        for (int i = 0; i < length; i++) {
            if (i == pos) {
                tabLayout.getTabAt(i).setCustomView(mSectionsPagerAdapter.getTabView(i, true));
            } else {
                tabLayout.getTabAt(i).setCustomView(mSectionsPagerAdapter.getTabView(i, false));
            }
        }

    }



    public void setTittle(String charseq) {
        if(tvTittle!=null)
        tvTittle.setText(charseq);
    }

    public void changeColor(int color, int position) {
        Fragment page = getSupportFragmentManager().findFragmentByTag(
                "android:switcher:" + R.id.container + ":" + mViewPager.getCurrentItem());
        if (page != null && page instanceof MapFragment) {
            ((MapFragment) page).changeColor(color, position);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        AppPrefs appPrefs = AppPrefs.getInstance(MainActivity.this);
        User user = appPrefs.getUser();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {

                case 0:
                    return new MapFragment();

                case 1:
                    return new AlertFragment();

                case 2:
                    return new ProjectFragment();

                case 3:
                    return new ModelFragment();

                case 4:
                    return new ResultsFragment();

                default:
                    return new MapFragment();
            }
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Map";
                case 1:
                    return "Alert";
                case 2:
                    return "Project";
                case 3:
                    return "Model";
                case 4:
                    return "Results";
            }
            return null;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            if (!(position == 0)) super.destroyItem(container, position, object);
        }

        public View getTabView(int i, boolean isSelected) {
            View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.custom_tab, null);
            TextView tabOne = (TextView) view.findViewById(R.id.tabLabel);
            RelativeLayout rl = (RelativeLayout) view.findViewById(R.id.rl);
            tabOne.setText(tabNames[i]);
            if (isSelected) {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, activeIcons[i], 0, 0);
                tabOne.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                tabOne.setTextColor(getResources().getColor(R.color.colorAccent));
                rl.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            } else {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, inActiveIcons[i], 0, 0);
                tabOne.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                tabOne.setTextColor(getResources().getColor(R.color.colorPrimary));
                rl.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            }
            tabOne.setSelected(true);
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            return view;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.NAVIGATE_TO_PROJECT && resultCode == RESULT_OK) {
            Fragment page = getSupportFragmentManager().findFragmentByTag(
                    "android:switcher:" + R.id.container + ":" + mViewPager.getCurrentItem());
            ((ProjectFragment) page).reloadFragment(true);
        } else if (requestCode == Constants.NAVIGATE_TO_MODEL && resultCode == RESULT_OK) {
            Fragment page = getSupportFragmentManager().findFragmentByTag(
                    "android:switcher:" + R.id.container + ":" + mViewPager.getCurrentItem());
            ((ModelFragment) page).reloadFragment(true);
        }
        else if (requestCode == Constants.NAVIGATE_TO_NEWS && resultCode == RESULT_OK) {
            Fragment page = getSupportFragmentManager().findFragmentByTag(
                    "android:switcher:" + R.id.container + ":" + mViewPager.getCurrentItem());
            ((ResultsFragment) page).reloadFragment(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //adjust login view
        MenuItem login = menu.findItem(R.id.action_login);
        MenuItem logout = menu.findItem(R.id.action_logout);

        AppPrefs appPrefs = AppPrefs.getInstance(this);
        if (appPrefs.getUser().isLoggedIn()) {
            login.setVisible(false);
            logout.setVisible(true);
        } else {
            login.setVisible(true);
            logout.setVisible(false);
        }
        //adjust layer view
        MenuItem layers = menu.findItem(R.id.action_shape);
        Fragment page = getSupportFragmentManager().findFragmentByTag(
                "android:switcher:" + R.id.container + ":" + mViewPager.getCurrentItem());
        if (page instanceof MapFragment) {
            layers.setVisible(true);
        } else {
            layers.setVisible(false);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_shape:
                showShapeFilesDialog();
                return true;
            case R.id.action_logout:
                performLogouton();
                return true;
            case R.id.action_login:
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_website:
                String url = "http://uttarakhanddisastermanagementsystem.com/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void performLogouton() {
        final AppPrefs appPrefs = AppPrefs.getInstance(this);
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.setCancelable(false);

        dialog.findViewById(R.id.yesbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                appPrefs.putUser(new User());
                Fragment page = getSupportFragmentManager().findFragmentByTag(
                        "android:switcher:" + R.id.container + ":" + mViewPager.getCurrentItem());
                updateAppUI(page);
//                Intent loginIntent = new Intent(MainActivity.this,LoginActivity.class);
//                startActivity(loginIntent);
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.nobtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void updateAppUI(Fragment page) {
        if (page != null) {
            if (page instanceof ProjectFragment) {
                ((ProjectFragment) page).reloadFragment(false);
            } else if ((page instanceof ReportFragment))
                ((ReportFragment) page).reloadFragment(false);
            else if ((page instanceof ModelFragment))
                ((ModelFragment) page).reloadFragment(false);
            else if ((page instanceof ResultsFragment))
                ((ResultsFragment) page).reloadFragment(false);
        }
    }

    private void showShapeFilesDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout_shapefiles);
        final ListView listView = (ListView) dialog.findViewById(R.id.lvShapeFiles);
        MultiCheckListAdapter adapter =
                new MultiCheckListAdapter(this, R.layout.layout_checkable_list, selectedShapeFiles);
        listView.setAdapter(adapter);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Fragment page = getSupportFragmentManager().findFragmentByTag(
                        "android:switcher:" + R.id.container + ":" + mViewPager.getCurrentItem());
                if (page instanceof MapFragment) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("selectedShapeFile", selectedShapeFiles);
                    ((MapFragment) page).showShapeFile(bundle);
                }
            }
        });
        dialog.show();
    }


}
