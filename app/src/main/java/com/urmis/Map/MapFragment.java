package com.urmis.Map;

import android.app.ProgressDialog;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import org.nocrala.tools.gis.data.esri.shapefile.ShapeFileReader;
import org.nocrala.tools.gis.data.esri.shapefile.ValidationPreferences;
import org.nocrala.tools.gis.data.esri.shapefile.shape.AbstractShape;
import org.nocrala.tools.gis.data.esri.shapefile.shape.PointData;
import org.nocrala.tools.gis.data.esri.shapefile.shape.shapes.PointShape;
import org.nocrala.tools.gis.data.esri.shapefile.shape.shapes.PolygonShape;
import org.nocrala.tools.gis.data.esri.shapefile.shape.shapes.PolylineShape;

import com.urmis.R;
import com.urmis.Repository.AppPrefs;
import com.urmis.Repository.Models.Shape.MShape;
import com.urmis.Repository.Models.Shape.ShapeFileList;
import com.urmis.Repository.Models.Shape.ShapeLayer;
import com.urmis.Utils.Colors.Colors;
import com.urmis.Utils.Constants;
import com.urmis.Utils.PopMessage;
import com.urmis.Utils.Utility;

/**
 * A fragment that launches other parts of the demo application.
 */
public class MapFragment extends Fragment {

  MapView mMapView;
  private GoogleMap googleMap;
  private ArrayList<String> shapeFileList = new ArrayList<>();
  private ArrayList<ShapeLayer> shapeLayersList = new ArrayList<>();
  private ProgressDialog progressDialog;
  private String fileName;
  private int currentShapeFileIndex = 0;

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // inflat and return the layout
    View v = inflater.inflate(R.layout.fragment_map, container, false);
    if (getActivity() instanceof MainActivity) {
      ((MainActivity) getActivity()).setTitle("URMIS");
    }
    mMapView = (MapView) v.findViewById(R.id.mapView);
    mMapView.onCreate(savedInstanceState);

    mMapView.onResume();// needed to get the map to display immediately

    try {
      MapsInitializer.initialize(getActivity().getApplicationContext());
    } catch (Exception e) {
      e.printStackTrace();
    }

    googleMap = mMapView.getMap();
    // latitude and longitude
    //double latitude = 30.0668;
    //double longitude = 79.0193;

    double latitude = 77.391029;
    double longitude = 28.535517;


    CameraPosition cameraPosition =
        new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(0).build();
    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    initShapeFileLoading();
    return v;
  }

  private void initShapeFileLoading() {
    new ShapeLoadingTask().execute();
  }
  private class ShapeLoadingTask extends AsyncTask<Void,Void,Void>{

    @Override protected Void doInBackground(Void... voids) {
      AppPrefs appPrefs = AppPrefs.getInstance(getActivity());
      shapeLayersList = appPrefs.getShapeLayerList();
      return null;
    }

    @Override protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      if (shapeFileList.size() > 0) {
        ((MainActivity) getActivity()).hideLoadingView();
      } else {
        readAllLocalShapeFiles();
      }
    }
  }

  private void readAllLocalShapeFiles() {
    try {
      String[] shapeNames = getActivity().getAssets().list("Shape");
      shapeFileList.addAll(Arrays.asList(shapeNames));
      fileName = shapeFileList.get(currentShapeFileIndex);
      new LoadingTasks().execute();
    } catch (Exception e) {
      Log.e("ex", e.getMessage());
    }
  }

  public void readShapeFiles(String file) {
    try {
      //googleMap.clear();
      fileName = file;
      new LoadingTasks().execute();
    } catch (Exception e) {
    }
  }

  @Override public void onResume() {
    super.onResume();
    mMapView.onResume();
  }

  @Override public void onPause() {
    super.onPause();
    mMapView.onPause();
  }

  @Override public void onDestroy() {
    super.onDestroy();
    mMapView.onDestroy();
  }

  @Override public void onLowMemory() {
    super.onLowMemory();
    mMapView.onLowMemory();
  }

  public void showShapeFile(Bundle position) {
    ArrayList<ShapeFileList> selectedPostions =
        position.getParcelableArrayList("selectedShapeFile");
    for (int i = 0; i < shapeLayersList.size(); i++) {
      ShapeLayer shapeLayer = shapeLayersList.get(i);
      if (selectedPostions.get(i).isChecked()) {
        shapeLayer.setVisibility(true);
      } else {
        shapeLayer.setVisibility(false);
      }
    }
  }



  public void changeColor(int color, int position) {
    Log.e("color",color+" ");
    String hexColor = String.format("#%06X", (0xFFFFFF & color));
    Log.e("color",hexColor+" ");
    for (int i = 0; i < shapeLayersList.size(); i++) {
      if(position==i) {
        ShapeLayer shapeLayer = shapeLayersList.get(i);
        shapeLayer.setColor(hexColor);
      }
    }
  }

  private class LoadingTasks extends AsyncTask<Void, Void, Void> {
    private boolean error = false;
    private LatLng centerLatLng;
    private String color = Colors.getColor(shapeLayersList.size());
    private ShapeLayer shapeLayer = new ShapeLayer();
    private ProgressDialog progressDialog;

    @Override protected void onPreExecute() {
      super.onPreExecute();
      progressDialog = new ProgressDialog(getActivity());
      progressDialog.setMessage("Loading shape files..");
      shapeLayer.setVisibility(false);
    }

    @Override protected Void doInBackground(Void... voids) {
      try {
        //String targetFilePath = destPath + filename + ".shp";
        //ShapeFile shapeFile = new ShapeFile(destPath, filename);
        //shapeFile.READ();

        AssetManager assetManager = getActivity().getAssets();
        InputStream fileInputStream = assetManager.open("Shape/" + fileName);
        ValidationPreferences prefs = new ValidationPreferences();
        prefs.setMaxNumberOfPointsPerShape(100000);
        ShapeFileReader r = new ShapeFileReader(fileInputStream, prefs);
        Log.e("s","w");
        AbstractShape s;
        int record = 1;
        ArrayList<MShape> mshapeList = new ArrayList<>();

        while ((s = r.next()) != null) {
          MShape mShape = new MShape();
          switch (s.getShapeType()) {

            case POLYLINE:
              PolylineOptions polyline = new PolylineOptions();
              polyline.color(Color.parseColor(color));
              polyline.width(25);
              PolylineShape aPolyline = (PolylineShape) s;
              for (int i = 0; i < aPolyline.getNumberOfParts(); i++) {
                PointData[] points = aPolyline.getPointsOfPart(i);
                for (int j = 0; j < points.length; j++) {
                  polyline.add(new LatLng(points[j].getY(), points[j].getX()));
                }
              }
              mShape.setShapeType(Constants.POLYLINE);
              mShape.setPolylineOptions(polyline);
              mshapeList.add(mShape);

              // poliLineArraylist.add(polyline);
              break;
            case POLYGON:
              PolygonShape polygonShape = (PolygonShape) s;
              final PolygonOptions polygonOptions = new PolygonOptions();
              polygonOptions.strokeColor(Color.argb(150, 0, 0, 0));
              polygonOptions.fillColor(Color.parseColor(color));
              polygonOptions.strokeWidth(2.0f);
              for (int i = 0; i < polygonShape.getNumberOfParts(); i++) {
                PointData[] points = polygonShape.getPointsOfPart(i);
                for (int j = 0; j < points.length; j++) {
                  polygonOptions.add(new LatLng(points[j].getY(), points[j].getX()));
                }
                centerLatLng =
                    new LatLng(points[points.length / 2].getY(), points[points.length / 2].getX());
              }
              mShape.setShapeType(Constants.POLYGON);
              mShape.setPolygonOptions(polygonOptions);
              mshapeList.add(mShape);
              // polygonOptionsArrayList.add(polygonOptions);
              break;
            case POINT:
              PointShape pointShape = (PointShape) s;
              MarkerOptions markerOptions = new MarkerOptions();
              markerOptions.position(new LatLng(pointShape.getY(), pointShape.getX()))
                  .icon(Utility.getMarkerIcon(color));
              mShape.setShapeType(Constants.POINT);
              mShape.setMarkerOptions(markerOptions);
              mshapeList.add(mShape);
              //markerOptionsArrayList.add(markerOptions);
            default:
              System.out.println("Read other type of shape.");
          }

          record++;
        }
        Log.e("s","d");
        shapeLayer.setmShapeArrayList(mshapeList);
        shapeLayer.setColor(color);
        shapeLayersList.add(shapeLayer);
        shapeLayer.setCentreLatLong(centerLatLng);
        publishProgress();
      } catch (Exception e) {
        Log.e("ds", e.getMessage()+"s");
        error = true;
      }

      return null;
    }

    @Override protected void onProgressUpdate(Void... values) {
      super.onProgressUpdate(values);
      ArrayList<MShape> mShapeList =
          shapeLayersList.get(shapeLayersList.size() - 1).getmShapeArrayList();
      for (int i = 0; i < mShapeList.size(); i++) {
        MShape mShape = mShapeList.get(i);
        int shapeType = mShape.getShapeType();
        switch (shapeType) {
          case Constants.POINT:
            Marker marker = googleMap.addMarker(mShape.getMarkerOptions());
            marker.setVisible(true);
            mShape.setMarker(marker);
            break;
          case Constants.POLYLINE:
            Polyline polyline = googleMap.addPolyline(mShape.getPolylineOptions());
            polyline.setVisible(true);
            mShape.setPolyline(polyline);
            break;
          case Constants.POLYGON:
            Polygon polygon = googleMap.addPolygon(mShape.getPolygonOptions());
            polygon.setVisible(true);
            mShape.setPolygon(polygon);
            break;
        }
      }
      shapeLayersList.get(shapeLayersList.size() - 1).setmShapeArrayList(mShapeList);
      Log.e("shapeFileListSize", currentShapeFileIndex + " ");
    }

    @Override protected void onPostExecute(Void ignore) {
      if (isAdded()) {
        currentShapeFileIndex++;
        if (currentShapeFileIndex < shapeFileList.size()) {
          fileName = shapeFileList.get(currentShapeFileIndex);
          new LoadingTasks().execute();
        } else {
          progressDialog.dismiss();
          if (error) {
            //PopMessage.makeshorttoast(getActivity(), "Error");
          } else {
            //cacheShapeFileList();
            ((MainActivity) getActivity()).hideLoadingView();
            //PopMessage.makeshorttoast(getActivity(), "Layers successfully loaded.");
          }
        }
      }
    }
  }

  class CacheTask extends AsyncTask<Void,Void,Void>
  {
    @Override protected void onPreExecute() {
      super.onPreExecute();
      PopMessage.makeshorttoast(getActivity(),"Caching Shapefiles for subsequent use..");
    }

    @Override protected Void doInBackground(Void... voids) {
      try {
        Log.e("c","caching");
        AppPrefs appPrefs = AppPrefs.getInstance(getActivity());
        appPrefs.putShapeLayerList(shapeLayersList);
        PopMessage.makeshorttoast(getActivity(), "Shape Files Cached");
        Log.e("c","cac");
      }
      catch (Exception e)
      {
        Log.e("da",e.getMessage());
      }
      return null;
    }

    @Override protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      ((MainActivity) getActivity()).hideLoadingView();
      PopMessage.makeshorttoast(getActivity(), "Shape Files succesfully loaded.");
    }
  }

  private void cacheShapeFileList() {
    new CacheTask().execute();
  }


}