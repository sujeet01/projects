package com.urmis.Map;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import com.urmis.R;
import com.urmis.Repository.Models.Shape.ShapeFileList;
import com.urmis.Utils.RoundedImageView;
import yuku.ambilwarna.AmbilWarnaDialog;

/**
 * Created by Brajendr on 4/17/2017.
 */

public class MultiCheckListAdapter extends ArrayAdapter<ShapeFileList> {
  private ArrayList<ShapeFileList> shapeFileLists;
  private Context context;

  public MultiCheckListAdapter(Context context, int resource,
      ArrayList<ShapeFileList> shapeFileLists1) {
    super(context, resource);
    this.context = context;
    this.shapeFileLists = shapeFileLists1;
  }

  @NonNull @Override public View getView(final int position, View convertView, ViewGroup parent) {
    View v;
    LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    v = vi.inflate(R.layout.layout_checkable_list, null);
    final ShapeFileList shapeFileList = shapeFileLists.get(position);

    TextView tvName = (TextView) v.findViewById(R.id.tvName);
    RoundedImageView roundedImageView = (RoundedImageView) v.findViewById(R.id.imgColor);
    CheckBox checkBox = (CheckBox) v.findViewById(R.id.chk);

    tvName.setText(shapeFileList.getName());
    roundedImageView.setBackgroundColor(Color.parseColor(shapeFileList.getColor()));
    checkBox.setChecked(shapeFileList.isChecked());
    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        shapeFileList.setChecked(b);
        notifyDataSetChanged();
      }
    });
    roundedImageView.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        //TODO: CODE FOR COLOR PICKER
       // openColorPickerDialog(shapeFileList.getColor(), position);
      }
    });
    return v;
  }

  private void openColorPickerDialog(final String colo, final int position) {
    AmbilWarnaDialog ambilWarnaDialog = new AmbilWarnaDialog(context, Color.parseColor(colo), false,
        new AmbilWarnaDialog.OnAmbilWarnaListener() {
          @Override public void onOk(AmbilWarnaDialog ambilWarnaDialog, int color) {
            String hexColor = String.format("#%06X", (0xFFFFFF & color));
            if (context instanceof MainActivity) {
              ((MainActivity) context).changeColor(color, position);
            }
            shapeFileLists.get(position).setColor(hexColor);
            notifyDataSetChanged();
          }

          @Override public void onCancel(AmbilWarnaDialog ambilWarnaDialog) {

          }
        });
    ambilWarnaDialog.show();
  }

  @Override public int getCount() {
    return shapeFileLists.size();
  }
}
